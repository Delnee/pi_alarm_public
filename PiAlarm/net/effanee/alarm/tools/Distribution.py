'''
Created on 20 Apr 2015

@author: fjh
'''
import os
from shutil import rmtree, copyfile
import time

# pylint: disable=too-few-public-methods, anomalous-unicode-escape-in-string, anomalous-backslash-in-string
class Distribution(object):
    '''Build a distribution directory layout of this application.

    This class should not be instantiated. Just run. All methods are class
    methods.

    cd "C:\Users\fjh\git\PiAlarm\PiAlarm\dist"
    c:\Python27\python.exe setup.py check
    c:\Python27\python.exe setup.py sdist

    sudo cp -r /mnt/samba/media/alarmclock/net ./
    '''

    DIST_FILE_PATH = "../../../../../dist"
    SOURCE_FILE_PATH = "../../../../../net"
    SETUP_FILES = ["./MANIFEST.in",
                   "./setup.py"]
    EXCLUDE_DIRECTORY_PATHS = ["../../../../../net/effanee/alarm/doc_private",
                               "../../../../../net/effanee/alarm/res/chimes/xeno-canto",
                               "../../../../../net/effanee/alarm/test",
                               "../../../../../net/effanee/alarm/tools"]
    #CONFIG_FILE_PATH = "/../config"

    _this_path = None
    _dist_path = None
    _source_path = None
    _excluded_paths = []
    #_config_path = None

    def __init__(self):
        '''Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def refresh_dist(cls):
        '''Delete the contents of the distribution directory, and re-populate it.
        '''
        cls._init()
        cls._clean_dist_dir()
        cls._recursive_copy_files(cls._source_path, cls._dist_path)
        #cls._recursive_copy_files(cls._config_path, cls._dist_path)
        cls._copy_setup_files()
        print "Done."

    @classmethod
    def _init(cls):
        '''Initialise.
        '''
        cls._this_path = os.path.dirname(os.path.abspath(__file__))
        cls._dist_path = os.path.abspath(str(cls._this_path + cls.DIST_FILE_PATH))
        cls._source_path = os.path.abspath(str(cls._this_path + cls.SOURCE_FILE_PATH))
        #cls._config_path = os.path.abspath(str(cls._this_path + cls.CONFIG_FILE_PATH))

        for path in cls.EXCLUDE_DIRECTORY_PATHS:
            exclude_dir = os.path.abspath(str(cls._this_path + path))
            if os.path.isdir(exclude_dir):
                cls._excluded_paths.append(exclude_dir)

        print "This classes path - {0}.".format(cls._this_path)
        print "The source path - {0}.".format(cls._source_path)
        print "The destination path - {0}.".format(cls._dist_path)
        print "The excluded paths - {0}.".format(cls._excluded_paths)
        #print "The configuration path - {0}.".format(cls._config_path)

    @classmethod
    def _clean_dist_dir(cls):
        '''Empty the distribution directory.
        '''
        if os.path.isdir(cls._dist_path):
            rmtree(cls._dist_path)
            time.sleep(1)

        os.makedirs(cls._dist_path)

    @classmethod
    def _recursive_copy_files(cls, source, destination):
        '''Copy the source files, and directory structure, to the
        distribution directory. Ignoring any directories in the exclude list.

        Args:
            source - The source directory.
            destination - The destination directory.
        '''
        # Create the destination path from the source path.
        destination_path = os.path.join(destination, os.path.basename(source))
        os.makedirs(destination_path)

        # Get source files/directories.
        files_dirs = os.listdir(source)

        # Copy/create.
        for file_dir in files_dirs:

            source_path = os.path.join(source, file_dir)

            if os.path.isfile(source_path):
                copyfile(source_path, os.path.join(destination_path, file_dir))

            elif os.path.isdir(source_path):
                if not source_path in cls._excluded_paths:
                    cls._recursive_copy_files(source_path, destination_path)

    @classmethod
    def _copy_setup_files(cls):
        '''Copy the setup source files to the distribution directory.
        '''
        for setup_file in cls.SETUP_FILES:

            source = os.path.abspath(os.path.join(cls._this_path, setup_file))
            dest = os.path.abspath(os.path.join(cls._dist_path, setup_file))

            copyfile(source, dest)

# Start me up.
Distribution.refresh_dist()
