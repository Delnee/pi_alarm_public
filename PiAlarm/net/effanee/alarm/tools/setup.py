'''
Created on 13 May 2015

@author: fjh
'''
#from distutils.core import setup
from setuptools import setup

'''Switched to setuptools, to make installing data files more straightforward,
by using setup param "include_package_data=True".
'''
# pylint: disable=line-too-long
setup(
    name="net.effanee.alarm",
    version="0.0.1",
    author="Francis Hammell",
    author_email="hammell.francis@gmail.com",
    url="http://www.effanee.net",
    description="Alarm Clock for the Raspberry Pi - Controlled via IR Remote",
    license="GPLv3+",
    platforms=["linux2"],
    include_package_data=True,
    zip_safe=False,
    packages=[
        "net",
        "net.effanee",
        "net.effanee.alarm",
        "net.effanee.alarm.activity",
        "net.effanee.alarm.activity.action",
        "net.effanee.alarm.activity.implementation",
        "net.effanee.alarm.activity.implementation.alarm",
        "net.effanee.alarm.activity.implementation.clock",
        "net.effanee.alarm.activity.implementation.multipleinput",
        "net.effanee.alarm.activity.implementation.persist",
        "net.effanee.alarm.activity.implementation.play",
        "net.effanee.alarm.activity.implementation.schedule",
        "net.effanee.alarm.activity.implementation.talk",
        "net.effanee.alarm.activity.interface",
        "net.effanee.alarm.activity.interface.alarm",
        "net.effanee.alarm.activity.interface.clock",
        "net.effanee.alarm.activity.interface.multipleinput",
        "net.effanee.alarm.activity.interface.persist",
        "net.effanee.alarm.activity.interface.play",
        "net.effanee.alarm.activity.interface.schedule",
        "net.effanee.alarm.activity.interface.talk",
        "net.effanee.alarm.help",
        "net.effanee.alarm.input",
        "net.effanee.alarm.input.handler",
        "net.effanee.alarm.input.lirc",
        "net.effanee.alarm.input.listener",
        "net.effanee.alarm.ui",
        "net.effanee.alarm.ui.remotecontrol",
        "net.effanee.alarm.ui.remotecontrol.direct",
        "net.effanee.alarm.ui.remotecontrol.menu"
        ],
    keywords=["Alarm Clock", "Raspberry Pi"],
    dependency_links=["https://launchpad.net/python-espeak/trunk/0.5/+download/python-espeak-0.5.tar.gz"],
    install_requires=[
        "pylirc2==0.1",
        "RPi.GPIO==0.5.11",
        "apscheduler==3.0.2",
        "python-espeak==0.5"
        ],
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX :: Linux - Raspian",
        "Natural Language :: English",
        "Topic :: Home Automation",
        "Topic :: Other/Nonlisted Topic"
        ],
    long_description="""
===========================================================
Alarm Clock for the Raspberry Pi - Controlled via IR Remote
===========================================================

Functionality
-------------

This application is a Python 2.7 program that implements the following functionality:-
*    Runs as a daemon process, using a System V init script, and utilising the start-stop-daemon utility.
*    Optionally plays hourly chimes, from wav files stored in a specified directory.
*    Allows an alarm to be set, and plays a nominated audio source when triggered.
*    Supports audio streams from BBC Radio 1, 2, 3 and 4, and a local mpd playlist named "AlarmClock".
*    The selected audio stream can be played/stopped independently of the alarm.
*    Uses an infrared remote control and text-to-speech as the control interface.

Dependencies
------------

Hardware
~~~~~~~~

*    A Raspberry Pi.
*    An internet connection via Wi-Fi or RJ45 cable.
*    A USB sound card.
*    An infrared remote control.
*    An infrared receiver.
*    A speaker.

External Applications
~~~~~~~~~~~~~~~~~~~~~

*    aplay.
*    alsa.
*    espeak.
*    mpd.
*    mpc.
*    lirc.

Python Libraries
~~~~~~~~~~~~~~~~

*    APScheduler 3.0.2
*    pylirc2 0.1
*    python-espeak 0.5
*    RPi.GPIO 0.5.11
"""
)
