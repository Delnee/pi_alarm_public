'''
Created on 22 Mar 2015

@author: fjh
'''
import logging
from net.effanee.alarm.ui.remotecontrol.menu.Node import Node
from net.effanee.alarm.ui.remotecontrol.menu.Properties import Properties
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum
from net.effanee.alarm.activity.action.CommonAction import CommonAction
from net.effanee.alarm.activity.action.ClockAction import ClockAction
from net.effanee.alarm.activity.action.AudioAction import AudioAction

class Menu(object):
    '''This class is used to build a tree menu.

    Conceptually, this tree menu has its root to the left with its branches
    extending to the right, which represent the lower tree levels. At any
    level, the upper nodes vertically, are conceptually before the lower ones.

    A menu is composed of Nodes, which provide the menu structure, and each
    node has an embedded Properties object, (which stores the nodes state),
    and links to activities that are invoked when the menu is focused,
    selected or receiving input.

    This class implements the menu's actions - such as the navigation
    actions:-

        up - to the parent.
        down - to a child.
        left and right - to the siblings.

    and the state change actions:-

        onfocus - when a navigation action arrives at a node.
        onselect - when a focused node is chosen.

    and the enter arbitrary key sequences action:-

        oninput - obtain from the user a sequence of key presses.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self):
        '''Constructor
        '''
        # Create key mappings.
        self.__menu_key_mappings = {
            KeysEnum.KEY_UP: self.__up,
            KeysEnum.KEY_DOWN: self.__down,
            KeysEnum.KEY_LEFT: self.__left,
            KeysEnum.KEY_RIGHT: self.__right,
            KeysEnum.KEY_ENTER: self.__select,
            KeysEnum.KEY_BACK: self.__input,
            KeysEnum.KEY_KP0: self.__input,
            KeysEnum.KEY_KP1: self.__input,
            KeysEnum.KEY_KP2: self.__input,
            KeysEnum.KEY_KP3: self.__input,
            KeysEnum.KEY_KP4: self.__input,
            KeysEnum.KEY_KP5: self.__input,
            KeysEnum.KEY_KP6: self.__input,
            KeysEnum.KEY_KP7: self.__input,
            KeysEnum.KEY_KP8: self.__input,
            KeysEnum.KEY_KP9: self.__input
            }

        # Start node.
        self.__root_node = self.__build_menu()
        self.__current_node = self.__root_node

    # pylint: disable=too-many-locals, too-many-statements, line-too-long
    def __build_menu(self):
        '''Build the tree menu:-

            Home    Clock   Time
                            Chimes  On
                                    Off
                                    Play Random Chime

                    Alarm   On
                            Off
                            Set Time

                    Audio   Play
                            Pause
                            Stop
                            Source  get_sources()
                    Status

        Internal nodes say their name when focused and selected. Leaf nodes say
        their name when focused, but perform actions when selected or during
        input.
        '''
        # Get the menu actions.
        #self.__menu_action = MenuAction()
        self.__common_action = CommonAction()
        self.__clock_action = ClockAction()
        self.__audio_action = AudioAction()

        # --- LEVEL 0 ---
        # Create root (Home). Don't use the convenience methods, (to show how
        # to create a node using the Properties and Node methods directly).
        home_props = Properties("Home", Properties.INTERNAL)
        home_props.register_on_focus_activity(self.__common_action.say, home_props.get_name())
        home_props.register_on_select_activity(self.__common_action.say, home_props.get_name())
        home_node = Node(home_props, None)

        # --- LEVEL 1 ---
        # Clock.
        clock_props = self.__create_internal_properties("Clock")
        clock_node = home_node.create_child_after(clock_props)

        # Alarm.
        alarm_props = self.__create_internal_properties("Alarm")
        alarm_node = home_node.create_child_after(alarm_props)

        # Audio.
        audio_props = self.__create_internal_properties("Audio")
        audio_node = home_node.create_child_after(audio_props)

        # Status.
        status_props = self.__create_leaf_properties("Status")
        status_props.register_on_select_activity(self.__common_action.say_status)
        status_props = home_node.create_child_after(status_props)

        # --- LEVEL 2 ---
        # Clock Time.
        clock_time_props = self.__create_leaf_properties("Time")
        clock_time_props.register_on_select_activity(self.__clock_action.say_time)
        clock_node.create_child_after(clock_time_props)

        # Clock Chimes.
        clock_chimes_props = self.__create_internal_properties("Chimes")
        clock_chimes_node = clock_node.create_child_after(clock_chimes_props)

        # Alarm On.
        alarm_on_props = self.__create_leaf_properties("On")
        alarm_on_props.register_on_select_activity(self.__clock_action.alarm_on)
        alarm_node.create_child_after(alarm_on_props)

        # Alarm Off.
        alarm_off_props = self.__create_leaf_properties("Off")
        alarm_off_props.register_on_select_activity(self.__clock_action.alarm_off)
        alarm_node.create_child_after(alarm_off_props)

        # Alarm Set Time.
        alarm_set_time_props = self.__create_leaf_properties("Set Time")
        alarm_set_time_props.register_on_select_activity(self.__clock_action.set_alarm_time)
        alarm_set_time_props.register_on_input_activity(self.__clock_action.add_alarm_key_press)
        alarm_node.create_child_after(alarm_set_time_props)

        # Audio Play.
        audio_play_props = self.__create_leaf_properties("Play")
        audio_play_props.register_on_select_activity(self.__audio_action.play_pause)
        audio_node.create_child_after(audio_play_props)

        # Audio Pause.
        audio_pause_props = self.__create_leaf_properties("Pause")
        audio_pause_props.register_on_select_activity(self.__audio_action.play_pause)
        audio_node.create_child_after(audio_pause_props)

        # Audio Stop.
        audio_stop_props = self.__create_leaf_properties("Stop")
        audio_stop_props.register_on_select_activity(self.__audio_action.stop)
        audio_node.create_child_after(audio_stop_props)

        # Audio Source.
        audio_source_props = self.__create_internal_properties("Source")
        audio_source_node = audio_node.create_child_after(audio_source_props)

        # --- LEVEL 3 ---
        # Clock Chimes On.
        clock_chimes_on_props = self.__create_leaf_properties("On")
        clock_chimes_on_props.register_on_select_activity(self.__clock_action.chimes_on)
        clock_chimes_node.create_child_after(clock_chimes_on_props)

        # Clock Chimes Off.
        clock_chimes_off_props = self.__create_leaf_properties("Off")
        clock_chimes_off_props.register_on_select_activity(self.__clock_action.chimes_off)
        clock_chimes_node.create_child_after(clock_chimes_off_props)

        # Play Random Chime.
        clock_chimes_play_random_props = self.__create_leaf_properties("Play Random Chime")
        clock_chimes_play_random_props.register_on_select_activity(self.__clock_action.play_chime)
        clock_chimes_node.create_child_after(clock_chimes_play_random_props)

        #Audio Sources...
        audio_sources = self.__audio_action.get_sources()
        for source in audio_sources:
            source_props = self.__create_leaf_properties(source)
            source_props.register_on_select_activity(self.__audio_action.set_source, source)
            audio_source_node.create_child_after(source_props)

        # Land on the current node.
        home_props.set_focused()

        return home_node

    def __create_leaf_properties(self, node_name):
        ''' Create a leaf node Properties object. With an on focus activity that
        speaks the node name.

        Args:
            node_name - The name of the node.

        Returns:
            The initialised Properties object.
        '''
        props = Properties(node_name, Properties.LEAF)
        props.register_on_focus_activity(self.__common_action.say, node_name)

        return props

    def __create_internal_properties(self, node_name):

        ''' Create an internal node Properties object. With an on focus
        activity, and an on select method, that speak the node's name.

        Args:
            node_name - The name of the node.

        Returns:
            The initialised Properties object.
        '''
        props = Properties(node_name, Properties.INTERNAL)
        props.register_on_focus_activity(self.__common_action.say, node_name)
        message = node_name + " has options to the right."
        props.register_on_select_activity(self.__common_action.say, message)

        return props

    def __input(self, *args):
        '''Invoke the input method on the current node.

        Args:
            *args - This method will have a KeysEnum argument.
        '''
        self.__current_node.get_properties().set_input(args[0])
        self.__current_node.get_properties().unset_input()

        # pylint: disable=logging-format-interpolation, line-too-long
        self.__class__.logger.debug("Invoked input on node {0}"
                                    .format(self.__current_node.get_properties().get_name()))

    # pylint: disable=unused-argument
    def __select(self, *args):
        '''Invoke the selected method on the current node.

        Args:
            *args - This method will not have any arguments.
        '''
        self.__current_node.get_properties().set_selected()
        self.__current_node.get_properties().unset_selected()

        # pylint: disable=logging-format-interpolation, line-too-long
        self.__class__.logger.debug("Invoked select on node {0}"
                                    .format(self.__current_node.get_properties().get_name()))

    # pylint: disable=unused-argument
    def __up(self, *args):
        '''Move to the next sibling up (vertically with root left-most). Will
        loop around from the top to the bottom.

        Args:
            *args - This method will not have any arguments.
        '''
        next_node = self.__get_next_vertically(-1)
        self.__set_next_vertically(next_node)
        # pylint: disable=logging-format-interpolation, line-too-long
        self.__class__.logger.debug("Moved up to node {0}"
                                    .format(self.__current_node.get_properties().get_name()))

    # pylint: disable=unused-argument
    def __down(self, *args):
        '''Move to the next sibling down (vertically with root left-most). Will
        loop around from the bottom to the top.

        Args:
            *args - This method will not have any arguments.
        '''
        next_node = self.__get_next_vertically(+1)
        self.__set_next_vertically(next_node)
        # pylint: disable=logging-format-interpolation, line-too-long
        self.__class__.logger.debug("Moved down to node {0}"
                                    .format(self.__current_node.get_properties().get_name()))

    def __set_next_vertically(self, next_node):
        '''Set the supplied node as the current node.

        Args:
            next_node - The node to set as current.
        '''
        self.__current_node.get_properties().unset_focused()
        self.__current_node = next_node
        self.__current_node.get_properties().set_focused()

    def __get_next_vertically(self, next_position):
        '''Get the next node up or down in relation to the current node.

        Args:
            next_position - +1 to get the node node down, or -1 to get the next
            node up.

        Returns:
            The next node up or down.
        '''
        sibling_list = self.__get_self_and_siblings()
        current_index = sibling_list.index(self.__current_node)
        next_index = (current_index + next_position) % len(sibling_list)

        return sibling_list[next_index]

    # pylint: disable=unused-argument
    def __left(self, *args):
        ''' Move to the parent and set it as focused. If the current node is
        the root, do not move.

        Args:
            *args - This method will not have any arguments.
        '''
        if self.__current_node.get_parent():
            self.__current_node = self.__current_node.get_parent()

        self.__current_node.get_properties().set_focused()
        # pylint: disable=logging-format-interpolation, line-too-long
        self.__class__.logger.debug("Moved left to node {0}"
                                    .format(self.__current_node.get_properties().get_name()))

    # pylint: disable=unused-argument
    def __right(self, *args):
        ''' Move to the focused child. If no child has focus, move to and focus
        the first child. If the current node is a leaf, do not move.

        Args:
            *args - This method will not have any arguments.
        '''
        if self.__current_node.get_children():
            node_list = self.__current_node.get_children()
            self.__current_node = self.__get_focused(node_list)

        self.__current_node.get_properties().set_focused()
        # pylint: disable=logging-format-interpolation, line-too-long
        self.__class__.logger.debug("Moved right to node {0}"
                                    .format(self.__current_node.get_properties().get_name()))

    def __get_self_and_siblings(self):
        '''Get a list of all the children of the current nodes parent node.

        Returns:
            A node list, including this node and any siblings.
        '''
        parent = self.__current_node.get_parent()
        if parent:
            return parent.get_children()
        else:
            return [self.__current_node]

    # pylint: disable=no-self-use
    def __get_focused(self, node_list):
        '''Get the node within the list that has focus.

        Returns:
            The focused node. If no node has focus, then the first node in the
            list.
        '''
        for node in node_list:
            if node.get_properties().is_focused():
                return node

        node_list[0].get_properties().set_focused()
        return node_list[0]

    def activate(self, key_press):
        '''Activity interface method. Uses the menu_key_mappings map to
        select which method to invoke.

        Args:
            key_press: The internal enum representing the key press code.
        '''
        # pylint: disable=logging-format-interpolation
        if key_press in self.__menu_key_mappings:
            self.__class__.logger.info("Activating key press {0} in activity {1}."
                                        .format(key_press, self.__class__.__name__))

            self.__menu_key_mappings[key_press](key_press)

        else:
            self.__class__.logger.warn("Key press {0}, in activity {1}, "
                                       "does not map to an interface method."
                                       .format(key_press, self.__class__.__name__))

        return False

    def pretty_string(self):
        '''Create an indented string representation of the supplied nodes
        subtree.
        '''
        return self.__string_subtree(self.__root_node)

    def simple_pretty_string(self):
        '''Create an indented string representation of the supplied nodes
        subtree.
        '''
        return self.__simple_string_subtree(self.__root_node)

    def __string_subtree(self, node, depth=0):
        '''Create an indented string representation of the supplied nodes
        subtree.

        Args:
            node - The starting node to create the subtree from.
            depth - always initiated with 0. Used internally during recursion
            to keep track of the tree depth, and hence the indentation level.

        Returns:
            A string representation of this nodes subtree.
        '''
        formatted_output = "  " * depth + str(node) + "\n"
        for child in node.get_children():
            formatted_output += self.__string_subtree(child, depth + 1)

        return formatted_output

    def __simple_string_subtree(self, node, depth=0):
        '''Create an indented string representation of the supplied nodes
        subtree.

        Args:
            node - The starting node to create the subtree from.
            depth - always initiated with 0. Used internally during recursion
            to keep track of the tree depth, and hence the indentation level.

        Returns:
            A string representation of this nodes subtree.
        '''
        formatted_output = "  " * depth + node.get_properties().get_name() + "(" + node.get_properties().get_node_type() + ")" +  "\n"
        for child in node.get_children():
            formatted_output += self.__simple_string_subtree(child, depth + 1)

        return formatted_output
