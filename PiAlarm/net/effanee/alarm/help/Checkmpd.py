'''
Created on 5 May 2015

@author: fjh
'''
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
import time

# pylint: disable=too-few-public-methods
class Checkmpd(object):
    '''Helper class used to check the local audio source.
    '''
    def play_local(self):
        '''Play the local audio source "Alarm Clock".
        '''
        self.__config_implementations()
        PlayFacade.set_source("Alarm Clock")
        print "Playing 5 seconds of 'Alarm Clock' source."
        PlayFacade.play_pause()
        time.sleep(5)
        PlayFacade.stop()
        print "Finished."

    # pylint: disable=no-self-use
    def __config_implementations(self):
        '''Configure the facade/implementation pairings.
        '''
        PlayFacade.set_implementation(CliPlayImpl())

if __name__ == "__main__":
    Checkmpd().play_local()
