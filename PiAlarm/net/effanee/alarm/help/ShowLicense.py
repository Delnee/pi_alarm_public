'''
Created on 6 May 2015

@author: fjh
'''

# pylint: disable=too-few-public-methods
class ShowLicense(object):
    '''Helper class used to display the license.
    '''

    license = '''Alarm Clock provides some alarm clock functionality.

Copyright (C) 2015 Francis J. Hammell <hammell.francis@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

There is a copy of this license in net/effanee/alarm/doc/license.txt.
If not, see <http://www.gnu.org/licenses/>.'''

    # pylint: disable=no-self-use
    def display_license(self):
        '''Display license.
        '''
        print ShowLicense.license

if __name__ == "__main__":
    ShowLicense().display_license()
