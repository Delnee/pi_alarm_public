'''
Created on 6 May 2015

@author: fjh
'''
from net.effanee.alarm.input.lirc.KeyPressNotifier import KeyPressNotifier
import threading
import time
import platform

# pylint: disable=too-few-public-methods
class Checklirc(object):
    '''Helper class used to check the lirc configuration.
    '''
    def input(self):
        '''Receive a IR remote key press and echo the result.
        '''
        # Create notifier.
        key_press_notifier = KeyPressNotifier()
        key_press_notifier.daemon = True
        key_press_notifier.start()

        # Create listener to receive key presses.
        listener_thread = self.Listener()
        listener_thread.daemon = True
        listener_thread.start()
        time.sleep(1)

        # Register listener.
        key_press_notifier.register_listener(listener_thread)

        # Send key press.
        if platform.system() == "Windows":
            from net.effanee.alarm.test.mocks.pylirc.pylirc import pylirc
            pylirc.add_key({"config": "KEY_KP1", "repeat": "1"})
            pylirc.send_keys()
        else:
            print "Press a key on the remote..."

        count_down = 10
        while (not listener_thread.get_key_press()) and (count_down > 0):
            print "Time remaining... {0} seconds.".format(count_down)
            count_down -= 1
            time.sleep(1)

        print "You pressed key {0}".format(listener_thread.get_key_press())

        # Tidy.
        key_press_notifier.stop()

    class Listener(threading.Thread):
        '''Utility class used to capture key presses.
        '''

        def __init__(self):
            '''
            Constructor.
            '''
            threading.Thread.__init__(self)
            self.__last_key_press = None

        def run(self):
            '''Override thread run. Wait until a key press is received.
            '''
            # Wait here.
            while not self.__last_key_press:
                if time:
                    time.sleep(.1)
                else:
                    break

        def update(self, key_press):
            '''Listener interface method. Stores the last key press enum.

            Args:
                key_press: The internal enum representing the key press code.
            '''
            # pylint: disable=logging-format-interpolation
            self.__last_key_press = key_press

            return False

        def get_key_press(self):
            '''Return the key press.
            '''
            return self.__last_key_press

if __name__ == "__main__":
    Checklirc().input()
