'''
Created on 5 May 2015

@author: fjh
'''
from net.effanee.alarm.ui.remotecontrol.menu.Menu import Menu

# Facade/Implementation.
# pylint: disable=line-too-long
from net.effanee.alarm.activity.interface.multipleinput.MultipleInputFacade import MultipleInputFacade
from net.effanee.alarm.activity.implementation.multipleinput.MultipleInputImpl import MultipleInputImpl
from net.effanee.alarm.activity.interface.clock.ClockFacade import ClockFacade
from net.effanee.alarm.activity.implementation.clock.ClockImpl import ClockImpl
from net.effanee.alarm.activity.interface.talk.SpeechFacade import SpeechFacade
from net.effanee.alarm.activity.implementation.talk.eSpeakImpl import eSpeakImpl
from net.effanee.alarm.activity.interface.alarm.AlarmFacade import AlarmFacade
from net.effanee.alarm.activity.implementation.alarm.AlarmImpl import AlarmImpl
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
from net.effanee.alarm.activity.interface.schedule.SchedulerFacade import SchedulerFacade
from net.effanee.alarm.activity.implementation.schedule.APSchedulerImpl import APSchedulerImpl
from net.effanee.alarm.activity.interface.persist.PersistFacade import PersistFacade
from net.effanee.alarm.activity.implementation.persist.PersistImpl import PersistImpl

# pylint: disable=too-few-public-methods
class ShowMenu(object):
    '''Helper class used to display the IR remote menu.
    '''
    def display_menu_structure(self):
        '''Display simple menu structure.
        '''
        self.__config_implementations()
        menu_string = Menu().simple_pretty_string()
        print menu_string

    # pylint: disable=no-self-use
    def __config_implementations(self):
        '''Configure the facade/implementation pairings.
        '''
        MultipleInputFacade.set_implementation(MultipleInputImpl)
        ClockFacade.set_implementation(ClockImpl)
        SpeechFacade.set_implementation(eSpeakImpl)
        AlarmFacade.set_implementation(AlarmImpl)
        PlayFacade.set_implementation(CliPlayImpl())
        SchedulerFacade.set_implementation(APSchedulerImpl)
        PersistFacade.set_implementation(PersistImpl)

if __name__ == "__main__":
    ShowMenu().display_menu_structure()
