'''
Created on 5 May 2015

@author: fjh
'''
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
from net.effanee.alarm.activity.interface.clock.ClockFacade import ClockFacade
from net.effanee.alarm.activity.implementation.clock.ClockImpl import ClockImpl

# pylint: disable=too-few-public-methods
class Checkaplay(object):
    '''Helper class used to check the local audio source.
    '''
    def play(self):
        '''Play a random chime.
        '''
        self.__config_implementations()
        print "Playing random chime."
        PlayFacade.play_over_sync(ClockFacade.get_random_chime_wav())
        PlayFacade.stop()
        print "Finished."

    # pylint: disable=no-self-use
    def __config_implementations(self):
        '''Configure the facade/implementation pairings.
        '''
        PlayFacade.set_implementation(CliPlayImpl())
        ClockFacade.set_implementation(ClockImpl)

if __name__ == "__main__":
    Checkaplay().play()
