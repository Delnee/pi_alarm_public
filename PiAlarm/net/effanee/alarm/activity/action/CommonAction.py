'''
Created on 24 Apr 2015

@author: fjh
'''
import logging
# pylint: disable=line-too-long
from net.effanee.alarm.activity.interface.clock.ClockFacade import ClockFacade
from net.effanee.alarm.activity.interface.talk.SpeechFacade import SpeechFacade
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.interface.alarm.AlarmFacade import AlarmFacade
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum

class CommonAction(object):
    '''A collection of actions for common tasks.

    UI elements should call action classes, rather than the facade classes
    directly.

    Uses and combines various facade classes.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self):
        '''
        Constructor
        '''
        self.__alarm_facade = AlarmFacade
        self.__clock_facade = ClockFacade
        self.__play_facade = PlayFacade
        self.__speech_facade = SpeechFacade

    def say(self, words):
        '''Say the words.
        '''
        self.__speech_facade.say(words)

    def say_status(self):
        '''Say the device status.
        '''
        self.__speech_facade.say(self.__get_status_string())

    def __get_source(self):
        '''Get the current source.

        Returns:
            The source.
        '''
        return self.__play_facade.get_source()

    def __get_time_string(self):
        '''Get the current time.

        Returns:
            The current time as a pronounceable string.
        '''
        local_time = self.__clock_facade.get_time()
        hours = local_time.hour
        minutes = local_time.minute
        time_string = "The time is now {0:d} hours and {1:d} minutes".format(hours, minutes)

        return time_string

    def __get_status_string(self):
        '''Get the current state.

        Returns:
            A string description of the current state.
        '''
        # Time.
        status_string = self.__get_time_string()
        status_string += "."

        # Chimes.
        if self.__clock_facade.chimes_are_on():
            status_string += " The chimes are on."
        else:
            status_string += " The chimes are off."

        # Alarm.
        if self.__alarm_facade.is_on():
            status_string += " The alarm is on and set for "
            local_time = self.__alarm_facade.get_alarm_time()
            hours = local_time.hour
            minutes = local_time.minute
            time_string = "{0:d} hours and {1:d} minutes.".format(hours, minutes)
            status_string += time_string
        else:
            status_string += " The alarm is off."

        # Audio source.
        status_string += " The audio source is set to "
        status_string += self.__get_source()
        status_string += "."

        return status_string

    def say_key_press(self, key_press):
        '''Say the key press.

        Args:
            key_press - The key press to say.
        '''
                # Create key mappings.
        menu_key_mappings = {
            KeysEnum.KEY_UP: "Up",
            KeysEnum.KEY_DOWN: "Down",
            KeysEnum.KEY_LEFT: "Left",
            KeysEnum.KEY_RIGHT: "Right",
            KeysEnum.KEY_ENTER: "Enter",
            KeysEnum.KEY_BACK: "Undo",
            KeysEnum.KEY_KP0: "Zero",
            KeysEnum.KEY_KP1: "One",
            KeysEnum.KEY_KP2: "Two",
            KeysEnum.KEY_KP3: "Three",
            KeysEnum.KEY_KP4: "Four",
            KeysEnum.KEY_KP5: "Five",
            KeysEnum.KEY_KP6: "Six",
            KeysEnum.KEY_KP7: "Seven",
            KeysEnum.KEY_KP8: "Eight",
            KeysEnum.KEY_KP9: "Nine"
        }

        if key_press in menu_key_mappings:
            self.say(menu_key_mappings[key_press])
