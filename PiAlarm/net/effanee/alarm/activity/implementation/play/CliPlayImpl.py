# pylint: disable=invalid-name
'''
Created on 23 Feb 2015

@author: fjh
'''
import logging
import ConfigParser
import urllib2
import atexit
import os
import platform
from urlparse import urlparse

from net.effanee.alarm.activity.implementation.play.aplayCli import aplayCli
from net.effanee.alarm.activity.implementation.play.mpcCli import mpcCli
from net.effanee.alarm.activity.implementation.play.PlaySourceImpl import PlaySourceImpl
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum

class CliPlayImpl(object):
    # pylint: disable=line-too-long
    '''Use this class as a set of instance methods, and wrap it in a AudioFacade
    facade.

    This class should be instantiated.

    This implementation plays BBC MP3 streams, described in pls files, and mpd
    play lists. Both via mpd. At the time of implementation the BBC were
    retiring their MP3 streams in favour of ACC via HLS, described in m3u8
    files.

    The BBC MP3 streams hard coded into this implementation are:-

        Radio 1
        Radio 2
        Radio 3
        Radio 4

    and the mpd play list:-

        AlarmClock

    is expected to exist and is used as the default alarm audio.

    This implementation uses the mpcCli and aplayCli classes to play audio via
    shell commands issued in a new process.

    All public methods refer to the use of mpd, other than:-

        play_over_async
        play_over_sync

    which refer to aplay.

    Methods can be called directly, or called via selected key presses, passed
    via the activate() method.

    Redundant - mpd2 would not pause and resume local mp3 files correctly.
    Played via mpd, (http://www.musicpd.org/), using the mpd2 Python bindings,
    (http://pythonhosted.org//python-mpd2/topics/commands.html and
    http://linux.die.net/man/1/mpc).

    This class should NOT be assumed to be thread safe, and will behave
    unpredictably if multiple instances are accessed via different threads.
    '''

    logger = logging.getLogger(__name__)

    # BBC MP3 encoded Shoutcast streams - likely to close during 2015!
    # pylint: disable=line-too-long
    RADIO1 = ("Radio 1",
              "http://www.radiofeeds.co.uk/bbcradio1.pls", True)
    RADIO2 = ("Radio 2",
              "http://www.radiofeeds.co.uk/bbcradio21.pls", True)
    RADIO3 = ("Radio 3",
              "http://www.radiofeeds.co.uk/bbcradio3.pls", True)
    RADIO4 = ("Radio 4",
              "http://www.radiofeeds.co.uk/bbcradio4fm.pls", True)

    # Local mpd play list, used as default alarm audio.
    ALARMCLOCK = ("Alarm Clock", "AlarmClock", False)

    # Defaults.
    DEFAULT_SOURCE_NAME = "Alarm Clock"
    DEFAULT_VOLUME = 50

    # Status enum's.
    PLAYING = "playing"
    STOPPED = "stopped"
    PAUSED = "paused"

    def __init__(self):
        '''
        Constructor
        '''
        # Create key mappings.
        self.__audio_key_mappings = {
            KeysEnum.KEY_VOLUMEDOWN: self.volume_down,
            KeysEnum.KEY_PLAYPAUSE: self.play_pause,
            KeysEnum.KEY_VOLUMEUP: self.volume_up,
            KeysEnum.KEY_STOP: self.stop
            }

        # Set sources.
        self.__sources = self.__create_sources()
        self.__source = self.__default_source()

        # Create mpd client.
        # pylint: disable=no-member
        self.__mpd = self.__create_mpd_client()
        self.__set_volume_from_mpd()
        self.__status = CliPlayImpl.STOPPED
        # pylint: disable=logging-format-interpolation
        self.__class__.logger.debug("Set status to {0}, set volume to {1}, set source to {2}."
                                    .format(self.__status, self.__volume, self.__source))

        # Create aplay client.
        self.__aplay = self.__create_aplay_client()

        # Register clean up.
        atexit.register(self.__destroy_clients)

        self.__class__.logger.debug("Initialised.")

    def init(self):
        '''Initialise this class.
        '''
        pass

    def activate(self, key_press):
        '''Activity interface method. Uses the audio_key_mappings map to
        select which method to invoke.

        Args:-
            key_press: the internal enum representing the key press code.

        Returns:
            False.
        '''
        # pylint: disable=logging-format-interpolation
        if key_press in self.__audio_key_mappings:
            self.__class__.logger.debug("Activating key press {0} in activity {1}."
                                        .format(key_press, self.__class__.__name__))
            self.__audio_key_mappings[key_press]()
        else:
            self.__class__.logger.warn("Key press {0}, in activity {1}, "
                                       "does not map to an interface method."
                                       .format(key_press, self.__class__.__name__))

        return False

    # pylint: disable=no-self-use
    def __create_mpd_client(self):
        '''Create an mpc client.

        Returns:
            An mpc client.
        '''
        path = ""

        if platform.system() == "Windows":

            this_path = os.path.dirname(os.path.abspath(__file__))
            mpc_path = this_path + '/../../../test/mocks/mpc/mpc'
            path = os.path.abspath(mpc_path)
            client = mpcCli(path)

        else:
            path = "mpc"
            client = mpcCli(path)

        # pylint: disable=logging-format-interpolation
        self.__class__.logger.debug("Set mpc path to {0}"
                                    .format(path))
        self.__class__.logger.info("Created mpc client.")
        return client

    # pylint: disable=no-self-use
    def __create_aplay_client(self):
        '''Create an aplay client.

        Returns:
            An aplay client.
        '''
        path = ""

        if platform.system() == "Windows":

            this_path = os.path.dirname(os.path.abspath(__file__))
            aplay_path = this_path + '/../../../test/mocks/aplay/aplay'
            path = os.path.abspath(aplay_path)
            client = aplayCli(path)

        else:
            path = "aplay"
            client = aplayCli(path)

        # pylint: disable=logging-format-interpolation
        self.__class__.logger.debug("Set aplay path to {0}"
                                    .format(path))
        self.__class__.logger.info("Created aplay client.")
        return client

    def __destroy_clients(self):
        '''Callback, used during system exit, to tidy up.
        '''
        # Inform clients of closure.
        self.__destroy_mpd_client()
        self.__destroy_aplay_client()

        self.__class__.logger.info("Exiting.")

    def __destroy_mpd_client(self):
        '''Callback, used during system exit, to tidy up.
        '''
        # Inform mpd of closure.
        if not self.__mpd is None:
            self.stop()
            self.__class__.logger.info("Stopped mpd.")

    def __destroy_aplay_client(self):
        '''Callback, used during system exit, to tidy up.
        '''
        if not self.__aplay is None:
            # Interrupt executing aplay command.
            self.__aplay.stop()

        self.__class__.logger.info("Stopped aplay.")

    @classmethod
    def __create_sources(cls):
        '''Create a list of audio sources.

        Returns:
            A list of PlaySourceImpl.
        '''
        sources = []
        sources.append(PlaySourceImpl(cls.RADIO1[0], cls.RADIO1[1], cls.RADIO1[2]))
        sources.append(PlaySourceImpl(cls.RADIO2[0], cls.RADIO2[1], cls.RADIO2[2]))
        sources.append(PlaySourceImpl(cls.RADIO3[0], cls.RADIO3[1], cls.RADIO3[2]))
        sources.append(PlaySourceImpl(cls.RADIO4[0], cls.RADIO4[1], cls.RADIO4[2]))
        sources.append(PlaySourceImpl(cls.ALARMCLOCK[0], cls.ALARMCLOCK[1], cls.ALARMCLOCK[2]))

        return sources

    def __default_source(self):
        '''Select a default audio source.

        Returns:
            The default audio source as defined by CliPlayImpl.DEFAULT_SOURCE_NAME,
            or if this cannot be found, None.
        '''
        return self.__get_source_by_name(CliPlayImpl.DEFAULT_SOURCE_NAME)

    def __get_source_by_name(self, name):
        '''Get the named audio source.

        Args:
            name: the name to search for.

        Returns:
            The audio source with the supplied name, or if this cannot be found
            None.
        '''
        for source in self.__sources:
            if source.get_name() == name:
                return source

        return None

    def __get_stream_uri(self, uri):
        '''Get the stream URI for the BBC Radio stream.

        Args:
            uri - The uri of the play list.

        Returns:
            The stream URI.

        Raises:
            HTTPError: when the server does not return a valid response.
            URLError: when URL cannot be opened.
            ConfigParser.Error: when the returned content is not in the expected
            format.
            ValueError: when the return stream URL is not in the expected
            format.
        '''
        stream_uri = ""
        # pylint: disable=logging-format-interpolation
        try:
            config = ConfigParser.SafeConfigParser()
            config.readfp(urllib2.urlopen(uri))
            # Get the first file from the retrieved play list.
            stream_uri = config.get("playlist", "file1")
        except urllib2.HTTPError as ex:
            self.__class__.logger.critical("Server error code {0}, message {1}"
                                           .format(ex.code, ex.message))

        except urllib2.URLError as ex:
            self.__class__.logger.critical("Server error {0}".format(ex))

        except ConfigParser.Error as ex:
            self.__class__.logger.critical("Parser error {0}".format(ex))

        except StandardError as ex:
            self.__class__.logger.critical("Error {0}".format(ex))

        #Roughly check the stream URI.
        if stream_uri:
            try:
                urlparse(stream_uri)
            except StandardError as ex:
                self.__class__.logger.critical("Parse error {0}".format(ex))
        else:
            message = "Stream URI could not be obtained from {0}".format(uri)
            self.__class__.logger.critical(message)
            raise ValueError(message)

        self.__class__.logger.info("Retrieved stream URI {0}".format(stream_uri))
        return stream_uri

    def __update_playlist(self):
        '''Clear the current play list and add the URI of the current source to
        the top.
        '''
        # pylint: disable=no-member
        self.__mpd.clear()

        stream_uri = ""
        if self.__source.is_stream():
            stream_uri = self.__get_stream_uri(self.__source.get_uri())
            self.__mpd.add(stream_uri)
        else:
            stream_uri = self.__source.get_uri()
            self.__mpd.load(stream_uri)

        # pylint: disable=logging-format-interpolation
        self.__class__.logger.info("Updated stream URI to {0}".format(stream_uri))

    def play_pause(self):
        '''Toggle playing of the source stream.
        '''
        if self.__status == CliPlayImpl.PAUSED:
            self.__play()

        elif self.__status == CliPlayImpl.PLAYING:
            self.__pause()

        elif self.__status == CliPlayImpl.STOPPED:
            self.__play()

    def is_playing(self):
        '''Is the audio playing?

        Returns:
            True - If the audio is playing, else False.
        '''
        return self.__status == CliPlayImpl.PLAYING

    def is_paused(self):
        '''Is the audio paused?

        Returns:
            True - If the audio is paused, else False.
        '''
        return self.__status == CliPlayImpl.PAUSED

    def is_stopped(self):
        '''Is the audio stopped?

        Returns:
            True - If the audio is stopped, else False.
        '''
        return self.__status == CliPlayImpl.STOPPED

    def __play(self):
        '''Play the current source stream.
        '''
        self.__class__.logger.debug("play")

        if self.__status == CliPlayImpl.PLAYING:

            return

        elif self.__status == CliPlayImpl.STOPPED:

            # Update the play list - in case the stream URI has changed.
            self.__update_playlist()
            # pylint: disable=no-member
            self.__mpd.play()
            self.__status = CliPlayImpl.PLAYING

        elif self.__status == CliPlayImpl.PAUSED:

            # pylint: disable=no-member
            self.__mpd.play()
            self.__status = CliPlayImpl.PLAYING

    def __pause(self):
        '''Pause the current source stream.
        '''
        self.__class__.logger.debug("pause")

        if self.__status == CliPlayImpl.PAUSED:

            return

        elif self.__status == CliPlayImpl.STOPPED:

            return

        elif self.__status == CliPlayImpl.PLAYING:

            # pylint: disable=no-member
            self.__mpd.pause()
            self.__status = CliPlayImpl.PAUSED

    def stop(self):
        '''Stop the current source stream.
        '''
        self.__class__.logger.debug("stop")

        if self.__status == CliPlayImpl.STOPPED:

            return

        elif self.__status == CliPlayImpl.PAUSED:

            # pylint: disable=no-member
            self.__mpd.stop()
            self.__mpd.clear()
            self.__status = CliPlayImpl.STOPPED

        elif self.__status == CliPlayImpl.PLAYING:

            # pylint: disable=no-member
            self.__mpd.stop()
            self.__mpd.clear()
            self.__status = CliPlayImpl.STOPPED

    def __set_volume_from_mpd(self):
        '''Try to get the current mpd volume level, and synchronise this objects
        volume to it.
        '''
        # pylint: disable=no-member
        output = self.__mpd.getvol()
        volume = self.__parse_mpd_output(output)

        if volume >= 0 and volume <= 100:
            self.__volume = volume
        else:
            self.__volume = CliPlayImpl.DEFAULT_VOLUME

    def __parse_mpd_output(self, output):
        '''Parse mpd output to get the volume level as an integer.

        Args:
            output - The output from mpd.

        Returns:
            The parsed value from the input, or if that fails the value or
            CliPlayImpl.DEFAULT_VOLUME
        '''
        # output - volume: 20%
        items = output.split(":")

        if (len(items) < 2) or (items[0] != ("volume")):
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.warning("Could not obtain volume from mpd output {0}, using default {1}"
                                          .format(output, CliPlayImpl.DEFAULT_VOLUME))

            return CliPlayImpl.DEFAULT_VOLUME

        try:
            value = items[1].strip()
            value = value.strip("%")
            volume = int(value)

            return volume

        except (TypeError, ValueError) as ex:
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.warning("Could not obtain volume from mpd {0}"
                                          .format(ex))

    def __set_volume(self):
        '''Set mpd volume.
        '''
        # pylint: disable=no-member
        self.__mpd.setvol(self.__volume)
        # pylint: disable=logging-format-interpolation

        self.__class__.logger.debug("Set mpd volume to {0}".format(self.__volume))
    def volume_up(self):
        '''Increase mpd volume by 10.
        '''
        self.__class__.logger.debug("volume_up")

        if self.__volume <= 90:
            # pylint: disable=no-member
            self.__volume += 10
            self.__set_volume()
        else:
            pass

    def volume_down(self):
        '''Decrease mpd volume by 10.
        '''
        self.__class__.logger.debug("volume_down")

        if self.__volume >= 10:
            # pylint: disable=no-member
            self.__volume -= 10
            self.__set_volume()
        else:
            pass

    def get_sources(self):
        '''Get this objects sources.

        Returns:
            A list of source names.
        '''
        self.__class__.logger.debug("get_sources")

        source_names = []
        for source in self.__sources:
            source_names.append(source.get_name())

        return source_names

    def set_source(self, name):
        '''Set this objects source.

        Args:
            name - The name of the source to select.
        '''
        self.__class__.logger.debug("set_source")

        source = self.__get_source_by_name(name)
        self.__source = source
        # pylint: disable=logging-format-interpolation
        self.__class__.logger.debug("Set source to {0}".format(self.__source))

    def get_source(self):
        '''Get this objects current source.

        Returns:
            The name of the current source.
        '''
        self.__class__.logger.debug("get_source")

        return self.__source.get_name()

    def play_over_sync(self, wav_file):
        '''Play a wav file over the top of the currently playing file/stream.

        Plays at 100% volume and blocks. If requested while a previous
        request is still playing, the previous request is first stopped.

        Args:
            wav_file: The path to the wav file.

        Returns:
            One of PlayFacade.RUNNING, PlayFacade.DONE, PlayFacade.CANCELLED.
            Cancelled is returned if an error occurs.
        '''
        return self.__aplay.play_sync(wav_file)

    def play_over_async(self, wav_file):
        '''Play a wav file over the top of the currently playing file/stream.

        Plays at 100% volume and does not block. If requested while a previous
        request is still playing, the previous request is first stopped.

        Args:
            wav_file: The path to the wav file.
        '''
        self.__aplay.play_async(wav_file)

    def __str__(self):
        '''Override string.
        '''
        return self.__class__.__name__ + "[" \
                "__status=" + self.__status + ", " \
                "__volume=" + str(self.__volume) + ", " \
                "__source=" + str(self.__source) + ", " \
                "__sources=" + ", ".join([str(source) for source in self.__sources]) \
                + "]"
