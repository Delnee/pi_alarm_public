'''
Created on 1 Mar 2015

@author: fjh
'''

import logging

class PlaySourceImpl(object):
    '''Defines an audio source.

    A source has a short name, a location URI and a stream indicator.

    Attributes:
        name: The name of this source. Short, so it can be pronounced quickly.
        uri: The location of the audio.
        is_stream: Is the source a local file or a remote stream?

    This class should be safe, when accessed concurrently by multiple
    threads, as it is immutable.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self, name, uri, is_stream):
        '''
        Constructor
        '''
        self.name = name
        self.uri = uri
        self.is_a_stream = is_stream
        # pylint: disable=logging-format-interpolation
        self.__class__.logger.info("Created audio source {0} with URI {1}, which is indirect {2}."
                                   .format(name, uri, is_stream))

    def get_uri(self):
        '''Get URI.

        Returns:
            The URI.
        '''
        return self.uri

    def get_name(self):
        '''Get name.

        Returns:
            The name.
        '''
        return self.name

    def is_stream(self):
        '''Is the URI pointing to a stream, or a local file?

        Returns:
            True if the source is a stream, else False.
        '''
        return self.is_a_stream

    def __str__(self):
        '''Override string.
        '''
        return self.__class__.__name__ + "[" \
                "name=" + self.name + ", " \
                "uri=" + self.uri + ", " \
                "is_a_stream=" + str(self.is_a_stream) \
                + "]"
