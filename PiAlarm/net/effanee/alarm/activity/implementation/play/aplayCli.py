# pylint: disable=invalid-name
'''
Created on 16 Mar 2015

@author: fjh
'''
import subprocess
import logging
from subprocess import CalledProcessError
import os
import signal
import threading
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade

# pylint: disable=too-few-public-methods
class aplayCli(object):
    '''aplay command line interface implementation, using shell commands, issued
    in a new process.

    Only one aplay process may be active at a time. If an aplay process is
    active when a new play_async/play_sync request is received, the playing
    process is terminated.

    This class should be safe, when accessed concurrently by multiple
    threads.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self, aplay_path):
        '''Constructor
        '''
        self.__aplay_path = aplay_path
        self.__process = None
        self.__status = PlayFacade.DONE
        self.__lock = threading.RLock()

    def play_sync(self, wav_file):
        '''Play the wav file. This method does block, but it will stop an
        existing playing instance, before starting to play the new sound.

        Args:
            wav_file - The wav file to play.

        Returns:
            One of PlayFacade.RUNNING, PlayFacade.DONE, PlayFacade.CANCELLED.
            Cancelled is returned if an error occurs.
        '''
        self.__lock.acquire()

        if self._is_playing():
            self._cancel()

        self.__execute(wav_file, True)

        return self.__status

    def play_async(self, wav_file):
        '''Play the wav file. This method does not block, but it will stop an
        existing playing instance, before starting to play the new sound.

        Args:
            wav_file - The wav file to play.
        '''
        self.__lock.acquire()

        if self._is_playing():
            self._cancel()

        self.__execute(wav_file, False)

    def _is_playing(self):
        '''Is aplay playing?

        Returns:
            True if playing, else False.
        '''
        if self.__process is None:
            return False

        self.__process.poll()
        if self.__process.returncode is None:
            return True

        else:
            return False

    def _cancel(self):
        '''Kill a running process, i.e. before it completes playing normally.
        '''
        # Try to kill the process.
        try:
            os.kill(self.__process.pid, signal.SIGINT)

        except StandardError as ex:
            if ex.args[0] == 5:
                # The exception - (5, 'Access is denied') is thrown by Windows
                # when, the kill function is called on a process that has
                # already exited.
                return
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.critical("Could not cancel playing, exception {0}"
                                           .format(ex))

    def __execute(self, wav_file, play_sync):
        '''Play the supplied wav file. Blocks until the wav file has been
        played.

        Args:
            wav_file - The path to the wav file to play.
            play_sync - If True, the wav file is played synchronously, else
            asynchronously.

        Raises:
            CalledProcessError
        '''
        command_line = [self.__aplay_path, wav_file, "-q"]
        try:
            self.__process = subprocess.Popen(command_line, close_fds=True)
            self.__lock.release()
            self.__status = PlayFacade.RUNNING

        except CalledProcessError as ex:
            self.__lock.release()
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.critical("Could not play the aplay wav file {0}, exception {1}"
                                           .format(wav_file, ex))
            self.__status = PlayFacade.CANCELLED
            return

        # Play synchronously?
        if play_sync:
            return_code = self.__process.wait()
            if return_code != 0:
                self.__status = PlayFacade.CANCELLED
            else:
                self.__status = PlayFacade.DONE

    def stop(self):
        '''Tidy up when the application closes.
        '''
        self.__lock.acquire()

        if self._is_playing():
            self._cancel()

        self.__lock.release()

#     def __execute(self, wav_file):
#         '''Play the supplied aplay file.
#
#         Args:
#             wav_file - The path to the wav file to play.
#
#         Raises:
#             CalledProcessError
#         '''
#         output = ""
#         command_line = [self.__aplay_path, "-q", wav_file]
#         try:
#             # pylint: disable=line-too-long
#             output = subprocess.check_output(command_line, stdin=None, stderr=None, shell=False)
#
#         except CalledProcessError as ex:
#             # pylint: disable=logging-format-interpolation
#             self.__class__.logger.critical("Could not play the aplay wav file {0}, exception {1}"
#                                            .format(wav_file, ex))
#             raise ex
#
#         return output
        