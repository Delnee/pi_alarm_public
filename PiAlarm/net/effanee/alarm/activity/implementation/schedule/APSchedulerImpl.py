'''
Created on 12 Mar 2015

@author: fjh
'''
import logging
import datetime
import atexit
import pytz

from apscheduler.schedulers.background import BackgroundScheduler

# pylint: disable=abstract-class-not-used
class APSchedulerImpl(object):
    '''Use this class as a set of instance methods, and wrap it in a
    SchedulerFacade facade.

    This class should not be instantiated. It is initialised using the init()
    class method, which this class calls itself, when the class is first
    referenced. The init() method never needs to be called directly.

    This class schedules and manages repeating tasks.

    This class in NOT safe, when accessed concurrently by multiple threads.
    In the current application, this class is only accessed by a single thread.
    '''

    logger = logging.getLogger(__name__)

    __scheduler = None
    __every_min_jobs = []
    __every_five_mins_jobs = []
    __every_hour_jobs = []

    _paused_jobs = []
    _alarm_job = None

    # Spread out the recurring jobs so that they don't overlap. these are the
    # seconds past the hour that the jobs will run.
    EVERY_MINUTE_JOB = 20
    EVERY_FIVE_MINUTES_JOB = 40

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def init(cls):
        '''Initialise this class.
        '''
        cls.__scheduler = BackgroundScheduler()
        cls.__scheduler.configure(timezone=pytz.timezone("Europe/London"))
        cls.__scheduler.start()

        # Every minute.
        next_run_slot = cls.__get_next_min_time_slot(cls.EVERY_MINUTE_JOB)
        # pylint: disable=line-too-long
        cls.__scheduler.add_job(cls.__every_min_job, trigger="interval", seconds=60, next_run_time=next_run_slot, max_instances=1)
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Created every minute job, which will run first at {0}".format(next_run_slot))

        #Every five minutes.
        next_run_slot = cls.__get_next_min_time_slot(cls.EVERY_FIVE_MINUTES_JOB)
        # pylint: disable=line-too-long
        cls.__scheduler.add_job(cls.__every_five_mins_job, trigger="interval", seconds=(60 * 5), next_run_time=next_run_slot, max_instances=1)
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Created every five minutes job, which will run first at {0}".format(next_run_slot))

        # Every hour.
        # pylint: disable=line-too-long
        cls.__scheduler.add_job(cls.__every_hour_job, trigger="cron", hour="8-22", max_instances=1)
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Created every hour job")

        # Tidy on exit.
        atexit.register(cls.__finalise)

        cls.logger.debug("Initialised.")

    @classmethod
    def create_alarm_job(cls, job, alarm_time):
        '''Create an alarm job to run at the supplied time each day.

        Args:
            job - The method to invoke when the alarm is triggered.
            alarm_time - The time the alarm should trigger.
        '''
        if cls._alarm_job:
            cls.remove_alarm_job()

        # Get todays date.
        now_datetime = datetime.datetime.today()

        # Get the time now.
        now_time = now_datetime.time()

        if alarm_time > now_time:
            # Todays alarm time has not passed, so set it to activate today.
            new_alarm_time = datetime.datetime.combine(now_datetime.date(), alarm_time)
        else:
            # Todays alarm time has passed, so set it to activate tomorrow.
            tomorrow_datetime = now_datetime + datetime.timedelta(days=1)
            new_alarm_time = datetime.datetime.combine(tomorrow_datetime.date(), alarm_time)

        # pylint: disable=line-too-long
        cls._alarm_job = cls.__scheduler.add_job(job, trigger="interval", days=1, next_run_time=new_alarm_time, max_instances=1)

    @classmethod
    def remove_alarm_job(cls):
        '''Remove the singular alarm job.
        '''
        if cls._alarm_job:
            cls._alarm_job.remove()
            cls._alarm_job = None

    @classmethod
    def __get_next_min_time_slot(cls, offset):
        '''Get the next time slot.

        Args:
            offset - The number of seconds past the hour for the next time slot.

        Returns:
            The next time slot, which is the number of seconds past the hour, in
            the next minute. i.e.

            time now        next time slot
            10:30:05        10:31:15
            10:30:27        10:31:15
            10:30:50        10:31:15
        '''
        # int(math.ceil(num / 15.0)) * 15
        now = datetime.datetime.now()
        seconds = now.second
        next_run = (60 - seconds) + offset
        return now + datetime.timedelta(0, next_run, -now.microsecond)

    @classmethod
    def __every_min_job(cls):
        '''A job that is run every minute.

        Previously registered methods are invoked in sequence when this job is
        run.
        '''
        for job in cls.__every_min_jobs:
            try:
                job()
            except StandardError as ex:
                # pylint: disable=logging-format-interpolation
                cls.logger.error("Could not invoke job {0},"
                                 " exception is {1}".format(job, ex))

    @classmethod
    def register_every_min_job(cls, job):
        '''Register a new method to be invoked each minute.

        Args:
            job - A method name to be invoked.
        '''
        if not job in cls.__every_min_jobs:
            cls.__every_min_jobs.append(job)
            # pylint: disable=logging-format-interpolation
            cls.logger.debug("Added job {0}".format(job))

    @classmethod
    def unregister_every_min_job(cls, job):
        '''Unregister an existing method.

        Args:
            job - The method name to unregister.
        '''
        if job in cls.__every_min_jobs:
            cls.__every_min_jobs.remove(job)
            # pylint: disable=logging-format-interpolation
            cls.logger.debug("Removed job {0}".format(job))

    @classmethod
    def register_paused_job(cls, job):
        '''Register a new method to be invoked each minute, when the audio is
        paused, to act as a reminder to the user.

        Args:
            job - A method name to be invoked.
        '''
        cls.register_every_min_job(job)
        cls._paused_jobs.append(job)

    @classmethod
    def unregister_paused_jobs(cls):
        '''Unregister all jobs associated with the audio being paused.
        '''
        for job in cls._paused_jobs:
            cls.unregister_every_min_job(job)

    @classmethod
    def __every_five_mins_job(cls):
        '''A job that is run every five minutes.

        Previously registered methods are invoked in sequence when this job is
        run.
        '''
        for job in cls.__every_five_mins_jobs:
            try:
                job()
            except StandardError as ex:
                # pylint: disable=logging-format-interpolation
                cls.logger.error("Could not invoke job {0},"
                                 " exception is {1}".format(job, ex))

    @classmethod
    def register_every_five_mins_job(cls, job):
        '''Register a new method to be invoked every five minutes.

        Args:
            job - A method name to be invoked.
        '''
        if not job in cls.__every_five_mins_jobs:
            cls.__every_five_mins_jobs.append(job)
            # pylint: disable=logging-format-interpolation
            cls.logger.debug("Added job {0}".format(job))

    @classmethod
    def unregister_every_five_mins_job(cls, job):
        '''Unregister an existing method.

        Args:
            job - The method name to unregister.
        '''
        if job in cls.__every_five_mins_jobs:
            cls.__every_five_mins_jobs.remove(job)
            # pylint: disable=logging-format-interpolation
            cls.logger.debug("Removed job {0}".format(job))

    @classmethod
    def __every_hour_job(cls):
        '''A job that is run every hour.

        Previously registered methods are invoked in sequence when this job is
        run.
        '''
        for job in cls.__every_hour_jobs:
            try:
                job()
            except StandardError as ex:
                # pylint: disable=logging-format-interpolation
                cls.logger.error("Could not invoke job {0},"
                                 " exception is {1}".format(job, ex))

    @classmethod
    def register_every_hour_job(cls, job):
        '''Register a new method to be invoked every hour.

        Args:
            job - A method name to be invoked.
        '''
        if not job in cls.__every_hour_jobs:
            cls.__every_hour_jobs.append(job)
            # pylint: disable=logging-format-interpolation
            cls.logger.debug("Added job {0}".format(job))

    @classmethod
    def unregister_every_hour_job(cls, job):
        '''Unregister an existing method.

        Args:
            job - The method name to unregister.
        '''
        if job in cls.__every_hour_jobs:
            cls.__every_hour_jobs.remove(job)
            # pylint: disable=logging-format-interpolation
            cls.logger.debug("Removed job {0}".format(job))

    @classmethod
    def now_job(cls, job):
        '''?
        '''
        raise NotImplementedError()

    @classmethod
    def __finalise(cls):
        '''Shutdown the scheduler.
        '''
        if cls.__scheduler.running:
            cls.__scheduler.shutdown()
        cls.logger.debug("Exiting.")
