# pylint: disable=invalid-name
'''
Created on 4 Mar 2015

@author: fjh
'''

import logging

# Annoying conditional imports used for testing on Windows, before deploying
# on the Raspberry Pi.
try:
    # This is the actual library used to access eSpeak on the Raspberry Pi,
    # which is installed into the local Python environment.
    from espeak import espeak

except ImportError:
    # This is the mock library used to access eSpeak in the development
    # environment, which is PyDev/Eclipse on Windows.
    from net.effanee.alarm.test.mocks.espeak.eSpeak import espeak

class eSpeakImpl(object):
    '''Use this class as a set of instance methods, and wrap it in a
    SpeechFacade facade.

    This class should not be instantiated. It is initialised using the init()
    class method, which this class calls itself, when the class is first
    referenced. The init() method never needs to be called directly.

    This class uses the espeak Python library to speak words. Available from:-

        https://launchpad.net/python-espeak

    This class in NOT safe, when accessed concurrently by multiple threads.
    In the current application, this class is only accessed by a single thread.
    '''

    logger = logging.getLogger(__name__)

    VOICE = "english_rp+f5"
    RATE = 160
    CAPITALS = 10

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def init(cls):
        '''Initialise this class.
        '''
        espeak.set_voice(cls.VOICE)
        espeak.set_parameter(espeak.Parameter.Rate, cls.RATE)
        espeak.set_parameter(espeak.Parameter.Capitals, cls.CAPITALS)
        espeak.set_parameter(espeak.Parameter.Pitch, 60)
        # espeak.set_parameter(espeak.Parameter.Volume, 50)

        cls.logger.debug("Initialised.")

    @classmethod
    def activate(cls, words):
        '''Activity interface method.

        Args:
            words - The words to say.

        Returns:
            False.
        '''
        # pylint: disable=logging-format-interpolation
        cls.say(words)

        return False

    @classmethod
    def say(cls, words):
        '''Say the words.

        Args:
            words - The words to say.
        '''
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Speaking {0}".format(words))

        if espeak.is_playing():
            espeak.cancel()

        espeak.synth(words)

    @classmethod
    def say_list(cls, word_list):
        '''Say the words.

        Args:
            word_list - The words to say.
        '''
        for words in word_list:
            cls.say(words)
