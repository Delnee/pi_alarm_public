'''
Created on 18 Mar 2015

@author: fjh
'''
import logging
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum
from array import array
import time
import datetime

class MultipleInputImpl(object):
    '''Use this class as a set of class methods, and wrap it in a
    MultipleInputFacade facade.

    This class should not be instantiated. It is initialised using the init()
    class method, which this class calls itself, when the class is first
    referenced. The init() method never needs to be called directly.

    This class references the non-standard library class:-

        net.effanee.alarm.input.lirc.KeysEnum

    It is used to collect multiple consecutive key presses from an IR remote
    control. At the moment it just collects a four digit time.

    It operates by implementing the call-back method activate(), which is passed
    a key press enum. Activate then passes the key press enum to the "active
    method" for further processing. The "active method" is created, by calling
    the set_alarm_time() method. When the "active method" returns true, the
    input has been fully processed and the result can be retrieved using the
    appropriate method, in this example get_alarm_time().

    The sequence is therefore:-

        set_alarm_time()
        activate(key_press_enum)    -    Delegates to "active method" until True.
        get_alarm_time()

    This class in NOT safe, when accessed concurrently by multiple threads.
    In the current application, this class is only accessed by a single thread.
    '''

    logger = logging.getLogger(__name__)

    # Initialise the time with a default value.
    _alarm_time = datetime.time(7, 30, 0)

    # Collect the input.
    _input = array("i")

    # The currently active method, or None.
    _active_method = None

    # Bind a set of KeyEnum constants to our preferred representation.
    _key_mappings = {
        KeysEnum.KEY_BACK : None,
        KeysEnum.KEY_KP0 : 0,
        KeysEnum.KEY_KP1 : 1,
        KeysEnum.KEY_KP2 : 2,
        KeysEnum.KEY_KP3 : 3,
        KeysEnum.KEY_KP4 : 4,
        KeysEnum.KEY_KP5 : 5,
        KeysEnum.KEY_KP6 : 6,
        KeysEnum.KEY_KP7 : 7,
        KeysEnum.KEY_KP8 : 8,
        KeysEnum.KEY_KP9 : 9
    }

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def init(cls):
        '''Initialise this class.
        '''
        cls.logger.debug("Initialised.")

    @classmethod
    def activate(cls, key_press):
        '''Activity interface method. Calls the appropriate local method, which
        is actively processing key presses when this method is called.

        Args:-
            key_press - The internal enum representing the key press code.

        Returns:
            False - if the processing is not complete.
            True - if the processing is complete, or no method is currently
            processing key presses.

        Raises:
            Nothing
        '''
        # pylint: disable=logging-format-interpolation
        if cls._active_method is None:
            return True

        else:
            cls.logger.debug("Invoking method {0} with key press {1}."
                             .format(cls._active_method.__name__, key_press))
            return cls._active_method(key_press)

    @classmethod
    def set_alarm_time(cls):
        '''Set the 24 hour time of the alarm.
        '''
        if cls._active_method is None:
            cls._active_method = cls.__set_24_hour_time

    @classmethod
    def get_alarm_time(cls):
        '''Get the 24 hour time of the alarm.

        Returns:
            The last valid alarm time input.
        '''
        return cls._alarm_time

    @classmethod
    def init_alarm_time(cls, alarm_time):
        '''Initialise the default alarm time. If not initialised, the default
        time of 07:30 is used.

        Args:
            alarm_time - A default time.
        '''
        cls._alarm_time = alarm_time

    @classmethod
    # pylint: disable=too-many-branches, too-many-return-statements
    def __set_24_hour_time(cls, key_enum):
        '''Get the four digits of a 24 hour time.

            State:-
                        -------Keys------
            Array   |   0 - 9        Back
            -----   |   -----        ----
            None    |     Never Reached
            0       |   Append       Leave
            1       |   Append       Remove Last
            2       |   Append       Remove Last
            3       |   Append/Set   Remove Last
            4+      |     Never Reached

        Returns:
            True - If the time is set, or back is received on an empty list.
            False - If the time is not set, and more key presses are required.
        '''
        # Unsupported key.
        if key_enum not in cls._key_mappings:
            # pylint: disable=logging-format-interpolation
            cls.logger.debug("Key press {0} is invalid when setting the alarm time."
                             .format(key_enum))
            return False

        # Process the key press.
        if len(cls._input) == 0:
            if key_enum == KeysEnum.KEY_BACK:
                cls.__leave()
                return True

            else:
                cls.__append(key_enum)
                return False

        elif len(cls._input) > 0 and len(cls._input) < 4:
            if key_enum == KeysEnum.KEY_BACK:
                cls.__remove_last()
                return False

            else:
                cls.__append(key_enum)
                if len(cls._input) == 4:
                    cls.__set()
                    return True
                else:
                    return False

    @classmethod
    def __remove_last(cls):
        '''Remove the last item from the time array.
        '''
        cls._input.pop(-1)
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Removed last key press.")

    @classmethod
    def __append(cls, key_enum):
        ''' Add the value of the key enum to the time array.

        Args:
            key_enum - The key enum of the value to append.
        '''
        cls._input.append(cls._key_mappings[key_enum])
        if cls.__validate_time():
            # pylint: disable=logging-format-interpolation
            cls.logger.debug("Added key {0} to alarm time.".format(key_enum))

        else:
            cls.__remove_last()
            cls.__noop(key_enum)

    @classmethod
    def __noop(cls, key_enum):
        '''Called when an inappropriate key is pressed.

        Args:
            key_enum - The incorrect key enum.
        '''
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Key {0} is not valid at this time.".format(key_enum))

    @classmethod
    def __set(cls):
        '''Set the alarm time.
        '''
        string_time = "".join([str(digit) for digit in cls._input]) + "00"
        # If the date part of the time is left blank, the defaults make the
        # resultant datetime unparseable by the mktime() method.
        time_tuple = time.strptime("01012014-"+ string_time, "%d%m%Y-%H%M%S")
        cls._alarm_time = datetime.datetime.fromtimestamp(time.mktime(time_tuple)).time()
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Alarm time set to {0}.".format(cls._alarm_time))

        cls._input = array("i")
        cls._active_method = None

    @classmethod
    def __leave(cls):
        '''Exit the collection process without setting a new time.
        '''
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Alarm time not set.")
        cls._active_method = None

    @classmethod
    def __validate_time(cls):
        '''Validate the time array.

        Check the digits at each location match the restrictions:-

            00:00
            0        0 -> 2
             0       0 -> 9
               0     0 -> 5
                0    0 -> 9

        Return:
            True - If the criteria is met, or the array is empty.
        '''
        is_valid = False
        length = len(cls._input)

        if length == 0:
            is_valid = True

        if length > 0:
            is_valid = cls._input[0] in range(3)

        if length > 1:
            is_valid = cls._input[1] in range(10)

        if length > 2:
            is_valid = cls._input[2] in range(6)

        if length > 3:
            is_valid = cls._input[1] in range(10)

        return is_valid
