'''
Created on 15 Apr 2015

@author: fjh
'''
import logging
import os
import ConfigParser
import datetime

class PersistImpl(object):
    '''Use this class as a set of class methods, and wrap it in a
    PersistFacade facade.

    This class should not be instantiated. It is initialised using the init()
    class method, which this class calls itself, when the class is first
    referenced. The init() method never needs to be called directly.

    This class stores information that is required between restarts.

    This class in NOT safe, when accessed concurrently by multiple threads.
    In the current application, this class is only accessed by a single thread.
    '''

    logger = logging.getLogger(__name__)

    CONFIG_FILE_PATH = "/../../../config/persist.cfg"

    _persist_file = None

    # Configuration file identifiers.
    # pylint: disable=invalid-name
    ALARM_SECTION = "alarm"
    ALARM_IS_ON_ITEM = "_is_on"
    ALARM_ALARM_TIME_ITEM = "_alarm_time"

    CLOCK_SECTION = "clock"
    CLOCK_IS_ON_ITEM = "_chimes_are_on"

    MULTIPLE_INPUT_SECTION = "multipleinput"
    MULTIPLE_INPUT_INIT_ALARM_TIME_ITEM = "_init_alarm_time"

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def init(cls):
        '''Initialise this class.
        '''
        cls._set_persist_file()

        cls.logger.debug("Initialised.")

    @classmethod
    def _set_persist_file(cls):
        '''Get and set the file used to persist the configuration.
        '''
        this_path = os.path.dirname(os.path.abspath(__file__))
        cls._persist_file = os.path.abspath(str(this_path + cls.CONFIG_FILE_PATH))

        # pylint: disable=logging-format-interpolation
        cls.logger.info("Selected the following persist configuration file {0}"
                        .format(cls._persist_file))

    @classmethod
    def _retrieve_config(cls):
        '''Retrieve the configuration.

        Return:
            The configuration read from the file system.
        '''
        config = ConfigParser.ConfigParser()
        config.readfp(open(cls._persist_file))

        return config

    @classmethod
    def _store_config(cls, config):
        '''Store the configuration.

        Args:
            config - The configuration file.
        '''
        with open(cls._persist_file, "w") as config_file:
            config.write(config_file)

    @classmethod
    def _string_to_time(cls, string_time):
        '''Convert string to time.

        Returns:
            The supplied string time as a time.
        '''
        return datetime.datetime.strptime(string_time, "%H:%M:%S").time()

    @classmethod
    def set_alarm_alarm_time(cls, alarm_time):
        '''Set the 24 hour time of the alarm.

        Args:
            alarm_time - A time indicating the new state of the alarm time.
        '''
        config = cls._retrieve_config()
        config.set(cls.ALARM_SECTION, cls.ALARM_ALARM_TIME_ITEM, str(alarm_time))
        cls._store_config(config)

    @classmethod
    def get_alarm_alarm_time(cls):
        '''Get the 24 hour time of the alarm.

        Returns:
            A time indicating the current state of the alarm time.
        '''
        config = cls._retrieve_config()
        string_time = config.get(cls.ALARM_SECTION, cls.ALARM_ALARM_TIME_ITEM)

        return cls._string_to_time(string_time)

    @classmethod
    def set_alarm_is_on(cls, is_on):
        '''Set the is on status of the alarm.

        Args:
            is_on - A boolean indicating the new state of the alarm.
        '''
        config = cls._retrieve_config()
        config.set(cls.ALARM_SECTION, cls.ALARM_IS_ON_ITEM, str(is_on))
        cls._store_config(config)

    @classmethod
    def get_alarm_is_on(cls):
        '''Get the is on status of the alarm.

        Returns:
            A boolean indicating the current state of the alarm.
        '''
        config = cls._retrieve_config()
        return config.getboolean(cls.ALARM_SECTION, cls.ALARM_IS_ON_ITEM)

    @classmethod
    def set_clock_chimes_are_on(cls, chimes_are_on):
        '''Set the is on status of the chimes.

        Args:
            chimes_are_on - A boolean indicating the new state of the chimes.
        '''
        config = cls._retrieve_config()
        config.set(cls.CLOCK_SECTION, cls.CLOCK_IS_ON_ITEM, str(chimes_are_on))
        cls._store_config(config)

    @classmethod
    def get_clock_chimes_are_on(cls):
        '''Get the is on status of the chimes.

        Returns:
            A boolean indicating the current state of the chimes.
        '''
        config = cls._retrieve_config()
        return config.getboolean(cls.CLOCK_SECTION, cls.CLOCK_IS_ON_ITEM)

#     @classmethod
#     # pylint: disable=invalid-name
#     def set_multipleinput_init_alarm_time(cls, init_alarm_time):
#         '''Set the initial alarm time of the multiple input.
#
#         Args:
#             init_alarm_time - A time indicating the new state of the init alarm
#             time.
#         '''
#         config = cls._retrieve_config()
#         # pylint: disable=line-too-long
#         config.set(cls.MULTIPLE_INPUT_SECTION, cls.MULTIPLE_INPUT_INIT_ALARM_TIME_ITEM, str(init_alarm_time))
#         cls._store_config(config)

    @classmethod
    def get_multipleinput_init_alarm_time(cls):
        '''Get the initial alarm time of the multiple input.

        Returns:
            A time indicating the current state of the init alarm time.
        '''
        return cls.get_alarm_alarm_time()
