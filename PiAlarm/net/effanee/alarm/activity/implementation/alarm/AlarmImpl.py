'''
Created on 16 Mar 2015

@author: fjh
'''
import logging
import datetime
import os

class AlarmImpl(object):
    '''Use this class as a set of class methods, and wrap it in a AlarmFacade
    facade.

    This class should not be instantiated. It is initialised using the init()
    class method, which this class calls itself, when the class is first
    referenced. The init() method never needs to be called directly.

    The switch_on/switch_off methods act as state markers. They do not create
    or schedule an alarm job.

    It is intended that the wave file is played, before the selected audio
    source, when the alarm is triggered. As some of the audio sources, are
    network resources, it cannot be guaranteed they will be available when
    required. So, to prevent a totally silent alarm occurring, the locally
    stored wave file will be played first.

    This class should be safe, when accessed concurrently by multiple
    threads. Assuming that assignment is atomic. In the current application,
    this class is only accessed by a single thread.
    '''

    logger = logging.getLogger(__name__)

    # Played first when the alarm is triggered.
    WAV_FILE_PATH = "/../../../res/alarm/Wake Up.wav"

    # State.
    _is_on = False
    _alarm_time = datetime.time(7, 30, 0)
    _wav_file = None

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def init(cls):
        '''Initialise this class.
        '''
        cls._set_wav_file()

        cls.logger.debug("Initialised.")

    @classmethod
    def switch_on(cls):
        '''Switch the alarm on.
        '''
        cls._is_on = True

    @classmethod
    def switch_off(cls):
        '''Switch the alarm on.
        '''
        cls._is_on = False

    @classmethod
    def is_on(cls):
        '''Is the alarm on?

        Returns:
            True if the alarm is on, else False.
        '''
        return cls._is_on

    @classmethod
    def set_alarm_time(cls, alarm_time):
        '''Set the alarm time.

        Args:
            alarm_time - The time to set off the alarm.
        '''
        cls._alarm_time = alarm_time

    @classmethod
    def get_alarm_time(cls):
        '''Get the alarm time.

        Returns:
            The time of the alarm.
        '''
        return cls._alarm_time

    @classmethod
    def _set_wav_file(cls):
        '''Get and set the wav file used as the alarm.
        '''
        this_path = os.path.dirname(os.path.abspath(__file__))
        cls._wav_file = os.path.abspath(str(this_path + cls.WAV_FILE_PATH))

        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Selected the following alarm wav {0}"
                         .format(cls._wav_file))

    @classmethod
    def get_wav_file(cls):
        '''Get the wav file used as the alarm.

        Returns:
            The alarm wave file.
        '''
        return cls._wav_file
