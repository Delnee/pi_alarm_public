'''
Created on 16 Mar 2015

@author: fjh
'''
import logging
import os
import glob
import random
import datetime

class ClockImpl(object):
    '''Use this class as a set of class methods, and wrap it in a ClockFacade
    facade.

    This class should not be instantiated. It is initialised using the init()
    class method, which this class calls itself, when the class is first
    referenced. The init() method never needs to be called directly.

    The chimes_on/chimes_off methods act as state markers. They do not create
    or schedule a chimes job.

    The chime wave files are stored within the application directory:-

        net/effanee/alarm/res/chimes/*.wav.

    Additional wave files stored in this location will be eligible for random
    selection when a wave file is required. Chimes should generally be under
    20 seconds in duration.

    This class should be safe, when accessed concurrently by multiple
    threads. Assuming that assignment is atomic. In the current application,
    this class is only accessed by a single thread.
    '''

    logger = logging.getLogger(__name__)

    WAV_FILES = []
    WAV_FILES_PATH = "/../../../res/chimes/*.wav"

    _chimes_are_on = False

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def init(cls):
        '''Initialise this class.
        '''
        cls._get_wav_files()

        cls.logger.debug("Initialised.")

    @classmethod
    def get_time(cls):
        '''Get the current local time.

        Returns:
            The current local time.
        '''
        #strftime("%X", time.localtime())
        return datetime.datetime.now().time()

    @classmethod
    def chimes_on(cls):
        '''Set the chimes state to on.
        '''
        cls._chimes_are_on = True

    @classmethod
    def chimes_off(cls):
        '''Set the chimes state to off.
        '''
        cls._chimes_are_on = False

    @classmethod
    def chimes_are_on(cls):
        '''Are the chimes on?

        Returns:
            True if the chimes are on, else False.
        '''
        return cls._chimes_are_on

    @classmethod
    def get_random_chime_wav(cls):
        '''Get the next wav.

        Returns:
            A random wav from the list.
        '''
        wav_file_chime = random.choice(cls.WAV_FILES)
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Selected the following chime wav to play {0}"
                         .format(wav_file_chime))
        return wav_file_chime

    @classmethod
    def get_all_chime_wavs(cls):
        '''Get the next wav.

        Returns:
            A copy of the list of wavs.
        '''
        return list(cls.WAV_FILES)

    @classmethod
    def _get_wav_files(cls):
        '''Get and set the list of wav files used as chimes, (used during
        initialisation).
        '''
        this_path = os.path.dirname(os.path.abspath(__file__))
        cls.WAV_FILES = glob.glob(os.path.abspath(str(this_path + cls.WAV_FILES_PATH)))

        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Selected the following wave chimes {0}"
                         .format(cls.WAV_FILES))
