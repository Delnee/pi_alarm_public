'''
Created on 15 Mar 2015

@author: fjh
'''
import logging

class SpeechFacade(object):
    '''A Speech facade around a speech implementation.

    Used to speak words using a Text to Speech engine.
    '''

    logger = logging.getLogger(__name__)

    # The implementation of this classes interface.
    __implementation = None

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def set_implementation(cls, implementation):
        '''Set the implementation used by this facade. This method should be
        called before any of the other classes methods are called.

        Args:
            implementation - An implementation that implements this classes
            interface.
        '''
        # TODO Consider calling implementation.init() here, rather than the
        # the class calling it during parsing.
        cls.__implementation = implementation
        cls.__implementation.init()

    @classmethod
    def activate(cls, words):
        '''Activity interface method.

        Args:
            words - The word/sentence to speak.

        Returns:
            False.
        '''
        cls.__implementation.activate(words)

        return False

    @classmethod
    def say(cls, words):
        '''Say the words.

        Args:
            words - The word/sentence to speak.
        '''
        # pylint: disable=logging-format-interpolation
        cls.__implementation.say(words)

    @classmethod
    def say_list(cls, word_list):
        '''Say the words.

        Args:
            words - The words to speak.
        '''
        cls.__implementation.say_list(word_list)
