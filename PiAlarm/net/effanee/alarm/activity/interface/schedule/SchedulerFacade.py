'''
Created on 15 Mar 2015

@author: fjh
'''
import logging

class SchedulerFacade(object):
    '''A Scheduler facade around a scheduler implementation.

    Used to schedule jobs. Jobs are added to the appropriate scheduler, and
    run at the scheduled time. Typically, minute jobs are run at 20 seconds
    past the minute, five minute jobs at 40 seconds and hourly jobs on the
    minute.
    '''

    logger = logging.getLogger(__name__)

    # The implementation of this classes interface.
    __implementation = None

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def set_implementation(cls, implementation):
        '''Set the implementation used by this facade. This method should be
        called before any of the other classes methods are called.

        Args:
            implementation - An implementation that implements this classes
            interface.
        '''
        cls.__implementation = implementation
        cls.__implementation.init()

    @classmethod
    def register_every_min_job(cls, job):
        '''Register a new method to be invoked each minute.

        Args:
            job - A method name to be invoked.
        '''
        cls.__implementation.register_every_min_job(job)

    @classmethod
    def unregister_every_min_job(cls, job):
        '''Unregister an existing method.

        Args:
            job - The method name to unregister.
        '''
        cls.__implementation.unregister_every_min_job(job)

    @classmethod
    def register_paused_job(cls, job):
        '''Register a new method to be invoked each minute, when the audio is
        paused, to act as a reminder to the user.

        Args:
            job - A method name to be invoked.
        '''
        cls.__implementation.register_paused_job(job)

    @classmethod
    def unregister_paused_jobs(cls):
        '''Unregister all jobs associated with the audio being paused.
        '''
        cls.__implementation.unregister_paused_jobs()

    @classmethod
    def create_alarm_job(cls, job, alarm_time):
        '''Create an alarm job to run at the supplied time each day.

        Args:
            job - The method to invoke when the alarm is triggered.
            alarm_time - The time the alarm should trigger.
        '''
        cls.__implementation.create_alarm_job(job, alarm_time)

    @classmethod
    def remove_alarm_job(cls):
        '''Remove the singular alarm job.
        '''
        cls.__implementation.remove_alarm_job()

    @classmethod
    def register_every_five_mins_job(cls, job):
        '''Register a new method to be invoked every five minutes.

        Args:
            job - A method name to be invoked.
        '''
        cls.__implementation.register_every_five_mins_job(job)

    @classmethod
    def unregister_every_five_mins_job(cls, job):
        '''Unregister an existing method.

        Args:
            job - The method name to unregister.
        '''
        cls.__implementation.unregister_every_five_mins_job(job)

    @classmethod
    def register_every_hour_job(cls, job):
        '''Register a new method to be invoked every hour.

        Args:
            job - A method name to be invoked.
        '''
        cls.__implementation.register_every_hour_job(job)

    @classmethod
    def unregister_every_hour_job(cls, job):
        '''Unregister an existing method.

        Args:
            job - The method name to unregister.
        '''
        cls.__implementation.unregister_every_hour_job(job)
