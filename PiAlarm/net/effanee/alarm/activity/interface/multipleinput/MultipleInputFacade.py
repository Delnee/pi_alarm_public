'''
Created on 15 Mar 2015

@author: fjh
'''
import logging

class MultipleInputFacade(object):
    '''A Multiple Input facade around a multiple input implementation.

    Used to collect together several consecutive key presses.

    The process begins with calling one of the set methods. Subsequent key
    presses should then be passed to the activate method, until True is
    returned, indicating that the required key presses have been captured, or
    the capture process was aborted. The get method, corresponding to the
    previous set method should then be called to retrieve the value.
    '''

    logger = logging.getLogger(__name__)

    # The implementation of this classes interface.
    __implementation = None

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def set_implementation(cls, implementation):
        '''Set the implementation used by this facade. This method should be
        called before any of the other classes methods are called.

        Args:
            implementation - an implementation that implements this classes
            interface.
        '''
        cls.__implementation = implementation
        cls.__implementation.init()

    @classmethod
    def activate(cls, key_press):
        '''Activity interface method.

        Args:-
            key_press: The internal enum representing the key press code.

        Returns:
            True - If the time is set, or back is received on an empty time.
            False - If the time is not set, and more key presses are required.
        '''
        return cls.__implementation.activate(key_press)

    @classmethod
    def set_alarm_time(cls):
        '''Set up capturing of key press to get the four digits of a 24 hour
        time, in the form a valid 4 digit time HH:MM.
        '''
        cls.__implementation.set_alarm_time()

    @classmethod
    def get_alarm_time(cls):
        '''Get the 24 hour time of the alarm. Once the time has been captured,
        or aborted. If the process has been aborted, the previously set time
        will be returned.

        Returns:
            The last valid alarm time.
        '''
        return cls.__implementation.get_alarm_time()

    @classmethod
    def init_alarm_time(cls, alarm_time):
        '''Initialise the default alarm time. If not initialised, the default
        time of 07:30 is used.

        Args:
            alarm_time - A default time.
        '''
        cls.__implementation.init_alarm_time(alarm_time)
