'''
Created on 15 Apr 2015

@author: fjh
'''
import logging

class PersistFacade(object):
    '''A Persist facade around a persist implementation.

    Used to access state persistence functions.
    '''

    logger = logging.getLogger(__name__)

    # The implementation of this classes interface.
    __implementation = None

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def set_implementation(cls, implementation):
        '''Set the implementation used by this facade. This method should be
        called before any of the other classes methods are called.

        Args:
            implementation - an implementation that implements this classes
            interface.
        '''
        cls.__implementation = implementation
        cls.__implementation.init()

    @classmethod
    def set_alarm_alarm_time(cls, alarm_time):
        '''Set the 24 hour time of the alarm.

        Args:
            alarm_time - A time indicating the new state of the alarm time.
        '''
        cls.__implementation.set_alarm_alarm_time(alarm_time)

    @classmethod
    def get_alarm_alarm_time(cls):
        '''Get the 24 hour time of the alarm.

        Returns:
            A time indicating the current state of the alarm time.
        '''
        return cls.__implementation.get_alarm_alarm_time()

    @classmethod
    def set_alarm_is_on(cls, is_on):
        '''Set the is on status of the alarm.

        Args:
            is_on - A boolean indicating the new state of the alarm.
        '''
        cls.__implementation.set_alarm_is_on(is_on)

    @classmethod
    def get_alarm_is_on(cls):
        '''Get the is on status of the alarm.

        Returns:
            A boolean indicating the current state of the alarm.
        '''
        return cls.__implementation.get_alarm_is_on()

    @classmethod
    def set_clock_chimes_are_on(cls, chimes_are_on):
        '''Set the is on status of the chimes.

        Args:
            chimes_are_on - A boolean indicating the new state of the chimes.
        '''
        cls.__implementation.set_clock_chimes_are_on(chimes_are_on)

    @classmethod
    def get_clock_chimes_are_on(cls):
        '''Get the is on status of the chimes.

        Returns:
            A boolean indicating the current state of the chimes.
        '''
        return cls.__implementation.get_clock_chimes_are_on()

#     @classmethod
#     # pylint: disable=invalid-name
#     def set_multipleinput_init_alarm_time(cls, init_alarm_time):
#         '''Set the initial alarm time of the multiple input.
#
#         Args:
#             init_alarm_time - A time indicating the new state of the init alarm
#             time.
#         '''
#         cls.__implementation.set_multipleinput_init_alarm_time(init_alarm_time)

    @classmethod
    # pylint: disable=invalid-name
    def get_multipleinput_init_alarm_time(cls):
        '''Get the initial alarm time of the multiple input.

        Returns:
            A time indicating the current state of the init alarm time.
        '''
        return cls.__implementation.get_multipleinput_init_alarm_time()
    