'''
Created on 11 Feb 2015

@author: fjh
'''
import logging, threading

class KeyPressListener(threading.Thread):
    '''A listener, used to listen for IR remote key presses.

    Runs in a separate thread to the class receiving the key presses from the
    hardware, so that the processing of a key press doesn't cause key presses
    to be lost.

    Received key presses are stored in a list, and processed sequentially, in
    receive order.

    When a key press enum is received, via the update() method, a handler class
    is invoked to process the key press.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self):
        '''
        Constructor.
        '''
        threading.Thread.__init__(self)
        self.__event = threading.Event()
        self.__key_presses = []
        self.__keep_running = True
        self.__handlers = []

    def run(self):
        '''Override thread run.

        Start infinite loop, to wait for notification that a key press has been
        received. Then process any pending key presses in the list.
        '''
        while True:

            # Wait here, until notified that a new key press has occurred.
            self.__event.wait(None)

            if self.__keep_running:
                # Process the key presses, and start waiting again.
                while self.__key_presses:

                    key = self.__key_presses.pop(0)
                    # pylint: disable=logging-format-interpolation
                    self.__class__.logger.debug("Received key press {0}.".format(key))
                    self.__update_handlers(key)

                # Set wait.
                self.__event.clear()
            else:
                # Exit.
                break

        self.__class__.logger.debug("Exiting.")

    def stop(self):
        '''Stop the listener.
        '''
        self.__keep_running = False
        self.__event.set()

    def update(self, key_press):
        '''Listener interface method. Add the key press to the list, and wake
        up this listener.

        Args:
            key_press - The internal enum representing the remote key press code.
        '''
        self.__key_presses.append(key_press)
        # Unset wait.
        self.__event.set()

    def register_handler(self, new_handler):
        '''Register a handler.

        Handlers must implement the method self.update(self, keypress).

        Args:
            new_handler - A class interested in being notified when a key is
            pressed on the remote.
        '''
        if not new_handler in self.__handlers:
            self.__handlers.append(new_handler)
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.debug("Added handler {0}".format(new_handler))

    def unregister_handler(self, existing_handler):
        '''Unregister a handler.

        Args:
            existing_handler - A class interested in being notified when a key
            is pressed on the remote, which has been previously registered.
        '''
        if existing_handler in self.__handlers:
            self.__handlers.remove(existing_handler)
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.debug("Removed handler {0}".format(existing_handler))

    def __update_handlers(self, key_press):
        '''Update all registered handlers.

        Each handlers update() method is invoked.

        Args:
            key_press - The internal enum representing the remote key code that
            has been pressed.
        '''
        for handler in self.__handlers:
            try:
                handler.update(key_press)
            except StandardError as ex:
                # pylint: disable=logging-format-interpolation
                self.__class__.logger.error("Could not invoke update method on {0},"
                                            " in response to key {1}, exception was {2}"
                                            .format(handler, key_press, ex))
        