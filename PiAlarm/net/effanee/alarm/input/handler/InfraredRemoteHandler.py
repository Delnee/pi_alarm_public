'''
Created on 12 Feb 2015

@author: fjh
'''

import logging

from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum

class InfraredRemoteHandler(object):
    '''Handler for key presses on an infrared remote control.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self):
        '''
        Constructor
        '''
        self.__key_mappings = self.__initialise_key_mappings()
        self.__handlers = []

    # pylint: disable=no-self-use
    def __initialise_key_mappings(self):
        '''Bind a set of key constants to a list of handlers.

        Keys - The enum constants used by the IR remote.

        Values - An ordered list of handlers for the key enum.

        Args:-
            None.

        Returns:
            A dictionary of key mappings.

        Raises:
            Nothing
        '''
        self.key_mappings = {
            KeysEnum.KEY_VOLUMEDOWN : [],
            KeysEnum.KEY_PLAYPAUSE : [],
            KeysEnum.KEY_VOLUMEUP : [],
            KeysEnum.KEY_SETUP : [],
            KeysEnum.KEY_UP : [],
            KeysEnum.KEY_STOP : [],
            KeysEnum.KEY_LEFT : [],
            KeysEnum.KEY_ENTER : [],
            KeysEnum.KEY_RIGHT : [],
            KeysEnum.KEY_KP0 : [],
            KeysEnum.KEY_DOWN : [],
            KeysEnum.KEY_BACK : [],
            KeysEnum.KEY_KP1 : [],
            KeysEnum.KEY_KP2 : [],
            KeysEnum.KEY_KP3 : [],
            KeysEnum.KEY_KP4 : [],
            KeysEnum.KEY_KP5 : [],
            KeysEnum.KEY_KP6 : [],
            KeysEnum.KEY_KP7 : [],
            KeysEnum.KEY_KP8 : [],
            KeysEnum.KEY_KP9 : []
            }

        return self.key_mappings

    def register_handler(self, new_handler):
        '''Register a handler. Handlers are invoked in the order they are
        registered.

        Handlers must implement the methods self.update(self, key press), and
        self.handled_keys(self).

        Args:-
            new_handler: a class interested in being notified when a key is
            pressed on the remote.

        Returns:
            Nothing.

        Raises:
            Nothing
        '''
        if not new_handler in self.__handlers:
            self.__handlers.append(new_handler)

            # Match handler to its handled keys.
            handled_keys = new_handler.handled_keys()
            for key in handled_keys:
                self.__key_mappings[key].append(new_handler)

            # pylint: disable=logging-format-interpolation
            self.__class__.logger.debug("Added handler {0}.".format(new_handler))

    def unregister_handler(self, existing_handler):
        '''Unregister a handler.

        Args:-
            existing_handler: a class interested in being notified when a key
            is pressed on the remote, which has been previously registered.

        Returns:
            Nothing.

        Raises:
            Nothing
        '''
        if existing_handler in self.__handlers:
            self.__handlers.remove(existing_handler)
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.debug("Removed handler {0}".format(existing_handler))

    def __update_handlers(self, key_press):
        '''Update all registered handlers.

        Each handlers update() method is invoked. If the handlers update() method
        returns true the next handler is invoked, else no subsequent handlers
        are invoked.

        Args:-
            key_press: the internal enum representing the remote key code that
            has been pressed.

        Returns:
            Nothing.

        Raises:
            Nothing
        '''
        for handler in self.__handlers:
            try:
                do_next = handler.update(key_press)
                # Should subsequent handlers be called?
                if not do_next:
                    break
            except StandardError as ex:
                # pylint: disable=logging-format-interpolation
                self.__class__.logger.error("Could not invoke update method on {0},"
                                            " in response to key {1}, exception {2}"
                                            .format(handler, key_press, ex))

    def update(self, key_press):
        '''Handler interface method.

        Args:-
            key_press: the internal enum representing the remote key press code.

        Returns:
            Nothing.

        Raises:
            Nothing
        '''
        # pylint: disable=logging-format-interpolation
        self.__class__.logger.debug("Received key press {0}.".format(key_press))

        if len(self.__key_mappings[key_press]) > 0:
            for handler in self.__key_mappings[key_press]:

                # pylint: disable=logging-format-interpolation
                self.__class__.logger.debug("Sent key press {0} to handler {1}."
                                            .format(key_press, handler))
                handler.update(key_press)
        else:
            self.__class__.logger.debug("Key press {0} ignored.".format(key_press))

    def __str__(self):
        '''Override string.
        '''
        return self.__class__.__name__ + "[" \
                "__key_mappings" + ", ".join("{}->{}".format([ str(handler) for handler in value ], key) for key, value in self.__key_mappings.iteritems()) \
                + "]"
