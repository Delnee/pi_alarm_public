'''
Created on 10 Feb 2015

@author: fjh
'''

# pylint: disable=too-few-public-methods
class KeysEnum(object):
    '''An enumeration of keys recognised by this application.

    Keys are mapped from the IR remote to user strings using the:-

        /etc/lirc/lircd.conf
        /etc/lirc/lircrc

    files.
    '''

    KEY_VOLUMEDOWN = "KEY_VOLUMEDOWN"
    KEY_PLAYPAUSE = "KEY_PLAYPAUSE"
    KEY_VOLUMEUP = "KEY_VOLUMEUP"
    KEY_SETUP = "KEY_SETUP"
    KEY_UP = "KEY_UP"
    KEY_STOP = "KEY_STOP"
    KEY_LEFT = "KEY_LEFT"
    KEY_ENTER = "KEY_ENTER"
    KEY_RIGHT = "KEY_RIGHT"
    KEY_KP0 = "KEY_KP0"
    KEY_DOWN = "KEY_DOWN"
    KEY_BACK = "KEY_BACK"
    KEY_KP1 = "KEY_KP1"
    KEY_KP2 = "KEY_KP2"
    KEY_KP3 = "KEY_KP3"
    KEY_KP4 = "KEY_KP4"
    KEY_KP5 = "KEY_KP5"
    KEY_KP6 = "KEY_KP6"
    KEY_KP7 = "KEY_KP7"
    KEY_KP8 = "KEY_KP8"
    KEY_KP9 = "KEY_KP9"
