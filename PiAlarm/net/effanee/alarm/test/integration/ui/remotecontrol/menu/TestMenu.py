'''
Created on 4 May 2015

@author: fjh
'''
import unittest
#from net.effanee.alarm.test.Configure import Configure
from net.effanee.alarm.ui.remotecontrol.menu.Menu import Menu
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum
import time

# Facade/Implementation.
# pylint: disable=line-too-long
from net.effanee.alarm.activity.interface.multipleinput.MultipleInputFacade import MultipleInputFacade
from net.effanee.alarm.activity.implementation.multipleinput.MultipleInputImpl import MultipleInputImpl
from net.effanee.alarm.activity.interface.clock.ClockFacade import ClockFacade
from net.effanee.alarm.activity.implementation.clock.ClockImpl import ClockImpl
from net.effanee.alarm.activity.interface.talk.SpeechFacade import SpeechFacade
from net.effanee.alarm.activity.implementation.talk.eSpeakImpl import eSpeakImpl
from net.effanee.alarm.activity.interface.alarm.AlarmFacade import AlarmFacade
from net.effanee.alarm.activity.implementation.alarm.AlarmImpl import AlarmImpl
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
from net.effanee.alarm.activity.interface.schedule.SchedulerFacade import SchedulerFacade
from net.effanee.alarm.activity.implementation.schedule.APSchedulerImpl import APSchedulerImpl
from net.effanee.alarm.activity.interface.persist.PersistFacade import PersistFacade
from net.effanee.alarm.activity.implementation.persist.PersistImpl import PersistImpl

class TestMenu(unittest.TestCase):
    '''Unit test.
    '''
    # The menu under test.
    _menu = None

    simple_menu_string = '''Home(internal)
  Clock(internal)
    Time(leaf)
    Chimes(internal)
      On(leaf)
      Off(leaf)
      Play Random Chime(leaf)
  Alarm(internal)
    On(leaf)
    Off(leaf)
    Set Time(leaf)
  Audio(internal)
    Play(leaf)
    Pause(leaf)
    Stop(leaf)
    Source(internal)
      Radio 1(leaf)
      Radio 2(leaf)
      Radio 3(leaf)
      Radio 4(leaf)
      Alarm Clock(leaf)
  Status(leaf)
'''

    @classmethod
    def setUpClass(cls):
        '''Create environment.
        '''
        cls.__config_implementations()
        cls._menu = Menu()

    def test_menu_structure(self):
        '''Menu structure.
        '''
        menu_string = TestMenu._menu.simple_pretty_string()
        self.assertEquals(menu_string, TestMenu.simple_menu_string)

    # pylint: disable=no-self-use
    def test_clock_chimes_random(self):
        '''Play random chime.
        '''
        # Play Chimes.
        TestMenu._menu.activate(KeysEnum.KEY_RIGHT)
        time.sleep(1)
        TestMenu._menu.activate(KeysEnum.KEY_RIGHT)
        time.sleep(1)
        TestMenu._menu.activate(KeysEnum.KEY_DOWN)
        time.sleep(1)
        TestMenu._menu.activate(KeysEnum.KEY_RIGHT)
        time.sleep(1)
        TestMenu._menu.activate(KeysEnum.KEY_DOWN)
        time.sleep(1)
        TestMenu._menu.activate(KeysEnum.KEY_DOWN)
        time.sleep(1)
        TestMenu._menu.activate(KeysEnum.KEY_ENTER)
        time.sleep(2)
        TestMenu._menu.activate(KeysEnum.KEY_LEFT)
        time.sleep(1)
        TestMenu._menu.activate(KeysEnum.KEY_LEFT)
        time.sleep(1)
        TestMenu._menu.activate(KeysEnum.KEY_LEFT)
        time.sleep(1)

    @classmethod
    def __config_implementations(cls):
        '''Configure the facade/implementation pairings.
        '''
        MultipleInputFacade.set_implementation(MultipleInputImpl)
        ClockFacade.set_implementation(ClockImpl)
        SpeechFacade.set_implementation(eSpeakImpl)
        AlarmFacade.set_implementation(AlarmImpl)
        PlayFacade.set_implementation(CliPlayImpl())
        SchedulerFacade.set_implementation(APSchedulerImpl)
        PersistFacade.set_implementation(PersistImpl)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
