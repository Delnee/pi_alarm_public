'''
Created on 5 May 2015

@author: fjh
'''
import unittest
# Annoying conditional imports used for testing on Windows, before deploying
# on the Raspberry Pi.
try:
    # This is the actual library used to access pylirc on the Raspberry Pi,
    # which is installed into the local Python environment.
    import pylirc

except ImportError:
    # This is the mock library used to access pylirc in the development
    # environment, which is PyDev/Eclipse on Windows.
    from net.effanee.alarm.test.mocks.pylirc.pylirc import pylirc
#from net.effanee.alarm.test.Configure import Configure
from net.effanee.alarm.input.lirc.KeyPressNotifier import KeyPressNotifier
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum
import threading
import time

class TestKeyPressNotifier(unittest.TestCase):
    '''Unit test.
    '''
    # The key press notifier under test.
    _key_press_notifier = None

    @classmethod
    def setUpClass(cls):
        '''Create environment.
        '''
        cls._key_press_notifier = KeyPressNotifier()
        cls._key_press_notifier.daemon = True
        cls._key_press_notifier.start()

    def test_received_key_presses(self):
        '''Received key press.
        '''
        # Create listener to receive key presses.
        listener_thread = self.Listener()
        listener_thread.daemon = True
        listener_thread.start()
        time.sleep(1)

        # Register listener.
        TestKeyPressNotifier._key_press_notifier.register_listener(listener_thread)

        # Send key press.
        pylirc.add_key({"config": "KEY_KP1", "repeat": "1"})
        pylirc.send_keys()
        listener_thread.join(1)

        self.assertEquals(listener_thread.get_key_press(), KeysEnum.KEY_KP1)

        # Tidy.
        TestKeyPressNotifier._key_press_notifier.stop()

    class Listener(threading.Thread):
        '''Utility class used to capture key presses.
        '''

        def __init__(self):
            '''
            Constructor.
            '''
            threading.Thread.__init__(self)
            self.__last_key_press = None

        def run(self):
            '''Override thread run. Wait until a key press is received.
            '''
            # Wait here.
            while not self.__last_key_press:
                time.sleep(.1)

        def update(self, key_press):
            '''Listener interface method. Stores the last key press enum.

            Args:
                key_press: The internal enum representing the key press code.
            '''
            # pylint: disable=logging-format-interpolation
            self.__last_key_press = key_press

            return False

        def get_key_press(self):
            '''Return the key press.
            '''
            return self.__last_key_press

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
