'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.action.AudioAction import AudioAction
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
from net.effanee.alarm.activity.interface.talk.SpeechFacade import SpeechFacade
from net.effanee.alarm.activity.implementation.talk.eSpeakImpl import eSpeakImpl
from net.effanee.alarm.activity.interface.schedule.SchedulerFacade import SchedulerFacade
from net.effanee.alarm.activity.implementation.schedule.APSchedulerImpl import APSchedulerImpl
import time

class TestAudioAction(unittest.TestCase):
    '''Unit test.
    '''
    def test_sources(self):
        '''Set source to Radio 1 and compare.
        '''
        PlayFacade.set_implementation(CliPlayImpl())
        SpeechFacade.set_implementation(eSpeakImpl)

        source_name = "Radio 1"
        audio_action = AudioAction()
        sources = audio_action.get_sources()

        radio_1 = None
        for source in sources:
            if source == source_name:
                radio_1 = source

        self.assertTrue(radio_1)
        audio_action.set_source(radio_1)
        self.assertEqual(radio_1, audio_action.get_source())

    # pylint: disable=no-self-use
    def test_say_radio_1(self):
        '''Say Radio 1.
        '''
        PlayFacade.set_implementation(CliPlayImpl())
        SpeechFacade.set_implementation(eSpeakImpl)

        source_name = "Radio 1"
        audio_action = AudioAction()
        audio_action.say(source_name)
        time.sleep(1)

    # pylint: disable=no-self-use
    def test_play_stop(self):
        '''Play and stop.
        '''
        PlayFacade.set_implementation(CliPlayImpl())
        SpeechFacade.set_implementation(eSpeakImpl)
        SchedulerFacade.set_implementation(APSchedulerImpl)

        audio_action = AudioAction()
        audio_action.play_pause()
        time.sleep(1)
        audio_action.stop()
        time.sleep(1)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
