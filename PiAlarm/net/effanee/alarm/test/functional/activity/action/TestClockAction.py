'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
#from net.effanee.alarm.test.Configure import Configure
from net.effanee.alarm.activity.action.ClockAction import ClockAction
from net.effanee.alarm.activity.interface.talk.SpeechFacade import SpeechFacade
from net.effanee.alarm.activity.implementation.talk.eSpeakImpl import eSpeakImpl
from net.effanee.alarm.activity.interface.clock.ClockFacade import ClockFacade
from net.effanee.alarm.activity.implementation.clock.ClockImpl import ClockImpl
from net.effanee.alarm.activity.interface.alarm.AlarmFacade import AlarmFacade
from net.effanee.alarm.activity.implementation.alarm.AlarmImpl import AlarmImpl
from net.effanee.alarm.activity.interface.persist.PersistFacade import PersistFacade
from net.effanee.alarm.activity.implementation.persist.PersistImpl import PersistImpl
from net.effanee.alarm.activity.interface.schedule.SchedulerFacade import SchedulerFacade
from net.effanee.alarm.activity.implementation.schedule.APSchedulerImpl import APSchedulerImpl
import time

# pylint: disable=too-few-public-methods
class TestClockAction(unittest.TestCase):
    '''Unit test.
    '''
    # pylint: disable=no-self-use
    def test_say_time(self):
        '''Say time.
        '''
        SpeechFacade.set_implementation(eSpeakImpl)
        ClockFacade.set_implementation(ClockImpl)

        clock_action = ClockAction()
        clock_action.say_time()
        time.sleep(1)

    # pylint: disable=no-self-use
    def test_alarm_on(self):
        '''Alarm on.
        '''
        SpeechFacade.set_implementation(eSpeakImpl)
        ClockFacade.set_implementation(ClockImpl)
        AlarmFacade.set_implementation(AlarmImpl)
        PersistFacade.set_implementation(PersistImpl)
        SchedulerFacade.set_implementation(APSchedulerImpl)

        clock_action = ClockAction()
        clock_action.alarm_on()
        time.sleep(5)

    # pylint: disable=no-self-use
    def test_chimes_on(self):
        '''Chimes on.
        '''
        SpeechFacade.set_implementation(eSpeakImpl)
        ClockFacade.set_implementation(ClockImpl)
        AlarmFacade.set_implementation(AlarmImpl)
        PersistFacade.set_implementation(PersistImpl)
        SchedulerFacade.set_implementation(APSchedulerImpl)

        clock_action = ClockAction()
        clock_action.chimes_on()
        time.sleep(5)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
