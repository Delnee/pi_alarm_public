'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.action.CommonAction import CommonAction
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
from net.effanee.alarm.activity.interface.talk.SpeechFacade import SpeechFacade
from net.effanee.alarm.activity.implementation.talk.eSpeakImpl import eSpeakImpl
from net.effanee.alarm.activity.interface.clock.ClockFacade import ClockFacade
from net.effanee.alarm.activity.implementation.clock.ClockImpl import ClockImpl
from net.effanee.alarm.activity.interface.alarm.AlarmFacade import AlarmFacade
from net.effanee.alarm.activity.implementation.alarm.AlarmImpl import AlarmImpl
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum
import time

class TestCommonAction(unittest.TestCase):
    '''Unit test.
    '''
    # pylint: disable=no-self-use
    def test_say(self):
        '''Say.
        '''
        PlayFacade.set_implementation(CliPlayImpl())
        SpeechFacade.set_implementation(eSpeakImpl)

        say_words = "test say"
        common_action = CommonAction()
        common_action.say(say_words)
        time.sleep(1)

    # pylint: disable=no-self-use
    def test_say_key_press(self):
        '''Say key press down.
        '''
        PlayFacade.set_implementation(CliPlayImpl())
        SpeechFacade.set_implementation(eSpeakImpl)

        common_action = CommonAction()
        common_action.say_key_press(KeysEnum.KEY_DOWN)
        time.sleep(1)

    # pylint: disable=no-self-use
    def test_say_status(self):
        '''Say status.
        '''
        PlayFacade.set_implementation(CliPlayImpl())
        SpeechFacade.set_implementation(eSpeakImpl)
        ClockFacade.set_implementation(ClockImpl)
        AlarmFacade.set_implementation(AlarmImpl)

        common_action = CommonAction()
        common_action.say_status()
        time.sleep(10)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
