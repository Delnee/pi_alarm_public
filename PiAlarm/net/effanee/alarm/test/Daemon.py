'''
Created on 10 Feb 2015

@author: fjh
'''

import sys, time, argparse
import signal
import platform
import logging

# pylint: disable=too-few-public-methods
class Main(object):
    '''Used to start the alarm clock. Call Main.main().

    From the command line:-

        python Main.py

    to show help:-

        python Main.py -h
    '''

    logger = logging.getLogger(__name__)

    def __init__(self):
        '''Do not use.
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def main(cls, *args):
        '''Thou shalt start here. Over there, thou shalt not start. Neither,
        right over there, other than thou be proceeding to here. Main() is right
        out.

        Args:
            The command line arguments.

        Returns:
            0 on a successful exit, else -1.
        '''
        # Trap keyboard interrupts
        signal.signal(signal.SIGINT, Main.__signal_handler)

        # Get the command line arguments.
        # pylint: disable=line-too-long
        parser = argparse.ArgumentParser()
        parser.add_argument("-v", "--version", help="show the version", action="version", version="1.0")
        parser.add_argument("-sm", "--show-menu", help="show the menu structure", action="store_true", required=False)
        parser.add_argument("-ss", "--show-sources", help="show the audio sources", action="store_true", required=False)
        args = parser.parse_args()

        # pylint: disable=no-member
        if args.show_menu:
            print "--show-menu has been selected."
        elif args.show_sources:
            print "--show-sources has been selected."

        # Start up tasks.
        cls.__config_logging()

        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Initialised.")

        # Wait.
        cls.bad_exit = False
        try:
            if platform.system() == "Windows":
                counter = 60
                while counter:
                    print "%s %s" % ("Sleeping", counter)
                    time.sleep(1)
                    counter -= 1
            else:
                # signal.pause() is not available under MS Windows.
                cls.logger.debug("Waiting on SIGINT.")
                signal.pause() # @UndefinedVariable

        except StandardError as ex:
            # pylint: disable=logging-format-interpolation
            cls.logger.critical("Error while running {0}".format(ex))
            cls.bad_exit = True

        finally:
            # Shutdown tasks.
            cls.logger.debug("Uninitialised.")

            #Finish.
            if cls.bad_exit:
                cls.logger.warn("Bad exit.")
                sys.exit(1)
            else:
                cls.logger.info("Good exit.")
                sys.exit(0)

    @classmethod
    # pylint: disable=unused-argument
    def __signal_handler(cls, sig_numb, handler):
        '''?
        '''
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Received signal number {0}".format(sig_numb))

    @classmethod
    def __config_logging(cls):
        '''Format the logging output.
        '''
        # Create file logger.
        # /var/log/alarmclock
        logging.basicConfig(format="%(asctime)s %(name)s:%(levelname)s - %(message)s",
                            datefmt="%d/%m/%Y %H:%M:%S",
                            filename="example.log",
                            filemode="a",
                            level=logging.DEBUG)

        # Add console logger.
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        formatter = logging.Formatter(fmt="%(asctime)s %(name)s:%(levelname)s - %(message)s", datefmt="%d/%m/%Y %H:%M:%S")
        console.setFormatter(formatter)
        logging.getLogger("").addHandler(console)

        cls.logger.debug("Configured file and console logging.")

def command_line_invocation():
    '''Tell me if it's a command line invocation.
    '''
    # pylint: disable=logging-format-interpolation
    Main.logger.debug("Started from the command line.")

if __name__ == "__main__":
    command_line_invocation()
    Main.main()
