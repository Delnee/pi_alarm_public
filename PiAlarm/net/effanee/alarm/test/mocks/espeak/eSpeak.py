# pylint: disable=invalid-name
'''
Created on 10 Mar 2015

@author: fjh
'''
import os
import subprocess
import logging
from time import sleep
import signal

class espeak(object):
    '''This is a mock speech object, used to replace eSpeak during development
    on Windows.
    '''

    logger = logging.getLogger(__name__)

    # pylint: disable=too-few-public-methods
    @staticmethod
    class Parameter(object):
        '''Dummy eSpeak class.
        '''
        Rate = 200
        Capitals = 10
        Pitch = 60
        Volume = 50

    # Initialise.
    __process = None
    this_path = os.path.dirname(os.path.abspath(__file__))
    __espeak_path = this_path + '/eSpeak.exe'

    @staticmethod
    def __init__():
        '''
        Constructor
        '''
        pass

    @staticmethod
    def set_voice(param1):
        '''Does nothing.
        '''
        pass

    @staticmethod
    def set_parameter(param1, param2):
        '''Does nothing.
        '''
        pass

    @staticmethod
    def is_playing():
        '''Is the process still running?

        Returns:
            True - if the process is still running, (speaking), else False.
        '''
        # A process is not underway.
        if espeak.__process is None:
            return False

        espeak.__process.poll()
        if espeak.__process.returncode is None:
            return True
        else:
            return False

    @staticmethod
    def cancel():
        '''Kill a running process, i.e. before it completes speaking naturally.
        '''
        try:
            os.kill(espeak.__process.pid, signal.SIGINT)

        except WindowsError as ex:
            if ex.args[0] == 5:
                # The exception - (5, 'Access is denied') is thrown by Windows
                # when, the kill function is called on a process that has
                # already exited.
                return
            # pylint: disable=logging-format-interpolation
            espeak.__class__.logger.critical("Could not cancel the speech, exception {0}"
                                            .format(ex))
    @staticmethod
    def synth(words):
        '''Speak the supplied words.

        Args:
            words: a string of words.
        '''
        args = [espeak.__espeak_path, words]
        espeak.__process = subprocess.Popen(args, close_fds=True)

# Quick test.
if __name__ == '__main__':
    # pylint: disable=line-too-long
    espeak.synth("These are lots of words, Jack and Jill went up the hill to fetch a pail of water.")
    sleep(3)
    if espeak.is_playing():
        espeak.cancel()
