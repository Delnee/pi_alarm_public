'''
Created on 11 Feb 2015

@author: fjh
'''

import socket, logging, threading

class Server(threading.Thread):
    '''A convenience class, used to create a receiving socket for the
    pylirc mock class.

    Note:- Apparently, Pythin doesn't require server sockets to be explicitly
    closed, so this class contains no clean up code.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self, host, port):
        '''
        Constructor
        '''
        threading.Thread.__init__(self)
        self.__last_connection = None
        self.__host = host
        self.__port = port

    def run(self):
        '''Override thread run.

        Args:-
            None.

        Returns:
            Nothing.

        Raises:
            Nothing
        '''
        self.__create_server()

    def __create_server(self):
        '''Create a new server socket, from which new receive sockets can be
        created. This method should be called once.
        '''
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((self.__host, self.__port))
        sock.listen(1)
        # pylint: disable=logging-format-interpolation
        self.__class__.logger.debug("Created server bound to port {0}.".format(self.__port))
        while True:
            connection, addr = sock.accept()
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.debug("Accepted connection from {0}.".format(addr))
            # pylint: disable=no-member
            connection.setsockopt(socket.SOL_SOCKET, socket.TCP_NODELAY, 1)
            self.__set_last_connection(connection)
            self.__class__.logger.debug("Created a new receive socket.")


    def get_last_connection(self):
        '''Get the last receive socket, created  by sending a request to this
        server. It is the responsibility of the caller to close the returned
        socket.
        '''
        return self.__last_connection

    def __set_last_connection(self, connection):
        '''Set the last receive socket.

        Args:-
            connection: a socket returned from a connection to this server.

        Returns:
            Nothing.

        Raises:
            Nothing
        '''
        if not self.__last_connection is None:
            self.__last_connection.close()

        self.__last_connection = connection
