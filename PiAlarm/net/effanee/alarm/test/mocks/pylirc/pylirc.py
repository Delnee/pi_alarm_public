'''
Created on 10 Feb 2015

@author: fjh
'''

import socket, logging, copy, threading
from time import sleep

from net.effanee.alarm.test.mocks.pylirc.Server import Server

# pylint: disable=invalid-name
class pylirc(object):
    '''This is a mock class, used to emulate the behaviour of the actual pylirc
    module from:-

        https://launchpad.net/pylirc2

    the methods init(), nextcode() and exit() mock the corresponding pylirc
    methods.

    This class is imported into the using object and the above methods are
    invoked to get the key presses. A test object causes the emulation of IR
    remote key presses by:-

        pylirc.add_key({"config": "KEY_KP1", "repeat": 0})
        pylirc.add_key({"config": "KEY_DOWN", "repeat": 0})
        pylirc.send_keys()
    '''

    logger = logging.getLogger(__name__)

    HOST = "localhost"
    PORT = 5000

    __server = None
    __recv_socket = None
    __send_socket = None
    __lock = threading.Lock()
    __keys_list = []

    @staticmethod
    # pylint: disable=unused-argument
    def init(lirc_prog, lirc_location, blocking):
        '''Part of the pylirc interface. Create a receive socket. The arguments
        are ignored.

        Args (from pylirc docs):

            lirc_prog: the name used for your program in the lirc configuration
            file, must be supplied.

            lirc_location: a filename to a lirc configuration file in case you
            wish not to use lircs default configuration file (usually ~/.lircrc).

            blocking: a flag indicating wether you want blocking mode or not.
            See also blocking() and select.select() (latter in python docs).

        Returns:
            A receive socket.

        Raises:
            Nothing
        '''

        pylirc.__create_server()
        pylirc.__create_client()
        pylirc.__set_sockets()
        return pylirc.__recv_socket

    @staticmethod
    # pylint: disable=unused-argument
    def nextcode(extended):
        '''Part of the pylirc interface. Return a key dictionary:-

        Args:
            extended: Ignored.

        Returns:
            config: The config string from lirc config file - the same string
            you'd get in non-extended mode.

            repeat: The repeat count of the button press.
        '''
        if len(pylirc.__keys_list) > 0:

            pylirc.__lock.acquire()
            try:
                keys = copy.copy(pylirc.__keys_list)
                pylirc.__keys_list = []
            finally:
                pylirc.__lock.release()

            return keys

        else:
            return None

    @staticmethod
    def exit():
        '''Part of the pylirc interface. Tidy.
        '''
        if not pylirc is None:
            pylirc.__send_socket.close()
            pylirc.__recv_socket.close()
            pylirc.logger.debug("Closed send and receive sockets.")

        pylirc.logger.debug("Exiting.")

    @staticmethod
    def add_key(key_dict):
        '''Not part of the pylirc interface. Add the key dictionary to the list
        of pending key presses.

        Args:
            send_dict: A dictionary containing:-

                config: The config string from the lirc config file.

                repeat: The repeat count of the button press. Always 1.

            i.e.

                dict = {
                    "config": "KEY_KP1",
                    "repeat": "1"
                }
        '''
        pylirc.__lock.acquire()
        try:
            pylirc.__keys_list.append(key_dict)
        finally:
            pylirc.__lock.release()

    @staticmethod
    def send_keys():
        '''Not part of the pylirc interface. Key presses added to the queue will
        not be sent to the receiving object until this method is invoked.
        '''
        pylirc.__send_socket.sendall('Wakeup')

    @staticmethod
    def __create_server():
        '''Start a server, used to obtain a receive socket.
        '''
        pylirc.__server = Server(pylirc.HOST, pylirc.PORT)
        pylirc.__server.daemon = True
        pylirc.__server.start()
        pylirc.logger.debug("Created server.")

    @staticmethod
    def __create_client():
        '''Create a send socket.
        '''
        pylirc.__send_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        pylirc.__send_socket.connect((pylirc.HOST, pylirc.PORT))
        pylirc.__send_socket.setsockopt(socket.SOL_SOCKET, socket.TCP_NODELAY, 1)
        pylirc.logger.debug("Created send socket.")

    @staticmethod
    def __set_sockets():
        '''Send a request to the server, using the send socket, and get the
        created receive socket.
        '''
        pylirc.__send_socket.sendall('Hello1')

        # pylint: disable=unused-variable
        for i in range(10):  # @UnusedVariable
            sock = pylirc.__server.get_last_connection()
            if not sock is None:
                pylirc.__recv_socket = sock
                pylirc.logger.debug("Created receive socket.")
                break

            sleep(.5)

        if sock is None:
            # Could not get the receive socket!
            message = "Could not get a receive socket."
            pylirc.logger.critical(message)
            raise RuntimeError(message)

        hello1 = pylirc.__recv_socket.recv(6)
        pylirc.__recv_socket.sendall("Hello2")
        hello2 = pylirc.__send_socket.recv(6)
        if hello1 == "Hello1" and hello2 == "Hello2":
            pylirc.logger.debug("Send and receive sockets are working.")
        else:
            raise RuntimeError("Could not open send and receive sockets.")
    