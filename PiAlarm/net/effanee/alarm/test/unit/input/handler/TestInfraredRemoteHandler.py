'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.input.handler.InfraredRemoteHandler import InfraredRemoteHandler
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum
import threading
import time

# pylint: disable=too-few-public-methods
class TestInfraredRemoteHandler(unittest.TestCase):
    '''Unit test.
    '''

    direct_key_mappings = [
        KeysEnum.KEY_VOLUMEDOWN,
        KeysEnum.KEY_PLAYPAUSE,
        KeysEnum.KEY_VOLUMEUP,
        KeysEnum.KEY_STOP
    ]

    def test_handler(self):
        '''Check handler.
        '''
        handler = InfraredRemoteHandler()

        # Create activity to receive key presses.
        handler_thread = self.Handler(TestInfraredRemoteHandler.direct_key_mappings)
        handler_thread.daemon = True
        handler_thread.start()

        # Send key press.
        handler.register_handler(handler_thread)
        handler.update(KeysEnum.KEY_VOLUMEDOWN)
        handler_thread.join(1)

        self.assertEquals(handler_thread.get_key_press(), KeysEnum.KEY_VOLUMEDOWN)

    class Handler(threading.Thread):
        '''Utility class used to capture key presses.
        '''

        def __init__(self, handled_keys):
            '''
            Constructor.
            '''
            threading.Thread.__init__(self)
            self.__handled_keys = handled_keys
            self.__last_key_press = None

        def run(self):
            '''Override thread run. Wait until a key press is received.
            '''
            # Wait here.
            while not self.__last_key_press:
                time.sleep(.1)

        def update(self, key_press):
            '''Handler interface method. Stores the last key press enum.

            Args:
                key_press: The internal enum representing the key press code.
            '''
            # pylint: disable=logging-format-interpolation
            self.__last_key_press = key_press

            return False

        def handled_keys(self):
            '''Handler interface method.
            '''
            return self.__handled_keys

        def get_key_press(self):
            '''Return the key press.
            '''
            return self.__last_key_press

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
