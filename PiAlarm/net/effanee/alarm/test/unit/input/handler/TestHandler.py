'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.input.handler.Handler import Handler
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum
import threading
import time

class TestHandler(unittest.TestCase):
    '''Unit test.
    '''

    direct_key_mappings = [
        KeysEnum.KEY_VOLUMEDOWN,
        KeysEnum.KEY_PLAYPAUSE,
        KeysEnum.KEY_VOLUMEUP,
        KeysEnum.KEY_STOP
    ]

    def test_handled_keys(self):
        '''Check keys enum.
        '''
        handler = Handler("test", TestHandler.direct_key_mappings)
        self.assertEquals(handler.handled_keys(), TestHandler.direct_key_mappings)

    def test_activate(self):
        '''Is Activated with correct key press.
        '''
        handler = Handler("test", TestHandler.direct_key_mappings)

        # Create activity to receive key presses.
        activity_thread = self.Activity()
        activity_thread.daemon = True
        activity_thread.start()

        # Send key press.
        handler.register_activity(activity_thread)
        handler.update(KeysEnum.KEY_VOLUMEDOWN)
        activity_thread.join(1)

        self.assertEquals(activity_thread.get_key_press(), KeysEnum.KEY_VOLUMEDOWN)

    class Activity(threading.Thread):
        '''Utility class used to capture key presses.
        '''

        def __init__(self):
            '''
            Constructor.
            '''
            threading.Thread.__init__(self)
            self.__last_key_press = None

        def run(self):
            '''Override thread run. Wait until a key press is received.
            '''
            # Wait here.
            while not self.__last_key_press:
                time.sleep(.1)

        def activate(self, key_press):
            '''Activity interface method. Stores the last key press enum.

            Args:
                key_press: The internal enum representing the key press code.
            '''
            # pylint: disable=logging-format-interpolation
            self.__last_key_press = key_press

            return False

        def get_key_press(self):
            '''Return the key press.
            '''
            return self.__last_key_press

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
