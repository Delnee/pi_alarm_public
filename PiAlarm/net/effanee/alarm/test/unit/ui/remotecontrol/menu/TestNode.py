'''
Created on 4 May 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.ui.remotecontrol.menu.Node import Node
#from net.effanee.alarm.test.Configure import Configure
from net.effanee.alarm.ui.remotecontrol.menu.Properties import Properties

class TestNode(unittest.TestCase):
    '''Unit test.
    '''
    def test_get_name(self):
        '''Get properties name.
        '''
        prop_name = "Test Node"
        props = Properties(prop_name, Properties.INTERNAL)
        node = Node(props, None)
        node.get_properties().get_name()

        self.assertEquals(prop_name, node.get_properties().get_name())

    def test_parent(self):
        '''Get parent.
        '''
        parent_prop_name = "Test Node Parent"
        parent_props = Properties(parent_prop_name, Properties.INTERNAL)
        parent_node = Node(parent_props, None)

        child_prop_name = "Test Node Child"
        child_props = Properties(child_prop_name, Properties.INTERNAL)
        child_node = parent_node.create_child_after(child_props)

        self.assertEquals(parent_node, child_node.get_parent())

    def test_children(self):
        '''Get children.
        '''
        parent_prop_name = "Test Node Parent"
        parent_props = Properties(parent_prop_name, Properties.INTERNAL)
        parent_node = Node(parent_props, None)

        child1_prop_name = "Test Node Child1"
        child1_props = Properties(child1_prop_name, Properties.INTERNAL)
        child1_node = parent_node.create_child_after(child1_props)

        child2_prop_name = "Test Node Child2"
        child2_props = Properties(child2_prop_name, Properties.INTERNAL)
        child2_node = parent_node.create_child_after(child2_props)

        children = parent_node.get_children()

        self.assertEquals(child1_node, children[0])
        self.assertEquals(child2_node, children[1])

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
