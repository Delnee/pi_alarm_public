'''
Created on 4 May 2015

@author: fjh
'''
import unittest
#from net.effanee.alarm.test.Configure import Configure
from net.effanee.alarm.ui.remotecontrol.menu.Properties import Properties
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum

# pylint: disable=no-self-use
class TestProperties(unittest.TestCase):
    '''Unit test.
    '''
    # Test state switches.
    _set_on_focus = False
    _set_on_select = False
    _set_on_input = False

    def test_get_name(self):
        '''Get name.
        '''
        prop_name = "Test Properties"
        props = Properties(prop_name, Properties.INTERNAL)
        self.assertEquals(prop_name, props.get_name())

    def test_get_node_type(self):
        '''Get node type.
        '''
        prop_name = "Test Properties"
        props = Properties(prop_name, Properties.INTERNAL)
        self.assertEquals(Properties.INTERNAL, props.get_node_type())

    def test_on_focus(self):
        '''Internal node on focus.
        '''
        prop_name = "Test Properties"#

        # Set focused.
        props = Properties(prop_name, Properties.INTERNAL)
        props.register_on_focus_activity(self.set_on_focus_true)
        props.set_focused()
        self.assertTrue(props.is_focused())
        self.assertTrue(TestProperties._set_on_focus)

        # Unset focused.
        props.unset_focused()
        self.set_on_focus_false()
        self.assertFalse(props.is_focused())
        self.assertFalse(TestProperties._set_on_focus)

        # Set focused.
        props.set_focused()
        self.assertTrue(props.is_focused())
        self.assertTrue(TestProperties._set_on_focus)

    def set_on_focus_true(self):
        '''Set on focus to True.
        '''
        TestProperties._set_on_focus = True

    def set_on_focus_false(self):
        '''Set on focus to False.
        '''
        TestProperties._set_on_focus = False

    def test_on_select(self):
        '''Internal node on select.
        '''
        prop_name = "Test Properties"#

        # Set selected.
        props = Properties(prop_name, Properties.INTERNAL)
        props.register_on_select_activity(self.set_on_select_true)
        props.set_selected()
        self.assertTrue(props.is_selected())
        self.assertTrue(TestProperties._set_on_select)

        # Unset selected.
        props.unset_selected()
        self.set_on_select_false()
        self.assertFalse(props.is_selected())
        self.assertFalse(TestProperties._set_on_select)

        # Set selected.
        props.set_selected()
        self.assertTrue(props.is_selected())
        self.assertTrue(TestProperties._set_on_select)

    def set_on_select_true(self):
        '''Set on select to True.
        '''
        TestProperties._set_on_select = True

    def set_on_select_false(self):
        '''Set on select to False.
        '''
        TestProperties._set_on_select = False

    def test_on_input(self):
        '''Internal node on input.
        '''
        prop_name = "Test Properties"#

        # Set inputting.
        props = Properties(prop_name, Properties.INTERNAL)
        props.register_on_input_activity(self.set_on_input_true)
        props.set_input(KeysEnum.KEY_ENTER)
        self.assertTrue(props.is_inputting())
        self.assertTrue(TestProperties._set_on_input)

        # Unset inputting.
        props.unset_input()
        self.set_on_input_false(KeysEnum.KEY_ENTER)
        self.assertFalse(props.is_inputting())
        self.assertFalse(TestProperties._set_on_input)

        # Set inputting.
        props.set_input(KeysEnum.KEY_ENTER)
        self.assertTrue(props.is_inputting())
        self.assertTrue(TestProperties._set_on_input)

    # pylint: disable=unused-argument
    def set_on_input_true(self, key_press):
        '''Set on input to True.
        '''
        TestProperties._set_on_input = True

    # pylint: disable=unused-argument
    def set_on_input_false(self, key_press):
        '''Set on input to False.
        '''
        TestProperties._set_on_input = False

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
