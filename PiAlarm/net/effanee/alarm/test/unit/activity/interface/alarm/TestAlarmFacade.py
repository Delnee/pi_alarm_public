'''
Created on 25 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.interface.alarm.AlarmFacade import AlarmFacade
import datetime
import inspect
from net.effanee.alarm.activity.implementation.alarm.AlarmImpl import AlarmImpl
import os

class TestAlarmFacade(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should not be instantiable.
        '''
        self.assertRaises(RuntimeError, AlarmFacade)

    def test_is_class(self):
        '''Should be class.
        '''
        AlarmFacade.set_implementation(AlarmImpl)
        self.assertTrue(inspect.isclass(AlarmFacade))

    def test_switch_on(self):
        '''Can switch on.
        '''
        AlarmFacade.set_implementation(AlarmImpl)
        AlarmFacade.switch_on()
        self.assertTrue(AlarmFacade.is_on())

    def test_switch_off(self):
        '''Can switch off.
        '''
        AlarmFacade.set_implementation(AlarmImpl)
        AlarmFacade.switch_off()
        self.assertFalse(AlarmFacade.is_on())

    def test_switching(self):
        '''Can switch on, off and on.
        '''
        AlarmFacade.set_implementation(AlarmImpl)
        AlarmFacade.switch_on()
        self.assertTrue(AlarmFacade.is_on())
        AlarmFacade.switch_off()
        self.assertFalse(AlarmFacade.is_on())
        AlarmFacade.switch_on()
        self.assertTrue(AlarmFacade.is_on())

    def test_set_alarm_time(self):
        '''Can set time.
        '''
        AlarmFacade.set_implementation(AlarmImpl)
        test_time = datetime.time(9, 30, 0)
        AlarmFacade.set_alarm_time(test_time)
        self.assertTrue(AlarmFacade.get_alarm_time() == test_time)

    def test_wav_file_exists(self):
        '''Wav file exists.
        '''
        AlarmFacade.set_implementation(AlarmImpl)
        wav = AlarmFacade.get_wav_file()
        self.assertTrue(os.path.isfile(wav))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
