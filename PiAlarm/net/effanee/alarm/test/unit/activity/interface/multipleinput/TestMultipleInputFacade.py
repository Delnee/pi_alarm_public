'''
Created on 27 Apr 2015

@author: fjh
'''
# pylint: disable=line-too-long
import unittest
from net.effanee.alarm.activity.interface.multipleinput.MultipleInputFacade import MultipleInputFacade
import inspect
import datetime
from net.effanee.alarm.activity.implementation.multipleinput.MultipleInputImpl import MultipleInputImpl
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum

class TestMultipleInputFacade(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should not be instantiable.
        '''
        self.assertRaises(RuntimeError, MultipleInputFacade)

    def test_is_class(self):
        '''Should be class.
        '''
        self.assertTrue(inspect.isclass(MultipleInputFacade))

    def test_init_alarm_time(self):
        '''Initialise time.
        '''
        MultipleInputFacade.set_implementation(MultipleInputImpl)
        init_time = datetime.time(19, 30, 0)
        MultipleInputFacade.init_alarm_time(init_time)
        alarm_time = MultipleInputFacade.get_alarm_time()
        self.assertTrue(alarm_time == init_time)

    def test_set_time_main_case(self):
        '''Set four digit time.
        '''
        MultipleInputFacade.set_implementation(MultipleInputImpl)
        MultipleInputFacade.set_alarm_time()
        MultipleInputFacade.activate(KeysEnum.KEY_KP1)
        MultipleInputFacade.activate(KeysEnum.KEY_KP4)
        MultipleInputFacade.activate(KeysEnum.KEY_KP4)
        MultipleInputFacade.activate(KeysEnum.KEY_KP4)
        alarm_time = MultipleInputFacade.get_alarm_time()

        expected_time = datetime.time(14, 44, 0)
        self.assertTrue(alarm_time == expected_time)

    def test_set_time_undo_case(self):
        '''Part set time, but undo and exit, retaining original time.
        '''
        MultipleInputFacade.set_implementation(MultipleInputImpl)
        # Set default.
        init_time = datetime.time(19, 30, 0)
        MultipleInputFacade.init_alarm_time(init_time)

        MultipleInputFacade.set_alarm_time()
        MultipleInputFacade.activate(KeysEnum.KEY_KP1)
        MultipleInputFacade.activate(KeysEnum.KEY_KP4)
        MultipleInputFacade.activate(KeysEnum.KEY_KP4)
        MultipleInputFacade.activate(KeysEnum.KEY_BACK)
        MultipleInputFacade.activate(KeysEnum.KEY_BACK)
        MultipleInputFacade.activate(KeysEnum.KEY_BACK)
        MultipleInputFacade.activate(KeysEnum.KEY_BACK)
        alarm_time = MultipleInputFacade.get_alarm_time()

        self.assertTrue(alarm_time == init_time)

    # pylint: disable=invalid-name
    def test_set_time_undo_immediately_case(self):
        '''Set time, but undo and exit, immediately.
        '''
        MultipleInputFacade.set_implementation(MultipleInputImpl)
        # Set default.
        init_time = datetime.time(19, 30, 0)
        MultipleInputFacade.init_alarm_time(init_time)

        MultipleInputFacade.set_alarm_time()
        MultipleInputFacade.activate(KeysEnum.KEY_BACK)
        alarm_time = MultipleInputFacade.get_alarm_time()

        self.assertTrue(alarm_time == init_time)

    def test_set_time_bad_keys_case(self):
        '''Set four digit time, but include bad key presses.
        '''
        MultipleInputFacade.set_implementation(MultipleInputImpl)
        MultipleInputFacade.set_alarm_time()
        # Bad.
        MultipleInputFacade.activate(KeysEnum.KEY_KP6)
        # Bad.
        MultipleInputFacade.activate(KeysEnum.KEY_KP4)
        # Bad.
        MultipleInputFacade.activate(KeysEnum.KEY_KP4)
        # Good.
        MultipleInputFacade.activate(KeysEnum.KEY_KP0)
        # Good.
        MultipleInputFacade.activate(KeysEnum.KEY_KP1)
        # Bad.
        MultipleInputFacade.activate(KeysEnum.KEY_KP8)
        # Good.
        MultipleInputFacade.activate(KeysEnum.KEY_KP5)
        # Good.
        MultipleInputFacade.activate(KeysEnum.KEY_KP0)

        alarm_time = MultipleInputFacade.get_alarm_time()

        expected_time = datetime.time(01, 50, 0)
        self.assertTrue(alarm_time == expected_time)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
