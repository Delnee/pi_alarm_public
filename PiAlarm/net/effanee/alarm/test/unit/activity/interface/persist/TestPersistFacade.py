'''
Created on 27 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.interface.persist.PersistFacade import PersistFacade
import inspect
import datetime
from net.effanee.alarm.activity.implementation.persist.PersistImpl import PersistImpl

class TestPersistFacade(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should not be instantiable.
        '''
        self.assertRaises(RuntimeError, PersistFacade)

    def test_is_class(self):
        '''Should be class.
        '''
        self.assertTrue(inspect.isclass(PersistFacade))

    def test_set_alarm_alarm_time(self):
        '''Set alarm time.
        '''
        PersistFacade.set_implementation(PersistImpl)
        set_time = datetime.time(20, 30, 0)
        PersistFacade.set_alarm_alarm_time(set_time)

        get_time = PersistFacade.get_alarm_alarm_time()
        self.assertTrue(set_time == get_time)

    # pylint: disable=invalid-name
    def test_get_multipleinput_init_alarm_time(self):
        '''Get multiple input initial alarm time.
        '''
        PersistFacade.set_implementation(PersistImpl)
        set_time = datetime.time(21, 30, 0)
        PersistFacade.set_alarm_alarm_time(set_time)

        get_time = PersistFacade.get_multipleinput_init_alarm_time()
        self.assertTrue(set_time == get_time)

    def test_set_alarm_is_on(self):
        '''Set alarm on.
        '''
        PersistFacade.set_implementation(PersistImpl)
        PersistFacade.set_alarm_is_on(True)
        is_on = PersistFacade.get_alarm_is_on()
        self.assertTrue(is_on)

    def test_clock_chimes_are_on(self):
        '''Set chimes on.
        '''
        PersistFacade.set_implementation(PersistImpl)
        PersistFacade.set_clock_chimes_are_on(True)
        is_on = PersistFacade.get_clock_chimes_are_on()
        self.assertTrue(is_on)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
