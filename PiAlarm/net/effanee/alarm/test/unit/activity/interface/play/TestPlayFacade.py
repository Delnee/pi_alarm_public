'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
import inspect
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum

class TestPlayFacade(unittest.TestCase):
    '''Unit test.
    '''
    _play_facade = None

    @classmethod
    def setUpClass(cls):
        '''The implementation should be thread safe. - BUT ISN'T!
        '''
        cls._play_facade = PlayFacade.set_implementation(CliPlayImpl())

    @classmethod
    def tearDownClass(cls):
        cls._play_facade = None

    def test_instantiation_fail(self):
        '''Should not be instantiable.
        '''
        self.assertRaises(RuntimeError, PlayFacade)

    def test_is_class(self):
        '''Should be class.
        '''
        self.assertTrue(inspect.isclass(PlayFacade))

    def test_sources(self):
        '''Set source to Radio 1 and compare.
        '''
        PlayFacade.set_implementation(CliPlayImpl())
        source_name = "Radio 1"
        sources = PlayFacade.get_sources()

        radio_1 = None
        for source in sources:
            if source == source_name:
                radio_1 = source

        self.assertTrue(radio_1)
        PlayFacade.set_source(radio_1)
        self.assertEqual(radio_1, PlayFacade.get_source())

    def test_play_status(self):
        '''Set and compare play, pause and stop.
        '''
        PlayFacade.set_implementation(CliPlayImpl())
        # Reset.
        PlayFacade.stop()

        # Play
        PlayFacade.play_pause()
        self.assertTrue(PlayFacade.is_playing())

        # Pause
        PlayFacade.play_pause()
        self.assertTrue(PlayFacade.is_paused())

        # Stopped
        PlayFacade.stop()
        self.assertTrue(PlayFacade.is_stopped())

    def test_activate(self):
        '''Activate using key press.
        '''
        PlayFacade.set_implementation(CliPlayImpl())
        # Reset
        PlayFacade.stop()

        # Play
        PlayFacade.activate(KeysEnum.KEY_PLAYPAUSE)
        self.assertTrue(PlayFacade.is_playing())

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
