'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.interface.schedule.SchedulerFacade import SchedulerFacade
import inspect

class TestSchedulerFacade(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should not be instantiable.
        '''
        self.assertRaises(RuntimeError, SchedulerFacade)

    def test_is_class(self):
        '''Should be class.
        '''
        self.assertTrue(inspect.isclass(SchedulerFacade))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
