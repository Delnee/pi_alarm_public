'''
Created on 27 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.interface.clock.ClockFacade import ClockFacade
import inspect
import datetime
import os
from net.effanee.alarm.activity.implementation.clock.ClockImpl import ClockImpl

class TestClockFacade(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should not be instantiable.
        '''
        self.assertRaises(RuntimeError, ClockFacade)

    def test_is_class(self):
        '''Should be class.
        '''
        self.assertTrue(inspect.isclass(ClockFacade))

    def test_get_time(self):
        '''Returned local time is accurate, (within 0.1 second).
        '''
        ClockFacade.set_implementation(ClockImpl)
        now_date_time = datetime.datetime.now()
        clock_time = ClockFacade.get_time()
        clock_date_time = datetime.datetime.combine(now_date_time.today(), clock_time)

        # No later than 0.1 seconds.
        clock_date_time_later = clock_date_time + datetime.timedelta(0, 1)
        self.assertFalse(((clock_date_time_later - now_date_time).total_seconds()) > 1.1)

        # No earlier than 0.1 seconds.
        clock_date_time_earlier = clock_date_time + datetime.timedelta(0, -1)
        self.assertFalse(((now_date_time - clock_date_time_earlier).total_seconds()) > 1.1)

    def test_chimes_on(self):
        '''Can switch on chimes.
        '''
        ClockFacade.set_implementation(ClockImpl)
        ClockFacade.chimes_on()
        self.assertTrue(ClockFacade.chimes_are_on())

    def test_chimes_off(self):
        '''Can switch off chimes.
        '''
        ClockFacade.set_implementation(ClockImpl)
        ClockFacade.chimes_off()
        self.assertFalse(ClockFacade.chimes_are_on())

    def test_chimes_switching(self):
        '''Can switch chimes on, off and on.
        '''
        ClockFacade.set_implementation(ClockImpl)
        ClockFacade.chimes_on()
        self.assertTrue(ClockFacade.chimes_are_on())
        ClockFacade.chimes_off()
        self.assertFalse(ClockFacade.chimes_are_on())
        ClockFacade.chimes_on()
        self.assertTrue(ClockFacade.chimes_are_on())

    def test_chime_wav_file_exists(self):
        '''Chime wav file exists.
        '''
        ClockFacade.set_implementation(ClockImpl)
        wav = ClockFacade.get_random_chime_wav()
        self.assertTrue(os.path.isfile(wav))

    def test_all_chime_wav_files_exist(self):
        '''All chime wav files exist.
        '''
        ClockFacade.set_implementation(ClockImpl)
        wav_files = ClockFacade.get_all_chime_wavs()
        for wav_file in wav_files:
            self.assertTrue(os.path.isfile(wav_file))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
