'''
Created on 25 Apr 2015

@author: fjh
'''
# pylint: disable=line-too-long
import unittest
from net.effanee.alarm.activity.implementation.multipleinput.MultipleInputImpl import MultipleInputImpl
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum
import inspect
import datetime

class TestMultipleInputImpl(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should not be instantiable.
        '''
        self.assertRaises(RuntimeError, MultipleInputImpl)

    def test_is_class(self):
        '''Should be class.
        '''
        multiple_input_impl = MultipleInputImpl
        multiple_input_impl.init()
        self.assertTrue(inspect.isclass(multiple_input_impl))

    def test_init_alarm_time(self):
        '''Initialise time.
        '''
        multiple_input_impl = MultipleInputImpl
        multiple_input_impl.init()
        init_time = datetime.time(19, 30, 0)
        multiple_input_impl.init_alarm_time(init_time)
        alarm_time = multiple_input_impl.get_alarm_time()
        self.assertTrue(alarm_time == init_time)

    def test_set_time_main_case(self):
        '''Set four digit time.
        '''
        multiple_input_impl = MultipleInputImpl
        multiple_input_impl.init()
        multiple_input_impl.set_alarm_time()
        multiple_input_impl.activate(KeysEnum.KEY_KP1)
        multiple_input_impl.activate(KeysEnum.KEY_KP4)
        multiple_input_impl.activate(KeysEnum.KEY_KP4)
        multiple_input_impl.activate(KeysEnum.KEY_KP4)
        alarm_time = multiple_input_impl.get_alarm_time()

        expected_time = datetime.time(14, 44, 0)
        self.assertTrue(alarm_time == expected_time)

    def test_set_time_undo_case(self):
        '''Part set time, but undo and exit, retaining original time.
        '''
        # Set default.
        multiple_input_impl = MultipleInputImpl
        multiple_input_impl.init()
        init_time = datetime.time(19, 30, 0)
        multiple_input_impl.init_alarm_time(init_time)

        multiple_input_impl.set_alarm_time()
        multiple_input_impl.activate(KeysEnum.KEY_KP1)
        multiple_input_impl.activate(KeysEnum.KEY_KP4)
        multiple_input_impl.activate(KeysEnum.KEY_KP4)
        multiple_input_impl.activate(KeysEnum.KEY_BACK)
        multiple_input_impl.activate(KeysEnum.KEY_BACK)
        multiple_input_impl.activate(KeysEnum.KEY_BACK)
        multiple_input_impl.activate(KeysEnum.KEY_BACK)
        alarm_time = multiple_input_impl.get_alarm_time()

        self.assertTrue(alarm_time == init_time)

    # pylint: disable=invalid-name
    def test_set_time_undo_immediately_case(self):
        '''Set time, but undo and exit, immediately.
        '''
        # Set default.
        multiple_input_impl = MultipleInputImpl
        multiple_input_impl.init()
        init_time = datetime.time(19, 30, 0)
        multiple_input_impl.init_alarm_time(init_time)

        multiple_input_impl.set_alarm_time()
        multiple_input_impl.activate(KeysEnum.KEY_BACK)
        alarm_time = multiple_input_impl.get_alarm_time()

        self.assertTrue(alarm_time == init_time)

    def test_set_time_bad_keys_case(self):
        '''Set four digit time, but include bad key presses.
        '''
        # Set default.
        multiple_input_impl = MultipleInputImpl
        multiple_input_impl.init()
        multiple_input_impl.set_alarm_time()
        # Bad.
        multiple_input_impl.activate(KeysEnum.KEY_KP6)
        # Bad.
        multiple_input_impl.activate(KeysEnum.KEY_KP4)
        # Bad.
        multiple_input_impl.activate(KeysEnum.KEY_KP4)
        # Good.
        multiple_input_impl.activate(KeysEnum.KEY_KP0)
        # Good.
        multiple_input_impl.activate(KeysEnum.KEY_KP1)
        # Bad.
        multiple_input_impl.activate(KeysEnum.KEY_KP8)
        # Good.
        multiple_input_impl.activate(KeysEnum.KEY_KP5)
        # Good.
        multiple_input_impl.activate(KeysEnum.KEY_KP0)

        alarm_time = multiple_input_impl.get_alarm_time()

        expected_time = datetime.time(01, 50, 0)
        self.assertTrue(alarm_time == expected_time)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
