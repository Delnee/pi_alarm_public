'''
Created on 25 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.implementation.persist.PersistImpl import PersistImpl
import inspect
import datetime

class TestPersistImpl(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should not be instantiable.
        TODO - Consider, reading current value and restoring it, after test.
        '''
        self.assertRaises(RuntimeError, PersistImpl)

    def test_is_class(self):
        '''Should be class.
        '''
        persist_impl = PersistImpl
        self.assertTrue(inspect.isclass(persist_impl))

    def test_set_alarm_alarm_time(self):
        '''Set alarm time.
        '''
        persist_impl = PersistImpl
        persist_impl.init()
        set_time = datetime.time(20, 30, 0)
        persist_impl.set_alarm_alarm_time(set_time)

        get_time = persist_impl.get_alarm_alarm_time()
        self.assertTrue(set_time == get_time)

    # pylint: disable=invalid-name
    def test_get_multipleinput_init_alarm_time(self):
        '''Get multiple input initial alarm time.
        '''
        persist_impl = PersistImpl
        persist_impl.init()
        set_time = datetime.time(21, 30, 0)
        persist_impl.set_alarm_alarm_time(set_time)

        get_time = persist_impl.get_multipleinput_init_alarm_time()
        self.assertTrue(set_time == get_time)

    def test_set_alarm_is_on(self):
        '''Set alarm on.
        '''
        persist_impl = PersistImpl
        persist_impl.init()
        persist_impl.set_alarm_is_on(True)
        is_on = persist_impl.get_alarm_is_on()
        self.assertTrue(is_on)

    def test_clock_chimes_are_on(self):
        '''Set chimes on.
        '''
        persist_impl = PersistImpl
        persist_impl.init()
        persist_impl.set_clock_chimes_are_on(True)
        is_on = persist_impl.get_clock_chimes_are_on()
        self.assertTrue(is_on)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
