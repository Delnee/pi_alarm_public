'''
Created on 25 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.implementation.alarm.AlarmImpl import AlarmImpl
import inspect
import datetime
import os

class TestAlarmImpl(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should not be instantiable.
        '''
        self.assertRaises(RuntimeError, AlarmImpl)

    def test_is_class(self):
        '''Should be class.
        '''
        alarm_impl = AlarmImpl
        self.assertTrue(inspect.isclass(alarm_impl))

    def test_switch_on(self):
        '''Can switch on.
        '''
        alarm_impl = AlarmImpl
        alarm_impl.switch_on()
        self.assertTrue(alarm_impl.is_on())

    def test_switch_off(self):
        '''Can switch off.
        '''
        alarm_impl = AlarmImpl
        alarm_impl.switch_off()
        self.assertFalse(alarm_impl.is_on())

    def test_switching(self):
        '''Can switch on, off and on.
        '''
        alarm_impl = AlarmImpl
        alarm_impl.switch_on()
        self.assertTrue(alarm_impl.is_on())
        alarm_impl.switch_off()
        self.assertFalse(alarm_impl.is_on())
        alarm_impl.switch_on()
        self.assertTrue(alarm_impl.is_on())

    def test_set_alarm_time(self):
        '''Can set time.
        '''
        alarm_impl = AlarmImpl
        test_time = datetime.time(9, 30, 0)
        alarm_impl.set_alarm_time(test_time)
        self.assertTrue(alarm_impl.get_alarm_time() == test_time)

    def test_wav_file_exists(self):
        '''Wav file exists.
        '''
        alarm_impl = AlarmImpl
        alarm_impl.init()
        wav = alarm_impl.get_wav_file()
        self.assertTrue(os.path.isfile(wav))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
