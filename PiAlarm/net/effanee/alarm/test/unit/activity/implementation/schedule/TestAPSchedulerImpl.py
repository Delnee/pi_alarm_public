'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.implementation.schedule.APSchedulerImpl import APSchedulerImpl
import inspect

class TestAPSchedulerImpl(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should not be instantiable.
        '''
        self.assertRaises(RuntimeError, APSchedulerImpl)

    def test_is_class(self):
        '''Should be class.
        '''
        scheduler_impl = APSchedulerImpl
        self.assertTrue(inspect.isclass(scheduler_impl))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
