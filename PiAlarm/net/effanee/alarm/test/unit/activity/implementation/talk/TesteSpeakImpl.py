'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.implementation.talk.eSpeakImpl import eSpeakImpl
import inspect

class TesteSpeakImpl(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should not be instantiable.
        '''
        self.assertRaises(RuntimeError, eSpeakImpl)

    def test_is_class(self):
        '''Should be class.
        '''
        speak_impl = eSpeakImpl
        self.assertTrue(inspect.isclass(speak_impl))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
