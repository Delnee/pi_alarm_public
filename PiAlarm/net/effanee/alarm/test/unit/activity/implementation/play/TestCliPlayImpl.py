'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum

class TestCliPlayImpl(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should be instantiable.
        '''
        try:
            CliPlayImpl()
        except RuntimeError:
            self.fail()

    def test_is_class(self):
        '''Should be instance of class.
        '''
        play_impl = CliPlayImpl()
        self.assertIsInstance(play_impl, CliPlayImpl)

    def test_sources(self):
        '''Set source to Radio 1 and compare.
        '''
        source_name = "Radio 1"
        play_impl = CliPlayImpl()
        sources = play_impl.get_sources()
        print sources
        radio_1 = None
        for source in sources:
            if source == source_name:
                radio_1 = source

        self.assertTrue(radio_1)
        play_impl.set_source(radio_1)
        self.assertEqual(radio_1, play_impl.get_source())

    def test_play_status(self):
        '''Set and compare play, pause and stop.
        '''
        play_impl = CliPlayImpl()

        # Play
        play_impl.play_pause()
        self.assertTrue(play_impl.is_playing())

        # Pause
        play_impl.play_pause()
        self.assertTrue(play_impl.is_paused())

        # Stopped
        play_impl.stop()
        self.assertTrue(play_impl.is_stopped())

    def test_activate(self):
        '''Activate using key press.
        '''
        play_impl = CliPlayImpl()

        # Play
        play_impl.activate(KeysEnum.KEY_PLAYPAUSE)
        self.assertTrue(play_impl.is_playing())

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
