'''
Created on 29 Apr 2015

@author: fjh
'''
import unittest
from net.effanee.alarm.activity.action.AudioAction import AudioAction
# from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
# from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
# from net.effanee.alarm.activity.interface.talk.SpeechFacade import SpeechFacade
# from net.effanee.alarm.activity.implementation.talk.eSpeakImpl import eSpeakImpl

class TestAudioAction(unittest.TestCase):
    '''Unit test.
    '''
    def test_instantiation_fail(self):
        '''Should be instantiable.
        '''
        try:
            AudioAction()
        except RuntimeError:
            self.fail()

    def test_is_class(self):
        '''Should be instance of class.
        '''
        audio_action_impl = AudioAction()
        self.assertIsInstance(audio_action_impl, AudioAction)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
