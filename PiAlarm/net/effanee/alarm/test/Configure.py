'''
Created on 12 Mar 2015

@author: fjh
'''
# pylint: disable=line-too-long
import logging
import os
from net.effanee.alarm.activity.interface.multipleinput.MultipleInputFacade import MultipleInputFacade
from net.effanee.alarm.activity.implementation.multipleinput.MultipleInputImpl import MultipleInputImpl
from net.effanee.alarm.activity.interface.clock.ClockFacade import ClockFacade
from net.effanee.alarm.activity.implementation.clock.ClockImpl import ClockImpl
from net.effanee.alarm.activity.interface.talk.SpeechFacade import SpeechFacade
from net.effanee.alarm.activity.implementation.talk.eSpeakImpl import eSpeakImpl
from net.effanee.alarm.activity.interface.alarm.AlarmFacade import AlarmFacade
from net.effanee.alarm.activity.implementation.alarm.AlarmImpl import AlarmImpl
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
from net.effanee.alarm.activity.interface.schedule.SchedulerFacade import SchedulerFacade
from net.effanee.alarm.activity.implementation.schedule.APSchedulerImpl import APSchedulerImpl
from net.effanee.alarm.activity.interface.persist.PersistFacade import PersistFacade
from net.effanee.alarm.activity.implementation.persist.PersistImpl import PersistImpl

# pylint: disable=too-few-public-methods
class Configure(object):
    '''Configuration details for each test.

    This class creates the environment for the tests. It must be imported into
    a test case before the class under test. This will cause it's class methods
    be run before the class under test is run. When this class is imported, it
    calls itself on it's last line to run the configuration methods.
    '''

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def init(cls):
        '''Initialise this class.
        '''
        cls.__config_logging()
        cls.__init_implementations()

    @classmethod
    def __init_implementations(cls):
        '''?
        '''
        MultipleInputFacade.set_implementation(MultipleInputImpl)
        ClockFacade.set_implementation(ClockImpl)
        SpeechFacade.set_implementation(eSpeakImpl)
        AlarmFacade.set_implementation(AlarmImpl)
        PlayFacade.set_implementation(CliPlayImpl())
        SchedulerFacade.set_implementation(APSchedulerImpl)
        PersistFacade.set_implementation(PersistImpl)

    @classmethod
    # pylint: disable=no-self-use
    def __config_logging(cls):
        '''Format the logging output.
        '''
        # Log file location.
        this_path = os.path.dirname(os.path.abspath(__file__)) + "/test.log"
        log_path = os.path.abspath(this_path)

        # Create file logger.
        logging.basicConfig(format='%(asctime)s %(name)s:%(levelname)s - %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p',
                            filename=log_path,
                            filemode='a',
                            level=logging.DEBUG)

        # Add console logger.
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        # pylint: disable=line-too-long
        formatter = logging.Formatter(fmt='%(asctime)s %(name)s:%(levelname)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)

Configure.init()
