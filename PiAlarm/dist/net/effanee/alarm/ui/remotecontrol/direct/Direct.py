'''
Created on 31 Mar 2015

@author: fjh
'''
import logging
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum
from net.effanee.alarm.activity.action.AudioAction import AudioAction
from net.effanee.alarm.activity.action.CommonAction import CommonAction
from net.effanee.alarm.activity.action.ClockAction import ClockAction
# pylint: disable=too-few-public-methods
class Direct(object):
    '''This class is used to respond directly to single key presses on the
    remote control.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self):
        '''Map the key enums to methods, and obtain Action instances to
        perform the actions.
        '''

        self.__direct_key_mappings = {
            KeysEnum.KEY_VOLUMEDOWN: self.__volume_down,
            KeysEnum.KEY_PLAYPAUSE: self.__play_pause,
            KeysEnum.KEY_VOLUMEUP: self.__volume_up,
            KeysEnum.KEY_STOP: self.__stop,
            KeysEnum.KEY_SETUP: self.__setup
            }

        # Get the direct key press actions.
        self.__audio_action = AudioAction()
        self.__common_action = CommonAction()
        self.__clock_action = ClockAction()

    def __volume_up(self):
        '''Volume up.
        '''
        self.__audio_action.volume_up()

    def __volume_down(self):
        '''Volume down.
        '''
        self.__audio_action.volume_down()

    def __play_pause(self):
        '''Play/pause playing toggle.
        '''
        self.__audio_action.play_pause()

    def __stop(self):
        '''Stop playing.
        '''
        self.__audio_action.stop()

    def __setup(self):
        '''Say the device status.
        '''
        self.__clock_action.alarm_toggle()
        #self.__common_action.say_status()

    def activate(self, key_press):
        '''Activity interface method. Uses the menu_key_mappings map to
        select which method to invoke.

        Args:
            key_press - The key press enum representing the key press code.
        '''
        # pylint: disable=logging-format-interpolation
        if key_press in self.__direct_key_mappings:
            self.__class__.logger.info("Activating key press {0} in activity {1}."
                                       .format(key_press, self.__class__.__name__))

            self.__direct_key_mappings[key_press]()

        else:
            self.__class__.logger.warn("Key press {0}, in activity {1}, "
                                       "does not map to an interface method."
                                       .format(key_press, self.__class__.__name__))

        return False
        