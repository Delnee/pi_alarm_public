'''
Created on 19 Mar 2015

@author: fjh
'''
import logging

class Node(object):
    '''A node object is used to build the structure of a menu tree.

    A node has an embedded Properties object, which stores the properties that
    are associated with this node, such as whether it is focused or selected.

    A node is part of a bigger menu structure and stores references to its
    parent and its children. A menu is not intended to be a dynamic structure.
    Once it is created it is not meant to be changed. The add_child methods
    are used as a convenience during menu creation.

    The encapsulated Properties object is intended to be mutable.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self, properties, parent):
        '''Create a new Node.

        Args:
            properties - An Properties object to associate with this node.
            parent - A node that will be this nodes parent.
        '''
        self.__properties = properties
        self.__parent = parent
        self.__children = []

    def get_parent(self):
        ''' Get this nodes parent node.

        Returns:
            This nodes parent, or None if the node is the root.
        '''
        return self.__parent

    def get_children(self):
        ''' Get this nodes children.

        Returns:
            A list of this node children, for leaf nodes an empty list.
        '''
        return self.__children

    def get_properties(self):
        '''Get the Properties object associated with this node.

        Returns:
            The Properties object associated with this node.
        '''
        return self.__properties

    def create_child_before(self, properties):
        '''Create a new child of this node, with the supplied properties.

        A new child node is created at the beginning, (top), of the child list.
        If this node has no existing children, a new list is created and the
        new node is appended.

        Args:
            properties - The properties object for the new child node.

        Returns:
            The new child node.
        '''
        child_node = Node(properties, self)
        self.__add_child(child_node, 0)

        return child_node

    def create_child_after(self, properties):
        '''Create a new child of this node, with the supplied properties.

        A new child node is created at the end, (bottom), of the child list.
        If this node has no existing children, a new list is created and the
        new node is appended.

        Args:
            properties - The properties object for the new child node.

        Returns:
            The new child node.
        '''
        child_node = Node(properties, self)

        if self.__children:
            self.__add_child(child_node, len(self.__children))
        else:
            self.__add_child(child_node, 0)

        return child_node

    def __add_child(self, child_node, index):
        '''Add a child node to this node instance.

        Args:
            child_node - The node to add as a child.
            index - The position in the list of children to add the child node.
        '''
        if self.__children:
            self.__children.insert(index, child_node)

        else:
            self.__children.append(child_node)

    def __str__(self):
        '''Override string.
        '''
        # pylint: disable=line-too-long
        return self.__class__.__name__ + "[" \
                "__parent=" + (self.__parent.get_properties().get_name() if self.__parent else "Root") + ", " \
                "__properties=" + str(self.__properties) + ", " \
                "__children=" + (", ".join([(child.get_properties().get_name()) for child in self.__children]) if self.__children else "Leaf") \
                + "]"
