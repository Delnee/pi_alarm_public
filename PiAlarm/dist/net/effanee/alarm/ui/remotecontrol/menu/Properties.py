'''
Created on 19 Mar 2015

@author: fjh
'''
import logging

# pylint: disable=too-many-instance-attributes
class Properties(object):
    '''A Properties object stores state inside a menu Node object. It is the
    active object inside a node. A node is either:-

        INTERNAL - A node without any functionality, other than saying its
        name. Internal nodes describe and group their children.

        LEAF - A node that provides functionality.

    Activities are registered with this object, and invoked when the event
    associated with the activity is triggered. The events are:-

        on focus.
        on select.
        on input.
    '''

    logger = logging.getLogger(__name__)

    # Type enum's.
    INTERNAL = "internal"
    LEAF = "leaf"

    def __init__(self, name, node_type):
        '''Constructor.

        Args:
            name - The string description of this node.
            node_type - Either Properties.INTERNAL or PROPERTIES.LEAF
        '''
        self.__name = name
        self.__node_type = node_type
        self.__is_focused = False
        self.__is_selected = False
        self.__is_inputting = False
        self.__on_focus_activities = []
        self.__on_select_activities = []
        self.__on_input_activities = []

    def get_name(self):
        '''Get this object name.

        Returns:
            This objects name.
        '''
        return self.__name

    def get_node_type(self):
        '''Get this object node type.

        Returns:
            Either INTERNAL or LEAF.
        '''
        return self.__node_type

    def register_on_focus_activity(self, new_activity, *args):
        '''Register an activity.

        Args:
            new_activity - An object method interested in being notified when a
            menu node receives focus.
            *args - Optional arguments for the new_activity method.
        '''
        method_signature = [new_activity] + (list(args))

        if not method_signature in self.__on_focus_activities:
            self.__on_focus_activities.append(method_signature)
            # pylint: disable=logging-format-interpolation, line-too-long
            self.__class__.logger.info("Added on focus activity {0} from {1} with args {2}"
                                       .format(new_activity.__name__, new_activity.__module__, list(args)))

    def __update_on_focus_activities(self):
        '''Update all registered activities.

        Each activities method is invoked.
        '''
        for activity in self.__on_focus_activities:

            # Get the method and any optional arguments.
            method = activity[0]
            if len(activity) > 1:
                params = activity[1:]
            else:
                params = []

            try:
                if params:
                    # pylint: disable=star-args
                    method(*params)
                else:
                    method()
                # pylint: disable=logging-format-interpolation
                self.__class__.logger.info("Invoked on focus activity {0} from {1}."
                                           .format(method.__name__, method.__module__))

            except StandardError as ex:
                # pylint: disable=logging-format-interpolation, line-too-long
                self.__class__.logger.error("Could not invoke activity {0},"
                                            " in response to receiving menu focus, exception was {1}".format(activity, ex))

    def set_focused(self):
        '''Set this object as focused and call it's on focused activities.
        '''
        self.__is_focused = True
        self.__update_on_focus_activities()

    def unset_focused(self):
        '''Set this object as not focused.
        '''
        self.__is_focused = False

    def is_focused(self):
        '''Returns the focused state.

        Returns:
            True - if the node is focused, else False.
        '''
        return self.__is_focused

    def register_on_select_activity(self, new_activity, *args):
        '''Register an activity.

        Args:
            new_activity - An object method interested in being notified when a
            menu node is selected.
            *args - Optional arguments for the new_activity method.
        '''
        method_signature = [new_activity] + (list(args))

        if not method_signature in self.__on_select_activities:
            self.__on_select_activities.append(method_signature)
            # pylint: disable=logging-format-interpolation, line-too-long
            self.__class__.logger.info("Added on select activity {0} from {1} with args {2}"
                                       .format(new_activity.__name__, new_activity.__module__, list(args)))

    def __update_on_select_activities(self):
        '''Update all registered activities.

        Each activities method is invoked.
        '''
        for activity in self.__on_select_activities:

            # Get the method and any optional arguments.
            method = activity[0]
            if len(activity) > 1:
                params = activity[1:]
            else:
                params = []

            try:
                if params:
                    # pylint: disable=star-args
                    method(*params)
                else:
                    method()
                # pylint: disable=logging-format-interpolation
                self.__class__.logger.info("Invoked on select activity {0} from {1}."
                                           .format(method.__name__, method.__module__))

            except StandardError as ex:
                # pylint: disable=logging-format-interpolation, line-too-long
                self.__class__.logger.error("Could not invoke activity {0},"
                                            " in response to receiving menu select, exception was {1}".format(activity, ex))

    def set_selected(self):
        '''Set this object as selected and call it's on selected activities.
        '''
        self.__is_selected = True
        self.__update_on_select_activities()

    def unset_selected(self):
        '''Set this object as not elected.
        '''
        self.__is_selected = False

    def is_selected(self):
        '''Returns the selected state.

        Returns:
            True - if the node is selected, else False.
        '''
        return self.__is_selected

    def register_on_input_activity(self, new_activity):
        '''Register an activity.

        Args:
            new_activity - An object method interested in being notified when a
            menu node receives focus.
        '''
        if not new_activity in self.__on_input_activities:
            self.__on_input_activities.append(new_activity)
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.info("Added on input activity {0} from {1}"
                                       .format(new_activity.__name__, new_activity.__module__))

    def __update_on_input_activities(self, key_press):
        '''Update all registered activities.

        Each activities method is invoked.
        '''
        for activity in self.__on_input_activities:

            # Get the method and any optional arguments.
            try:
                activity(key_press)
                # pylint: disable=logging-format-interpolation
                self.__class__.logger.info("Invoked on input activity {0} from {1}."
                                           .format(activity.__name__, activity.__module__))

            except StandardError as ex:
                # pylint: disable=logging-format-interpolation, line-too-long
                self.__class__.logger.error("Could not invoke activity {0},"
                                            " in response to receiving menu input, exception was {1}".format(activity, ex))

    def set_input(self, key_press):
        '''Set this object to receiving input and call it's on input activities.
        '''
        self.__is_inputting = True
        self.__update_on_input_activities(key_press)

    def unset_input(self):
        '''Set this object as not receiving input.
        '''
        self.__is_inputting = False

    def is_inputting(self):
        '''Returns the inputting state.

        Returns:
            True - If the node is receiving input, else False.
        '''
        return self.__is_inputting

    def __str__(self):
        '''Override string.
        '''
        # pylint: disable=line-too-long
        return self.__class__.__name__ + "[" \
                "__name=" + self.__name + ", " \
                "__is_focused=" + str(self.__is_focused) + ", " \
                "__is_selected=" + str(self.__is_selected) + ", " \
                "__on_focus_activities=" + ", ".join([str(activity) for activity in self.__on_focus_activities]) + ", " \
                "__on_select_activities=" + ", ".join([str(activity) for activity in self.__on_select_activities]) + ", " \
                "__on_input_activities=" + ", ".join([str(activity) for activity in self.__on_input_activities]) \
                + "]"
