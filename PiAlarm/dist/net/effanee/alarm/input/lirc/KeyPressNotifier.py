'''
Created on 10 Feb 2015

@author: fjh
'''
# Annoying conditional imports used for testing on Windows, before deploying
# on the Raspberry Pi.
try:
    # This is the actual library used to access pylirc on the Raspberry Pi,
    # which is installed into the local Python environment.
    import pylirc

except ImportError:
    # This is the mock library used to access pylirc in the development
    # environment, which is PyDev/Eclipse on Windows.
    from net.effanee.alarm.test.mocks.pylirc.pylirc import pylirc

import select, atexit, threading, logging, socket, errno

from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum

class KeyPressNotifier(threading.Thread):
    '''Reads key presses from the IR remote, via the pylirc2 library.

    Runs as a daemon thread. Includes __finalise() method to clean up on thread
    exit.

    Processes incoming IR key presses and passes them on to registered
    listeners. LIRC key constants are translated into this applications key
    enums - net.effanee.alarm.input.lirc.KeysEnum - which are then used
    throughout the rest of the application.
    '''

    logger = logging.getLogger(__name__)

    #The name of the sections within the lircrc file to associate with this
    #application.
    LIRC_PROG = "alarmremote"
    LIRCRC_LOCATION = "/etc/lirc/lircrc"

    #Use non-blocking IO in pylirc2, when reading key presses.
    BLOCKING = 0

    def __init__(self):
        '''
        Constructor
        '''
        threading.Thread.__init__(self)
        self.__key_mappings = self.__initialise_key_mappings()
        atexit.register(self.__finalise)
        self.__is_exiting = False
        self.__listeners = []

    def run(self):
        '''Override thread run.

        Start infinite loop, to read remote key presses.
        '''
        self.__read_keypress()

    def stop(self):
        '''Used to set a flag that this notifier thread is being closed
        legitimately.
        '''
        self.__is_exiting = True

    # pylint: disable=no-self-use
    def __initialise_key_mappings(self):
        '''Bind a set of key constants to LIRC strings.

        Keys - The enum constants used by the application.

        Values - The strings recognised as valid for the constant.
        The strings should correspond to those received from the IR remote, i.e.
        those configured in the lircrc file. So, if the string "KEY_VOLUMEDOWN"
        is received from the remote, it is referenced in the application by
        the constant KeysEnum.KEY_VOLUMEDOWN.

        Returns:
            A dictionary of key mappings.
        '''
        key_mappings = {
            KeysEnum.KEY_VOLUMEDOWN : "KEY_VOLUMEDOWN",
            KeysEnum.KEY_PLAYPAUSE : "KEY_PLAYPAUSE",
            KeysEnum.KEY_VOLUMEUP : "KEY_VOLUMEUP",
            KeysEnum.KEY_SETUP : "KEY_SETUP",
            KeysEnum.KEY_UP : "KEY_UP",
            KeysEnum.KEY_STOP : "KEY_STOP",
            KeysEnum.KEY_LEFT : "KEY_LEFT",
            KeysEnum.KEY_ENTER : "KEY_ENTER",
            KeysEnum.KEY_RIGHT : "KEY_RIGHT",
            KeysEnum.KEY_KP0 : "KEY_KP0",
            KeysEnum.KEY_DOWN : "KEY_DOWN",
            KeysEnum.KEY_BACK : "KEY_BACK",
            KeysEnum.KEY_KP1 : "KEY_KP1",
            KeysEnum.KEY_KP2 : "KEY_KP2",
            KeysEnum.KEY_KP3 : "KEY_KP3",
            KeysEnum.KEY_KP4 : "KEY_KP4",
            KeysEnum.KEY_KP5 : "KEY_KP5",
            KeysEnum.KEY_KP6 : "KEY_KP6",
            KeysEnum.KEY_KP7 : "KEY_KP7",
            KeysEnum.KEY_KP8 : "KEY_KP8",
            KeysEnum.KEY_KP9 : "KEY_KP9"
            }

        return key_mappings

    def register_listener(self, new_listener):
        '''Register a listener.

        Listeners must implement the method self.update(self, keypress).

        Args:
            new_listener - A class interested in being notified when a key is
            pressed on the remote.
        '''
        if not new_listener in self.__listeners:
            self.__listeners.append(new_listener)
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.debug("Added listener {0}".format(new_listener))

    def unregister_listener(self, existing_handler):
        '''Unregister a listener.

        Args:
            existing_handler - A class interested in being notified when a key
            is pressed on the remote, which has been previously registered.
        '''
        if existing_handler in self.__listeners:
            self.__listeners.remove(existing_handler)
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.debug("Removed listener {0}".format(existing_handler))

    def __update_listeners(self, key_press):
        '''Update all registered listeners.

        Each listeners update() method is invoked.

        Args:
            key_press - The internal enum representing the remote key code that
            has been pressed.
        '''
        for listener in self.__listeners:
            try:
                listener.update(key_press)
            except StandardError:
                # pylint: disable=logging-format-interpolation
                self.__class__.logger.error("Could not invoke update method on {0},"
                                            " in response to key {1}".format(listener, key_press))

    def __read_keypress(self):
        '''Read key strings from the remote.

        Uses the library:-

            name = 'pylirc2'
            version = '0.1'
            author = 'Linus McCabe and Paul Hummer'
            author_email = 'Linus@McCabe.nu'
            license = 'lgpl'
            description = 'Python lirc module. See http://www.lirc.org for more
                info on lirc and https://launchpad.net/pylirc2
                and https://code.launchpad.net/~rockstar/pylirc2/trunk

        to read strings from the remote via LIRC.

        This method loops indefinitely.

        Raises:
            socket.error: If a socket cannot be opened to communicate with LIRC.
        '''
        # pylint: disable=line-too-long
        file_descriptor = pylirc.init(KeyPressNotifier.LIRC_PROG, KeyPressNotifier.LIRCRC_LOCATION, KeyPressNotifier.BLOCKING)
        if file_descriptor:

            while True:

                # Non-busy indefinite wait on socket file descriptor.
                # The returned data is discarded and is instead read directly
                # from the pylirc.nextcode() method.
                try:
                    select.select([file_descriptor], [], []) == ([], [], [])  # @NoEffect
                except socket.error, ex:
                    if ex.errno == errno.EBADF and self.__is_exiting:
                        # We expect this error to be thrown when exiting
                        # the thread.
                        self.__class__.logger.debug("Exiting select wait during close down.")
                        break

                # Data has arrived, get the list of key codes.
                key_list = pylirc.nextcode(1)

                # Read the first, and any immediately subsequent, key codes.
                while key_list:

                    # Process the IR remote key press code.
                    for code in key_list:
                        # pylint: disable=logging-format-interpolation, line-too-long
                        self.__class__.logger.debug("Received key press {0}.".format(code["config"]))
                        self.__process_keypress(code["config"])

                    # If more codes are pending, process them too.
                    if not KeyPressNotifier.BLOCKING:
                        key_list = pylirc.nextcode(1)
                    else:
                        key_list = []

        else:
            msg = "Could not open a socket to communicate with LIRC."
            self.__class__.logger.error(msg)
            raise file_descriptor.error(msg)


    def __process_keypress(self, keypress):
        '''Translate the remote key press string into an internal enum, and
        invoke any registered listeners with the key press equivalent enum.

        Args:
            keypress - The string originating fom the remote.
        '''
        if not self.__is_recognised_key(keypress):
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.error("The key press {0} is not recognised.".format(keypress))
            return

        app_keypress = self.__get_app_enum(keypress)
        self.__update_listeners(app_keypress)

    def __get_app_enum(self, keypress):
        '''Get the application enum constant from the remote keypress string.

        Args:
            keypress - The string to match.

        Returns:
            The enum or None.
        '''
        try:
            return self.__key_mappings.keys()[self.__key_mappings.values().index(keypress)]
        except StandardError:
            return None

    def __is_recognised_key(self, keypress):
        '''Is the keypress in the list of keypresses recognised by this
        application?

        Args:
            keypress - The string to test.

        Returns:
            True, if the keypress is recognised.
        '''
        return keypress in self.__key_mappings.itervalues()

    def __finalise(self):
        '''Ensure that the connection to LIRC is properly closed when this
        thread is exited.
        '''
        self.__class__.logger.debug("Exiting.")
        if not pylirc is None:
            pylirc.exit()
