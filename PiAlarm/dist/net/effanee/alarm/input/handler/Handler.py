'''
Created on 12 Feb 2015

@author: fjh
'''
import logging

class Handler(object):
    '''A handler is a named instance, which maps key presses to activities.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self, name, handled_keys):
        '''Constructor.

        Args:
            name - A descriptive name to identify this handler.
            handled_keys - A list of key press enums that this handler will
            handle.
        '''
        self.__name = name
        self.__handled_keys = handled_keys
        self.__activities = []

    def update(self, key_press):
        '''Handler interface method.

        Args:
            key_press - The internal enum representing the remote key press code.

        Returns:
            True if further handlers in the sequence should be invoked, else
            False.
        '''
        # pylint: disable=logging-format-interpolation
        self.__class__.logger.debug("Received key press {0}.".format(key_press))
        do_next = self.__update_activities(key_press)

        return do_next

    def register_activity(self, new_activity):
        '''Register an activity.

        Activities must implement the method self.activate(self, key_press).

        Args:
            new_activity - A class interested in being notified when a key is
            pressed.
        '''
        if not new_activity in self.__activities:
            self.__activities.append(new_activity)
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.debug("Added activity {0}".format(new_activity))

    def unregister_activity(self, existing_activity):
        '''Unregister an activity.

        Args:
            existing_activity - A class interested in being notified when a key
            is pressed, which has been previously registered.
        '''
        if existing_activity in self.__activities:
            self.__activities.remove(existing_activity)
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.debug("Removed acyivity {0}".format(existing_activity))

    def __update_activities(self, key_press):
        '''Update all registered activities.

        Each activities activate() method is invoked.

        Args:
            key_press - The internal enum representing the key code that
            has been pressed.
        '''
        for activity in self.__activities:
            try:
                do_next = activity.activate(key_press)
                # pylint: disable=logging-format-interpolation
                self.__class__.logger.debug("Sent key press {0} to activity {1}."
                                            .format(key_press, activity))
                # Should subsequent activities be called?
                if not do_next:
                    return do_next

            except StandardError as ex:
                # pylint: disable=logging-format-interpolation, line-too-long
                self.__class__.logger.error("Could not invoke activate method on {0},"
                                            " in response to key {1}, exception was {2}".format(activity, key_press, ex))

        return True

    def handled_keys(self):
        '''Handler interface method.
        '''
        return self.__handled_keys

    def __str__(self):
        '''Override string.
        '''
        return self.__class__.__name__ + "[" \
                "__name=" + self.__name + ", " \
                "__handled_keys=" + ", ".join([str(key) for key in self.__handled_keys]) + ", " \
                "__activities" + ", ".join([str(key) for key in self.__activities]) \
                + "]"
