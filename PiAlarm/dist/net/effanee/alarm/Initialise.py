'''
Created on 10 Feb 2015

@author: fjh
'''
#import logging
import logging.handlers

# Handlers.
from net.effanee.alarm.input.handler.Handler import Handler
from net.effanee.alarm.ui.remotecontrol.menu.Menu import Menu
from net.effanee.alarm.ui.remotecontrol.direct.Direct import Direct
from net.effanee.alarm.input.handler.InfraredRemoteHandler import InfraredRemoteHandler

# Key press enums.
from net.effanee.alarm.input.lirc.KeysEnum import KeysEnum

# Listeners.
from net.effanee.alarm.input.lirc.KeyPressNotifier import KeyPressNotifier
from net.effanee.alarm.input.listener.KeyPressListener import KeyPressListener

# Facade/Implementation.
# pylint: disable=line-too-long
from net.effanee.alarm.activity.interface.multipleinput.MultipleInputFacade import MultipleInputFacade
from net.effanee.alarm.activity.implementation.multipleinput.MultipleInputImpl import MultipleInputImpl
from net.effanee.alarm.activity.interface.clock.ClockFacade import ClockFacade
from net.effanee.alarm.activity.implementation.clock.ClockImpl import ClockImpl
from net.effanee.alarm.activity.interface.talk.SpeechFacade import SpeechFacade
from net.effanee.alarm.activity.implementation.talk.eSpeakImpl import eSpeakImpl
from net.effanee.alarm.activity.interface.alarm.AlarmFacade import AlarmFacade
from net.effanee.alarm.activity.implementation.alarm.AlarmImpl import AlarmImpl
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl
from net.effanee.alarm.activity.interface.schedule.SchedulerFacade import SchedulerFacade
from net.effanee.alarm.activity.implementation.schedule.APSchedulerImpl import APSchedulerImpl
from net.effanee.alarm.activity.interface.persist.PersistFacade import PersistFacade
from net.effanee.alarm.activity.implementation.persist.PersistImpl import PersistImpl
import platform
import sys
from net.effanee.alarm.activity.action.ClockAction import ClockAction

class Initialise(object):
    '''Initialise the application.

    This involves:-

        Configuring the logging.
        Paring the facades with their selected implementations.
        Creating handlers for the key presses.
        Creating threads to receive and process the key presses.
    '''

    logger = logging.getLogger(__name__)

    # Threads.
    _key_press_listener = None
    _key_press_notifier = None

    def __init__(self):
        '''Empty.
        '''
        pass

    @classmethod
    def start(cls):
        '''Initialise the application.
        '''
        cls.__config_logging()

        # Match implementations to interfaces.
        cls.__config_implementations()
        cls.__intialise_implementations()

        # Get child handlers.
        menu_handler = cls.__get_menu_handler()
        direct_handler = cls.__get_direct_handler()

        # Get parent handler.
        infrared_remote_handler = InfraredRemoteHandler()

        # Add children to parent.
        cls.__register_handlers(infrared_remote_handler, [direct_handler, menu_handler])

        # Start listener and notification threads.
        cls._key_press_listener = cls.__start_listener(infrared_remote_handler)
        cls._key_press_notifier = cls.__start_notifier(cls._key_press_listener)

        cls.logger.info("Initialised and configured classes.")
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Running on platform {0}".format(sys.platform))

    @classmethod
    def stop(cls):
        '''Stop the application.
        '''
        cls.__stop_threads()
        cls.logger.info("Uninitialised and deconfigured classes.")

    @classmethod
    def __get_menu_handler(cls):
        '''Create and return the menu handler.

        Returns:
            The menu handler.
        '''
        menu_key_mappings = [
            KeysEnum.KEY_UP,
            KeysEnum.KEY_DOWN,
            KeysEnum.KEY_LEFT,
            KeysEnum.KEY_RIGHT,
            KeysEnum.KEY_ENTER,
            KeysEnum.KEY_BACK,
            KeysEnum.KEY_KP0,
            KeysEnum.KEY_KP1,
            KeysEnum.KEY_KP2,
            KeysEnum.KEY_KP3,
            KeysEnum.KEY_KP4,
            KeysEnum.KEY_KP5,
            KeysEnum.KEY_KP6,
            KeysEnum.KEY_KP7,
            KeysEnum.KEY_KP8,
            KeysEnum.KEY_KP9
            ]
        menu_handler = Handler("menu", menu_key_mappings)
        menu_handler.register_activity(Menu())
        cls.logger.debug("Created menu handler.")

        return menu_handler

    @classmethod
    def __get_direct_handler(cls):
        '''Create and return the menu handler.

        Returns:
            The menu handler.
        '''
        direct_key_mappings = [
            KeysEnum.KEY_VOLUMEDOWN,
            KeysEnum.KEY_PLAYPAUSE,
            KeysEnum.KEY_VOLUMEUP,
            KeysEnum.KEY_STOP,
            KeysEnum.KEY_SETUP
            ]

        direct_handler = Handler("direct", direct_key_mappings)
        direct_handler.register_activity(Direct())
        cls.logger.debug("Created direct key handler.")

        return direct_handler

    @classmethod
    def __infrared_remote_handler(cls):
        '''Create and return the infrared remote handler.

        Returns:
            The infrared remote handler.
        '''
        cls.logger.debug("Created infrared remote handler.")
        return InfraredRemoteHandler()

    @classmethod
    def __register_handlers(cls, ir_handler, handlers):
        '''Register the activity handlers.

        Args:
            ir_handler - The root handler for key presses originating from the
            infrared remote control.
            handlers - Handlers interested in responding to IR key presses.
        '''
        for handler in handlers:

            ir_handler.register_handler(handler)
            # pylint: disable=logging-format-interpolation
            cls.logger.debug("Registered handler {0}.".format(handler))

    @classmethod
    def __start_listener(cls, ir_handler):
        '''Start the key press listener in a separate thread.

        This is the user interface thread, which receives key presses, and
        invokes the associated activity. Activities should therefore, complete
        quickly, so as not to block the responsiveness of the user interface
        to key presses.

        Key presses received from the notifier thread are buffered.

        Args:
            ir_handler - The root handler for key presses.

        Returns:
            The listener, running in its own thread.
        '''
        key_press_listener = KeyPressListener()
        key_press_listener.register_handler(ir_handler)
        key_press_listener.daemon = True
        key_press_listener.start()
        cls.logger.debug("Created thread listener.")

        return key_press_listener

    @classmethod
    def __start_notifier(cls, listener):
        '''Start the key press notifier in a separate thread.

        This is the infrared remote reader thread, (via pylirc). It runs in a
        separate thread .

        Args:
            listener - The user interface thread.

        Returns:
            The notifier, running in its own thread.
        '''
        key_press_notifier = KeyPressNotifier()
        key_press_notifier.register_listener(listener)
        key_press_notifier.daemon = True
        key_press_notifier.start()
        cls.logger.debug("Created notifier thread.")

        return key_press_notifier

    @classmethod
    def __stop_threads(cls):
        '''Stop the listener and notifier threads.
        '''
        cls._key_press_listener.stop()
        cls._key_press_notifier.stop()
        cls.logger.debug("Stopped listener and notifier threads.")

    @classmethod
    def __config_implementations(cls):
        '''Configure the facade/implementation pairings.
        '''
        MultipleInputFacade.set_implementation(MultipleInputImpl)
        ClockFacade.set_implementation(ClockImpl)
        SpeechFacade.set_implementation(eSpeakImpl)
        AlarmFacade.set_implementation(AlarmImpl)
        PlayFacade.set_implementation(CliPlayImpl())
        SchedulerFacade.set_implementation(APSchedulerImpl)
        PersistFacade.set_implementation(PersistImpl)
        cls.logger.debug("Paired facades and implementations.")

    @classmethod
    def __intialise_implementations(cls):
        '''Initialise those implementations that have state, which should
        persist between restarts.
        '''
        clock_action = ClockAction()

        AlarmFacade.set_alarm_time(PersistFacade.get_alarm_alarm_time())

        if PersistFacade.get_alarm_is_on():
            clock_action.alarm_on()
        else:
            clock_action.alarm_off()

        if PersistFacade.get_clock_chimes_are_on():
            clock_action.chimes_on()
        else:
            clock_action.chimes_off()

        MultipleInputFacade.init_alarm_time(PersistFacade.get_multipleinput_init_alarm_time())

        cls.logger.debug("Initialised implementations.")

    @classmethod
    def __config_logging(cls):
        '''Format the logging output.
        '''
        # Create file logger.
        if platform.system() == "Windows":
            file_name = "./alarmclock.log"
        else:
            file_name = "/var/log/alarmclock.log"

#         logging.basicConfig(format="%(asctime)s %(name)s:%(levelname)s - %(message)s",
#                             datefmt="%d/%m/%Y %H:%M:%S",
#                             filename="./basic.log",
#                             filemode="a",
#                             level=logging.DEBUG)

        # Set root logger level.
        logging.getLogger("").setLevel(logging.DEBUG)

        # Add rotating file logger.
        rotating = logging.handlers.RotatingFileHandler(filename=file_name,
                                                        mode="a",
                                                        maxBytes=1000000,
                                                        backupCount=5)
        #rotating.setLevel(logging.DEBUG)
        formatter = logging.Formatter(fmt="%(asctime)s %(name)s:%(levelname)s - %(message)s", datefmt="%d/%m/%Y %H:%M:%S")
        rotating.setFormatter(formatter)
        logging.getLogger("").addHandler(rotating)

        # Add console logger.
        console = logging.StreamHandler()
        #console.setLevel(logging.DEBUG)
        #formatter = logging.Formatter(fmt="%(asctime)s %(name)s:%(levelname)s - %(message)s", datefmt="%d/%m/%Y %H:%M:%S")
        console.setFormatter(formatter)
        logging.getLogger("").addHandler(console)

        cls.logger.debug("Configured logging.")
