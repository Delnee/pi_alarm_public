'''
Created on 5 May 2015

@author: fjh
'''
# Facade/Implementation.
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.implementation.play.CliPlayImpl import CliPlayImpl

# pylint: disable=too-few-public-methods
class ShowSources(object):
    '''Helper class used to display the audio sources.
    '''
    def display_sources(self):
        '''Display audio sources.
        '''
        self.__config_implementations()
        sources = PlayFacade.get_sources()

        for source in sources:
            print source

    # pylint: disable=no-self-use
    def __config_implementations(self):
        '''Configure the facade/implementation pairings.
        '''
        PlayFacade.set_implementation(CliPlayImpl())

if __name__ == "__main__":
    ShowSources().display_sources()
