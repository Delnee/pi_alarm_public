'''
Created on 6 May 2015

@author: fjh
'''
from net.effanee.alarm.activity.interface.talk.SpeechFacade import SpeechFacade
from net.effanee.alarm.activity.implementation.talk.eSpeakImpl import eSpeakImpl
import time

# pylint: disable=too-few-public-methods
class Checkespeak(object):
    '''Helper class used to check eSpeak.
    '''
    def speak(self):
        '''Speak a phrase.
        '''
        self.__config_implementations()
        words = "I am completely operational, and all my circuits are functioning perfectly."
        print "Say - '{0}'".format(words)
        SpeechFacade.say(words)
        time.sleep(6)
        print "Finished."

    # pylint: disable=no-self-use
    def __config_implementations(self):
        '''Configure the facade/implementation pairings.
        '''
        SpeechFacade.set_implementation(eSpeakImpl)

if __name__ == "__main__":
    Checkespeak().speak()
