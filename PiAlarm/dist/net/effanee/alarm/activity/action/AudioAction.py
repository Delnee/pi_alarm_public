'''
Created on 24 Apr 2015

@author: fjh
'''
import logging
# pylint: disable=line-too-long
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.interface.schedule.SchedulerFacade import SchedulerFacade
from net.effanee.alarm.activity.action.CommonAction import CommonAction

class AudioAction(object):
    '''A collection of actions for audio tasks.

    UI elements should call action classes, rather than the facade classes
    directly.

    Uses and combines various facade classes and the common action class.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self):
        '''
        Constructor
        '''
        self.__play_facade = PlayFacade
        self.__scheduler_facade = SchedulerFacade

        self.__common_action = CommonAction()

        self.__paused_job = None

    def say(self, words):
        '''Say the words.
        '''
        self.__common_action.say(words)

    def play_pause(self):
        '''Play/pause toggle.
        '''
        self.__play_facade.play_pause()

        if self.__play_facade.is_paused():
            self.__common_action.say("Paused")
            self.__scheduler_facade.register_paused_job(self.__say_paused)

        elif self.__play_facade.is_playing():
            self.__common_action.say("Playing")
            self.__scheduler_facade.unregister_paused_jobs()

    def __say_paused(self):
        '''Say paused.
        '''
        self.__common_action.say("Paused")

    def stop(self):
        '''Stop.
        '''
        self.__play_facade.stop()
        self.__common_action.say("Stopped")

        self.__scheduler_facade.unregister_paused_jobs()

    def volume_up(self):
        '''Volume up.
        '''
        self.__play_facade.volume_up()

    def volume_down(self):
        '''Volume down.
        '''
        self.__play_facade.volume_down()

    def get_sources(self):
        '''Get a list of source names.

        Returns:
            A list of source names.
        '''
        return self.__play_facade.get_sources()

    def set_source(self, name):
        '''Set the current source.

        Args:
            name - The name of the source.
        '''
        self.__play_facade.set_source(name)
        self.__common_action.say("Source set to " + name)

    def get_source(self):
        '''Get the current source.

        Returns:
            The source.
        '''
        return self.__play_facade.get_source()
