'''
Created on 24 Apr 2015

@author: fjh
'''
import logging
import os
import threading
# pylint: disable=line-too-long
from net.effanee.alarm.activity.interface.clock.ClockFacade import ClockFacade
from net.effanee.alarm.activity.interface.play.PlayFacade import PlayFacade
from net.effanee.alarm.activity.interface.schedule.SchedulerFacade import SchedulerFacade
from net.effanee.alarm.activity.interface.alarm.AlarmFacade import AlarmFacade
from net.effanee.alarm.activity.interface.multipleinput.MultipleInputFacade import MultipleInputFacade
from net.effanee.alarm.activity.interface.persist.PersistFacade import PersistFacade
from net.effanee.alarm.activity.action.CommonAction import CommonAction

# pylint: disable=too-many-instance-attributes
class ClockAction(object):
    '''A collection of actions for clock tasks.

    UI elements should call action classes, rather than the facade classes
    directly.

    Uses and combines various facade classes and the common action class.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self):
        '''
        Constructor
        '''
        self.__alarm_facade = AlarmFacade
        self.__clock_facade = ClockFacade
        self.__multiple_input_facade = MultipleInputFacade
        self.__play_facade = PlayFacade
        self.__scheduler_facade = SchedulerFacade
        self.__persist_facade = PersistFacade

        self.__common_action = CommonAction()

        self.__paused_job = None

    def say_time(self):
        '''Say the current time.
        '''
        self.__common_action.say(self.__get_time_string())

    def __get_time_string(self):
        '''Get the current time.

        Returns:
            The current time as a pronounceable string.
        '''
        local_time = self.__clock_facade.get_time()
        hours = local_time.hour
        minutes = local_time.minute
        time_string = "The time is now {0:d} hours and {1:d} minutes".format(hours, minutes)

        return time_string

    def chimes_on(self):
        '''Set the chimes state to on.
        '''
        if not self.__clock_facade.chimes_are_on():
            self.__clock_facade.chimes_on()
            self.__persist_facade.set_clock_chimes_are_on(True)
            self.__scheduler_facade.register_every_hour_job(self.play_chime)

        self.__common_action.say("Chimes are on")

    def chimes_off(self):
        '''Set the chimes state to off.
        '''
        if self.__clock_facade.chimes_are_on():
            self.__clock_facade.chimes_off()
            self.__persist_facade.set_clock_chimes_are_on(False)
            self.__scheduler_facade.unregister_every_hour_job(self.play_chime)

        self.__common_action.say("Chimes are off")

    def play_chime(self):
        '''Play a random chime.
        '''
        play_thread = self.PlayChime(self.__common_action, self.__clock_facade, self.__play_facade)
        play_thread.daemon = True
        play_thread.start()

    def play_over(self, wav_file):
        '''Play a wav file over the top of the currently playing file/stream.

        Plays at 100% volume and blocks until the wav file has finished playing.

        Args:
            wav_file - The path to the wav file.
        '''
        self.__play_facade.play_over_sync(wav_file)

    def set_alarm_time(self):
        '''Start capturing keys to get the alarm time.
        '''
        self.__common_action.say("Input four digit time.")
        self.__multiple_input_facade.set_alarm_time()

    def add_alarm_key_press(self, key_press):
        '''Capture the key press and add it to the alarm time being input. Once
        the complete time has been captured, store the new alarm time.
        '''
        self.__common_action.say_key_press(key_press)

        if self.__multiple_input_facade.activate(key_press):
            alarm_time = self.__multiple_input_facade.get_alarm_time()
            self.__alarm_facade.set_alarm_time(alarm_time)
            self.__persist_facade.set_alarm_alarm_time(alarm_time)

            # If the alarm job is running, reset it with the new time.
            if self.__alarm_facade.is_on():
                self.alarm_off()
                self.alarm_on()

            hours = alarm_time.hour
            minutes = alarm_time.minute
            time_string = "The alarm is set to {0:d} hours and {1:d} minutes.".format(hours, minutes)
            self.__common_action.say(time_string)

    def alarm_toggle(self):
        '''Toggle the alarm state on/off.
        '''
        if self.__alarm_facade.is_on():
            self.alarm_off()
        else:
            self.alarm_on()

    def alarm_on(self):
        '''Set the alarm state to on.
        '''
        if self.__alarm_facade.is_on():
            self.alarm_off()
        if not self.__alarm_facade.is_on():
            self.__alarm_facade.switch_on()
            self.__persist_facade.set_alarm_is_on(True)
            alarm_time = self.__alarm_facade.get_alarm_time()
            self.__scheduler_facade.create_alarm_job(self.__play_alarm, alarm_time)

        hours = alarm_time.hour
        minutes = alarm_time.minute
        time_string = "Alarm is on and will activate at {0:d} hours and {1:d} minutes.".format(hours, minutes)
        self.__common_action.say(time_string)

    def alarm_off(self):
        '''Set the alarm state to off.
        '''
        if self.__alarm_facade.is_on():
            self.__alarm_facade.switch_off()
            self.__persist_facade.set_alarm_is_on(False)
            self.__scheduler_facade.remove_alarm_job()

        self.__common_action.say("Alarm is off")

    def __play_alarm(self):
        '''Play alarm.
        '''
        play_thread = self.PlayWakeUp(self.__common_action, self.__clock_facade, self.__play_facade, self.__alarm_facade)
        play_thread.daemon = True
        play_thread.start()

    # pylint: disable=too-few-public-methods
    class PlayChime(threading.Thread):
        '''Plays a chime and speaks its name.

        Plays the chime synchronously, and if the chime is fully played, speaks
        the name of the played wav file.
        '''

        logger = logging.getLogger(__name__)

        def __init__(self, common_action, clock_facade, play_facade):
            '''
            Constructor.
            '''
            threading.Thread.__init__(self)

            self.__common_action = common_action
            self.__clock_facade = clock_facade
            self.__play_facade = play_facade

        def run(self):
            '''Override thread run.
            '''
            wav_file_path = self.__clock_facade.get_random_chime_wav()
            wav_file_name = os.path.basename(wav_file_path)

            # Get and process the result.
            result = self.__play_facade.play_over_sync(wav_file_path)
            #pylint: disable=logging-format-interpolation
            if result == PlayFacade.DONE:
                self.__common_action.say(os.path.splitext(wav_file_name)[0])
                self.__class__.logger.debug("Played wav file {0}.".format(wav_file_name))
            else:
                self.__class__.logger.debug("Playing of wav file {0} cancelled.".format(wav_file_name))

    # pylint: disable=too-few-public-methods
    class PlayWakeUp(threading.Thread):
        '''Plays an alarm wav, speaks its name and plays the currently selected
        audio source.

        Plays the alarm wav synchronously, and if the alarm wav is fully played,
        speaks the name of the played wav file, before playing the audio source.
        '''

        logger = logging.getLogger(__name__)

        def __init__(self, common_action, clock_facade, play_facade, alarm_facade):
            '''
            Constructor.
            '''
            threading.Thread.__init__(self)

            self.__common_action = common_action
            self.__clock_facade = clock_facade
            self.__play_facade = play_facade
            self.__alarm_facade = alarm_facade

        def run(self):
            '''Override thread run.
            '''
            wav_file_path = self.__alarm_facade.get_wav_file()
            wav_file_name = os.path.basename(wav_file_path)

            # Play the alarm wav and speak its name.
            self.__play_facade.play_over_sync(wav_file_path)
            self.__common_action.say(os.path.splitext(wav_file_name)[0])
            #pylint: disable=logging-format-interpolation
            self.__class__.logger.debug("Played wav file {0}.".format(wav_file_name))

            # Play the selected audio source.
            if self.__play_facade.is_paused():
                self.__play_facade.play_pause()

            elif self.__play_facade.is_playing():
                pass

            elif self.__play_facade.is_stopped():
                self.__play_facade.play_pause()
