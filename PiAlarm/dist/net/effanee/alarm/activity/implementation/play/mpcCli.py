# pylint: disable=invalid-name
'''
Created on 9 Mar 2015

@author: fjh
'''
import subprocess
import logging
from subprocess import CalledProcessError

class mpcCli(object):
    '''mpc command line interface implementation, using shell commands, issued
    in a new process.

    mpc is a client for the mpd daemon, which intrinsically executes
    asynchronously.

    This class should be safe, when accessed concurrently by multiple
    threads.
    '''

    logger = logging.getLogger(__name__)

    def __init__(self, mpc_path):
        '''Constructor
        '''
        self.__mpc_path = mpc_path

    def add(self, uri):
        '''Add a new item to the current play list.

        Args:
            uri - The URI of the item.
        '''
        self.__execute("add", uri)

    def load(self, uri):
        '''Load the play list given by the uri.

        Args:
            uri - The URI of the play list.
        '''
        self.__execute("load", uri)

    def play(self):
        '''Play the current play list.
        '''
        self.__execute("play")

    def pause(self):
        '''Pause the current play list.
        '''
        self.__execute("pause")

    def stop(self):
        '''Stop the current play list.
        '''
        self.__execute("stop")

    def setvol(self, level):
        '''Set the volume.

        Args:
            level - A value between 0 and 100.
        '''
        self.__execute("volume", str(level))

    def getvol(self):
        '''Get the current volume.

        Returns:
            The current volume in the format - "volume: 20%"
        '''
        # volume on its own returns the current volume.
        return self.__execute("volume")

    def clear(self):
        '''Clear the current play list.
        '''
        self.__execute("clear")

    def status(self):
        # pylint: disable=line-too-long
        '''Get the current status.

        Returns:
            The current status in the format - "volume: 30%   repeat: off   random: off   single: off   consume: off"
        '''
        return self.__execute("status")

    def __execute(self, command, *args):
        '''Execute the supplied mpc command.

        Args:
            command - The mpc command to execute.
            *args - The single argument to the mpc command.

        Raises:
            CalledProcessError
        '''
        output = ""
        command_line = [self.__mpc_path, command] + (list(args))
        try:
            # pylint: disable=line-too-long
            output = subprocess.check_output(command_line, stdin=None, stderr=None, shell=False)

        except CalledProcessError as ex:
            # pylint: disable=logging-format-interpolation
            self.__class__.logger.critical("Could not execute the mpc command {0}, exception {1}"
                                           .format(command, ex))
            raise ex

        return output
        