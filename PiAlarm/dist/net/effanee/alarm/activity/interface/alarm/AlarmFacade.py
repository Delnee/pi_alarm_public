'''
Created on 18 Mar 2015

@author: fjh
'''
import logging

class AlarmFacade(object):
    '''An Alarm facade around an alarm implementation.

    Used to access alarm functions.
    '''

    logger = logging.getLogger(__name__)

    # The implementation of this classes interface.
    __implementation = None

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def set_implementation(cls, implementation):
        '''Set the implementation used by this facade. This method should be
        called before any of the other class methods are called.

        Args:
            implementation - An implementation that implements this classes
            interface.
        '''
        cls.__implementation = implementation
        cls.__implementation.init()

    @classmethod
    def switch_on(cls):
        '''Switch the alarm on.
        '''
        cls.__implementation.switch_on()

    @classmethod
    def switch_off(cls):
        '''Switch the alarm on.
        '''
        cls.__implementation.switch_off()

    @classmethod
    def is_on(cls):
        '''Is the alarm on?

        Returns:
            True if the alarm is on, else False.
        '''
        return cls.__implementation.is_on()

    @classmethod
    def set_alarm_time(cls, alarm_time):
        '''Set the alarm time.

        Args:
            alarm_time - The time to set off the alarm.
        '''
        cls.__implementation.set_alarm_time(alarm_time)

    @classmethod
    def get_alarm_time(cls):
        '''Get the alarm time.

        Returns:
            The time of the alarm.
        '''
        return cls.__implementation.get_alarm_time()

    @classmethod
    def get_wav_file(cls):
        '''Get the wav file used as the alarm.
        
        Returns:
            The alarm wave file.
        '''
        return cls.__implementation.get_wav_file()
