'''
Created on 15 Mar 2015

@author: fjh
'''
import logging

class ClockFacade(object):
    '''A Clock facade around a clock implementation.

    Used to access clock functions.
    '''

    logger = logging.getLogger(__name__)

    # The implementation of this classes interface.
    __implementation = None

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def set_implementation(cls, implementation):
        '''Set the implementation used by this facade. This method should be
        called before any of the other classes methods are called.

        Args:
            implementation - an implementation that implements this classes
            interface.
        '''
        cls.__implementation = implementation
        cls.__implementation.init()

    @classmethod
    def get_time(cls):
        '''Get the current local time.

        Returns:
            A current local time object.
        '''
        return cls.__implementation.get_time()

    @classmethod
    def chimes_on(cls):
        '''Set the chimes state to on.
        '''
        cls.__implementation.chimes_on()

    @classmethod
    def chimes_off(cls):
        '''Set the chimes state to off.
        '''
        cls.__implementation.chimes_off()

    @classmethod
    def chimes_are_on(cls):
        '''Switch the alarm on.
        '''
        return cls.__implementation.chimes_are_on()

    @classmethod
    def get_random_chime_wav(cls):
        '''Get a random chime wave file.

        Returns:
            The wave file.
        '''
        return cls.__implementation.get_random_chime_wav()

    @classmethod
    def get_all_chime_wavs(cls):
        '''Get a list of all the chime wavs.

        Returns:
            The wave file list.
        '''
        return cls.__implementation.get_all_chime_wavs()
