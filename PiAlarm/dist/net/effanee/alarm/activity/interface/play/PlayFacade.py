'''
Created on 15 Mar 2015

@author: fjh
'''
import logging

class PlayFacade(object):
    '''An Play facade around an play implementation.

    Used to play local audio files and remote audio streams.
    '''

    logger = logging.getLogger(__name__)

    # The implementation of this classes interface.
    __implementation = None

    # Status enum's
    RUNNING = "running"
    DONE = "done"
    CANCELLED = "cancelled"

    def __init__(self):
        '''
        Constructor
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def set_implementation(cls, implementation):
        '''Set the implementation used by this facade. This method should be
        called before any of the other classes methods are called.

        Args:
            implementation - An implementation that implements this classes
            interface.
        '''
        cls.__implementation = implementation
        cls.__implementation.init()

    @classmethod
    def activate(cls, key_press):
        '''Activity interface method. Uses the audio_key_mappings map to
        select which method to invoke.

        Args:
            key_press: The internal enum representing the key press code.

        Returns:
            False.

        Raises:
            Nothing
        '''
        cls.__implementation.activate(key_press)

    @classmethod
    def play_pause(cls):
        '''Play/pause toggle.
        '''
        cls.__implementation.play_pause()

    @classmethod
    def stop(cls):
        '''Stop.
        '''
        cls.__implementation.stop()

    @classmethod
    def is_playing(cls):
        '''Is the audio playing?

        Returns:
            True - If the audio is playing, else False.
        '''
        return cls.__implementation.is_playing()

    @classmethod
    def is_paused(cls):
        '''Is the audio paused?

        Returns:
            True - If the audio is paused, else False.
        '''
        return cls.__implementation.is_paused()

    @classmethod
    def is_stopped(cls):
        '''Is the audio stopped?

        Returns:
            True - If the audio is stopped, else False.
        '''
        return cls.__implementation.is_stopped()

    @classmethod
    def volume_up(cls):
        '''Volume up.
        '''
        cls.__implementation.volume_up()

    @classmethod
    def volume_down(cls):
        '''Volume down.
        '''
        cls.__implementation.volume_down()

    @classmethod
    def get_sources(cls):
        '''Get a list of source names.

        Returns:
            A list of source names.
        '''
        return cls.__implementation.get_sources()

    @classmethod
    def set_source(cls, name):
        '''Set the current source.

        Args:
            name: The name representing the source.
        '''
        cls.__implementation.set_source(name)

    @classmethod
    def get_source(cls):
        '''Get the current source.
        '''
        return cls.__implementation.get_source()

    @classmethod
    def play_over_async(cls, wav_file):
        '''Play a wav file over the top of the currently playing file/stream.

        Plays at 100% volume and does not block. If requested while a previous
        request is still playing, the previous request is first stopped.

        Args:
            wav_file: The path to the wav file
        '''
        cls.__implementation.play_over_async(wav_file)

    @classmethod
    def play_over_sync(cls, wav_file):
        '''Play a wav file over the top of the currently playing file/stream.

        Plays at 100% volume and blocks. If requested while a previous
        request is still playing, the previous request is first stopped.

        Args:
            wav_file: The path to the wav file

        Returns:
            One of PlayFacade.RUNNING, PlayFacade.DONE, PlayFacade.CANCELLED.
            Cancelled is returned if an error occurs.
        '''
        return cls.__implementation.play_over_sync(wav_file)
