'''
Alarm Clock provides some alarm clock functionality.

Copyright (C) 2015 Francis J. Hammell <hammell.francis@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

There is a copy of this license in net/effanee/alarm/doc/license.txt.
If not, see <http://www.gnu.org/licenses/>.

Created on 10 Feb 2015

@author: fjh
'''
import sys, os
sys.path.append(os.path.dirname(os.path.abspath(__file__)))

import time, argparse
from net.effanee.alarm.Initialise import Initialise
from net.effanee.alarm.help.ShowMenu import ShowMenu
import signal
import platform
import logging
from net.effanee.alarm.help.ShowSources import ShowSources
from net.effanee.alarm.help.Checkmpd import Checkmpd
from net.effanee.alarm.help.Checkaplay import Checkaplay
from net.effanee.alarm.help.Checkespeak import Checkespeak
from net.effanee.alarm.help.Checklirc import Checklirc
from net.effanee.alarm.help.ShowLicense import ShowLicense

# pylint: disable=too-few-public-methods
class Main(object):
    '''Used to start the alarm clock. Call Main.main().

    From the command line:-

        python Main.py

    to show help:-

        python Main.py -h
    '''

    logger = logging.getLogger(__name__)

    def __init__(self):
        '''Do not use.
        '''
        raise RuntimeError("This class should never be instantiated.")

    @classmethod
    def main(cls, *args):
        '''Verily thou shalt start hither. Somewhither, thou shalt not start.
        Neither, shalt thou start thitherward, other than thou be proceeding
        hitherward. Summoning Main() is right out.

        Args:
            *args - The command line arguments.

        Returns:
            0 on a successful exit, else -1.
        '''
        # Process command line args.
        if len(sys.argv) > 1:
            if cls.__process_arguments(*args):
                sys.exit(0)
            else:
                sys.exit(-1)

        # Trap keyboard interrupts.
        # Ctrl + c works on both Windows and Linux.
        # start-stop-daemon uses SIGTERM.
        signal.signal(signal.SIGINT, Main.__signal_handler)
        signal.signal(signal.SIGTERM, Main.__signal_handler)

        # Start up tasks.
        Initialise.start()

        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Initialised.")

        # Wait.
        cls.bad_exit = False
        try:
            if platform.system() == "Windows":
                # signal.pause() is not available under MS Windows.
                counter = 30
                while counter:
                    print "%s %s" % ("Sleeping", counter)
                    time.sleep(1)
                    counter -= 1
            else:
                ### Main thread just waits here for a POSIX signal to terminate. ###
                cls.logger.info("Waiting on SIGINT/SIGTERM.")
                # pylint: disable=no-member
                signal.pause() # @UndefinedVariable

        except StandardError as ex:
            # pylint: disable=logging-format-interpolation
            cls.logger.critical("Error while running {0}".format(ex))
            cls.bad_exit = True

        finally:
            # Shutdown tasks.
            Initialise.stop()
            cls.logger.debug("Uninitialised.")

            #Finish.
            if cls.bad_exit:
                cls.logger.warn("Bad exit.")
                sys.exit(-1)
            else:
                cls.logger.info("Good exit.")
                sys.exit(0)

    @classmethod
    # pylint: disable=unused-argument
    def __signal_handler(cls, sig_numb, handler):
        '''Log received signal.
        '''
        # pylint: disable=logging-format-interpolation
        cls.logger.debug("Received signal number {0}".format(sig_numb))

    @classmethod
    def __process_arguments(cls, *args):
        '''Process command line arguments.

        Args:
            *args - The command line arguments.

        Returns:
            True if the command line argument is executed without raising an
            exception, else False.
        '''
        # Get the command line arguments.
        # pylint: disable=line-too-long
        parser = argparse.ArgumentParser()
        parser.add_argument("-v", "--version", help="show the version", action="version", version="0.0.1")
        parser.add_argument("-sm", "--show-menu", help="show the menu structure", action="store_true", required=False)
        parser.add_argument("-ss", "--show-sources", help="show the audio sources", action="store_true", required=False)
        parser.add_argument("-sl", "--show-license", help="show license", action="store_true", required=False)
        group = parser.add_mutually_exclusive_group()
        group.add_argument("-cm", "--check-mpd", help="check the local mpd audio source (exclusive)", action="store_true", required=False)
        group.add_argument("-ca", "--check-aplay", help="check the local aplay audio source (exclusive)", action="store_true", required=False)
        group.add_argument("-ce", "--check-espeak", help="check espeak (exclusive)", action="store_true", required=False)
        group.add_argument("-cl", "--check-lirc", help="check lirc (exclusive)", action="store_true", required=False)
        args = parser.parse_args()

        success = True
        try:
            # pylint: disable=no-member
            if args.show_menu:
                print "--show-menu has been passed."
                ShowMenu().display_menu_structure()
                cls.logger.debug("Processed --show-menu command line argument.")
            if args.show_sources:
                print "--show-sources has been passed."
                ShowSources().display_sources()
                cls.logger.debug("Processed --show-sources command line argument.")
            if args.show_license:
                print "--show-license has been passed."
                ShowLicense().display_license()
                cls.logger.debug("Processed --show-license command line argument.")
            if args.check_mpd:
                print "--check-mpd has been passed."
                Checkmpd().play_local()
                cls.logger.debug("Processed --check-mpd command line argument.")
            if args.check_aplay:
                print "--check-aplay has been passed."
                Checkaplay().play()
                cls.logger.debug("Processed --check-aplay command line argument.")
            if args.check_espeak:
                print "--check-espeak has been passed."
                Checkespeak().speak()
                cls.logger.debug("Processed --check-espeak command line argument.")
            if args.check_lirc:
                print "--check-lirc has been passed."
                Checklirc().input()
                cls.logger.debug("Processed --check-lirc command line argument.")

        except StandardError as ex:
            # pylint: disable=logging-format-interpolation
            cls.logger.critical("Error {0}".format(ex))
            success = False

        return success

def command_line_invocation():
    '''Tell me if it's a command line invocation.
    '''
    # pylint: disable=logging-format-interpolation
    Main.logger.debug("Started from the command line.")

if __name__ == "__main__":
    command_line_invocation()
    Main.main()
